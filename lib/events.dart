import 'dart:developer';

import 'package:flutter_foreground_plugin/flutter_foreground_plugin.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:omnilight2/global.dart';
import 'package:omnilight2/model.dart';

import 'blescanner.dart';

final FlutterLocalNotificationsPlugin notify = FlutterLocalNotificationsPlugin();

enum eventType {
  UNKNOWN,
  SYSTEM,
  CHARACTER_UPDATED,
  INVENTORY_UPDATED,
  INVENTORY_ADDED,
  INVENTORY_REMOVED,
  NEW_DEAL,
  DEAL_UPDATED,
  MESSAGE
}

Set<eventType> filteredEvents = {};
Map<eventType, String> eventMessages = {};

class ReturnAnchor {
  late String path;
  Object? argument;

  ReturnAnchor(this.path, this.argument);

  String get key => path + (argument?.hashCode.toString() ?? "_");
}

Map<String, ReturnAnchor> anchors = {};

void notifyCallback(NotificationResponse key) {
  ReturnAnchor? anc = anchors.remove(key.payload);
  if (anc != null) {
    navigatorKey.currentState!.pushNamed(anc.path, arguments: anc.argument);
  }
}

void initEvents() {
  if (!platform.hasNotification) {
    return;
  }
  const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('notify');
  const InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid);
  notify.initialize(initializationSettings, onDidReceiveNotificationResponse: notifyCallback);
  eventMessages[eventType.UNKNOWN] = "Неизвестное событие";
  eventMessages[eventType.SYSTEM] = "Системное событие";
  eventMessages[eventType.CHARACTER_UPDATED] = "Характеристики персонажа изменены";
  eventMessages[eventType.INVENTORY_UPDATED] = "Предмет в инвентаре изменен";
  eventMessages[eventType.INVENTORY_ADDED] = "Добавлено новое ";
  eventMessages[eventType.INVENTORY_REMOVED] = "Удалено ";
  eventMessages[eventType.NEW_DEAL] = "Новая сделка";
  eventMessages[eventType.DEAL_UPDATED] = "Сделка изменена";
  eventMessages[eventType.MESSAGE] = "Новое сообщение";
}

void showNotification(String? text, String path, Object? argument) {
  if (!platform.hasNotification) {
    return;
  }
  var android = const AndroidNotificationDetails("Omnilight_0", "Omnilight");
  var details = NotificationDetails(android: android);
  var a = ReturnAnchor(path, argument);
  anchors[a.key] = a;
  notify.show(0, "Новое событие в игре", text, details, payload: a.key);
}

void notifyUserCustom(eventType event, String? text, String path, Object? argument) {
  if (filteredEvents.contains(event)) return;
  showNotification(text, path, argument);
}

void notifyUser(
  eventType event,
) {
  notifyUserCustom(event, eventMessages[event], "/character", null);
}

void notifyUserFeature(eventType event, FeatureTypeDesc ft) {
  notifyUserCustom(event, "${eventMessages[event]!} ${ft.name}", "/inventory", null);
}

void notifyUserMessage(Object messageId) {
  notifyUserCustom(eventType.MESSAGE, eventMessages[eventType.MESSAGE], "/chats/chat", messageId);
}

void foregroudServiceFunc() {
  log("Hi from foregroud");
  if (configuration.bleConfig.enabled) startBLEScanning();
}

void checkForegroundService() async {
  try {
    await FlutterForegroundPlugin.setServiceMethodInterval(seconds: 10);
    await FlutterForegroundPlugin.setServiceMethod(foregroudServiceFunc);
    await FlutterForegroundPlugin.startForegroundService(
      holdWakeLock: false,
      onStarted: () {
        log("Foreground on Started");
      },
      onStopped: () {
        log("Foreground on Stopped");
      },
      title: "Omnilight",
      content: "Нажмите для перехода в приложение",
      iconName: "notify",
    );
  } on Error catch (e) {
    addError(Error.system("Foreground error $e"));
  }
}
