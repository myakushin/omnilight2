import 'dart:async';
import 'dart:developer';
import 'dart:math' as math;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

import 'config.dart';
import 'global.dart';
import 'model.dart';

bool needRefresh = false;
bool isScanning = false;
FlutterBlue flutterBlue = FlutterBlue.instance;
bool bleStarted = false;
//AssetsAudioPlayer audioPlayer;
const int rssiMeasuredPower = -61;

typedef BLEActionFunc = void Function(List<String> args);

void startBLEScanning() {
  if (bleStarted) return;
  bleStarted = true;

  if (kDebugMode) {
    log("Start BLE Scanning");
  }
//TODO: сделать регулиреумую скорость скана
  flutterBlue.state.first.then((value) {
    if (value == BluetoothState.on) {
      Timer.periodic(const Duration(seconds: 10), doScan);
      Timer.periodic(const Duration(seconds: 1), doRefresh);
    } else {
      lastState.addError(Error.bluetooth("Проблема с запуском Bluetooth $value "));
    }
  });

  flutterBlue.connectedDevices.then((devices) {
    // for (BluetoothDevice dev in devices) {
    // }
  });
  //bleManager.startPeripheralScan(]).listen(handeScanResult);
}

void doScan(Timer timer) {
  if (configuration.bleConfig.enabled) {
    flutterBlue.stopScan();
    //Андройд не сканирует простраство в фоновом режиме, если не фильтровать UUID сервисов.
    //мы их знаем из конфига, поэтому будем фильровать.
    //List<Guid> uuids = configuration.bleConfig.beaconActions.keys
    //    .map((i) => Guid("$i-0000-1000-8000-00805F9B34FB".padLeft(36, "0")))
    //    .toList(growable: false);
    //flutterBlue.startScan(withDevices: uuids, scanMode: ScanMode.lowLatency);
    flutterBlue.startScan(scanMode: ScanMode.lowLatency);
    flutterBlue.scanResults.listen(handelScanResult);
    devices.removeWhere((key, value) {
      if (DateTime.now().difference(value.lastSeen).inSeconds > configuration.bleConfig.timeout) {
        value.beforeRemove();
        return true;
      }
      return false;
    });
  }
}

void doRefresh(Timer timer) {
  if (!needRefresh) return;
  List<KnownDevice> ours = [];
  for (var dev in devices.values) {
    if (dev.visible) ours.add(dev);
  }
  ourDevices.add(ours);
  needRefresh = false;
}

//TODO: рефактор под некоторое количество типов устройств
class KnownDevice {
  bool visible = false;
  String displayName = "Маяк";
  late BluetoothDevice dev;
  int? beaconID;
  String? action;
  late int rssi;
  DateTime? firstSeen;
  late DateTime lastSeen;
  Map<BLEActionTrigger, List<BLEAction>>? beaconActions;
  Map<String, BLEActionFunc> bleActionFuncs = {};
  bool peripheral = false;

  double get distance {
    return math.pow(10, (rssiMeasuredPower - rssi) / (10 * 2)) as double;
  }

  void update(ScanResult res) {
    rssi = res.rssi;
    lastSeen = DateTime.now();
    if (visible) needRefresh = true;
  }

  KnownDevice.newFound(ScanResult res) {
    firstSeen = DateTime.now();
    dev = res.device;
    update(res);
    for (var svc in res.advertisementData.serviceUuids) {
      if (!svc.endsWith("1000-8000-00805f9b34fb")) continue;
      beaconID = int.tryParse(svc.split("-")[0], radix: 16);
      if (beaconID == null) {
        continue;
      }
      beaconActions = configuration.bleConfig.beaconActions[beaconID!];
      if (beaconActions == null) {
        continue;
      }
      //bleActionFuncs["PlaySound"] = bleActionPlaySound;
      bleActionFuncs["ApplyTransition"] = bleActionApplyTransition;
      bleActionFuncs["Show"] = bleActionShow;
      runBeaconActions(BLEActionTrigger.onEnter);
    }
    if (res.device.name == "L4SHIELD") {
      peripheral = true;
      visible = true;
      displayName = "Щит";
      if (lastState.bleDeviceMAC?.valueStr == dev.id.id) {
        connect();
      }
    }
  }

  void runBeaconActions(BLEActionTrigger trigger) {
    for (BLEAction act in beaconActions?[trigger] ?? []) {
      var func = bleActionFuncs[act.name];
      if (func != null) {
        func(act.args);
      } else {}
    }
  }

  void beforeRemove() {
    runBeaconActions(BLEActionTrigger.onExit);
    if (visible) needRefresh = true;
  }

  Future<void> bleActionApplyTransition(List<String> args) async {
    for (var name in args) {
      var trans = lastState.transitions[name];
      if (trans == null) {
        return;
      }
      await trans.thenLoaded();
      if (trans.passed) {
        trans.apply();
      }
    }
  }

/*
  void bleActionPlaySound(List<String> args) {
    if (audioPlayer == null) audioPlayer = AssetsAudioPlayer();
    print("Load $args to playlist");
    var audios = args.map((e) => Audio(e)).toList();
    audioPlayer.open(Playlist(audios: audios)).then((value) {
      print("Start playing");
      audioPlayer.play();
    });
  }
*/
  void bleActionShow(List<String> args) {
    visible = true;
    if (args.isNotEmpty) displayName = args[0];
    needRefresh = true;
  }

  void connect() async {
    BluetoothDeviceState state = await dev.state.first;
    dev.state.listen(onConnectionChanged);
    if (state == BluetoothDeviceState.disconnected) {
      await dev.connect(autoConnect: true);
    }
  }

  void onConnectionChanged(BluetoothDeviceState state) {
    switch (state) {
      case BluetoothDeviceState.connected:
        dev.discoverServices().then(onServicesScaned);
        break;
      case BluetoothDeviceState.disconnected:
        lastState.bleConnection = null;
        lastState.updated = true;
        break;
      default:
        break;
    }
  }

  Future<void> onServicesScaned(List<BluetoothService> services) async {
    for (var s in services) {
      if (s.uuid.toString() == configuration.blePeripheralsUUID) {
        lastState.bleConnection = BLEConnection(dev, s);
        if (lastState.bleDeviceMAC != null && lastState.bleDeviceMAC?.valueStr != dev.id.id) {
          lastState.bleDeviceMAC!.set(dev.id.id);
        }
        await lastState.bleConnection!.setupAllCharacteristics();
      }
    }
  }
}

Map<DeviceIdentifier, KnownDevice> devices = {};
StreamController<List<KnownDevice>> ourDevices = StreamController.broadcast();

void handelScanResult(List<ScanResult> results) {
  for (var result in results) {
    if (result.advertisementData.localName.startsWith("L4")) {
      var devid = result.device.id;
      if (devices.containsKey(devid)) {
        devices[devid]!.update(result);
      } else {
        devices[devid] = KnownDevice.newFound(result);
      }
    }
  }
}

class ScanMenu extends StatelessWidget {
  const ScanMenu({super.key});

  Widget getLabel(BuildContext context) {
    var st = configuration.bleConfig.showTotal;
    if (st == null) return const Text("Видимые метки");
    FeatureTypeDesc? ft = lastState.featureTypesByName[st.feature];
    if (ft == null) return Text("${st.feature} не найдена");
    return StreamBuilder(
        stream: ft.getUpdates(),
        builder: (BuildContext ctx, AsyncSnapshot<Map<String, dynamic>> data) {
          if (data.data == null) return const Text("Нет данных");
          String val = "<неизвестно>";
          if (data.data!.isNotEmpty) val = data.data!.values.first.toString();
          return Text("${st.label} $val");
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: getLabel(context),
        ),
        drawer: getCommonDrawer(context),
        body: StreamBuilder(
            initialData: List<KnownDevice>.empty(),
            stream: ourDevices.stream,
            builder: (BuildContext ctx, AsyncSnapshot<List<KnownDevice>> snap) {
              if (!snap.hasData) return const Text("Нет доступных данных");
              List<Widget> tiles = [];
              for (KnownDevice dev in snap.data ?? []) {
                Widget? trail;
                if (dev.peripheral) {
                  trail = ElevatedButton(
                      onPressed: () {
                        dev.connect();
                        Navigator.of(context).pop();
                      },
                      child: const Text("Соеденить"));
                }
                tiles.add(ListTile(
                  title: Text(dev.displayName),
                  subtitle: Text("расстояние ${dev.distance.toStringAsFixed(2)} м"),
                  trailing: trail,
                ));
              }
              return ListView(
                children: tiles,
              );
            }));
  }
}

class BLEConnectionLink {
  FeatureDesc? feat;
  late Guid uuid;
  BluetoothCharacteristic? blechar;
  late BLEPeripheralsServiceConf conf;
  int valueInt = 0;
  String valueStr = "";

  BLEConnectionLink(BLEPeripheralsServiceConf cfg) {
    feat = lastState.char!.featuresByTypeName[cfg.feature]?.first;
    if (feat != null ||
        [BLEPeripherialCharDirection.both, BLEPeripherialCharDirection.outbound].contains(cfg.direction)) {
      lastState.featureUpdateListeners[cfg.feature] = onFeatureUpdated;
    }
    uuid = Guid(cfg.uuid);
    conf = cfg;
  }

  Future<void> onFeatureUpdated(FeatureDesc ft) async {
    feat = ft;
    if (blechar == null) return;
    if (conf.type == BLEPeripheralCharType.int) {
      await blechar!.write([feat!.valueInt]);
    }
    if (conf.type == BLEPeripheralCharType.string) {
      await blechar!.write(feat!.valueStr.codeUnits);
    }
  }

  void connectChar(BluetoothCharacteristic c) {
    blechar = c;
  }

  Future<void> syncBLE() async {
    if (conf.direction == BLEPeripherialCharDirection.inbound || conf.direction == BLEPeripherialCharDirection.both) {
      onData(await blechar!.read());
    }
    if (conf.direction == BLEPeripherialCharDirection.outbound && feat != null) {
      await onFeatureUpdated(feat!);
    }
  }

  void onData(List<int> data) {
    if (conf.type == BLEPeripheralCharType.int) {
      valueInt = data[0];
      valueStr = valueInt.toString();
    }
    if (conf.type == BLEPeripheralCharType.string) {
      valueInt = -1;
      valueStr = String.fromCharCodes(data);
    }
    if (feat != null &&
        [BLEPeripherialCharDirection.inbound, BLEPeripherialCharDirection.both].contains(conf.direction)) {
      feat!.set(valueStr);
    }
    lastState.updated = true;
  }

  Future<void> setupNotify() async {
    if (!blechar!.properties.notify) return;
    if (blechar!.isNotifying) return;
    await blechar!.setNotifyValue(true);
    blechar!.value.listen(onData);
  }
}

class BLEConnection {
  late BluetoothDevice dev;
  List<BLEConnectionLink> links = [];

  BLEConnection(BluetoothDevice d, BluetoothService serv) {
    dev = d;
    for (var ch in configuration.blePeripheralsService.values) {
      links.add(BLEConnectionLink(ch));
    }
    for (var ch in serv.characteristics) {
      BLEConnectionLink? link = getLinkByUUID(ch.uuid);
      if (link != null) {
        link.connectChar(ch);
      } else {}
    }
  }

  Future<void> setupAllCharacteristics() async {
    for (var l in links) {
      await l.syncBLE();
      await l.setupNotify();
    }
  }

  BLEConnectionLink? getLinkByUUID(Guid uuid) {
    for (var l in links) {
      if (l.uuid == uuid) return l;
    }
    return null;
  }
}
