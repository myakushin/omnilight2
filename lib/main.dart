import 'dart:async';
import 'dart:developer';
import 'dart:math' as math;

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:omnilight2/load.dart';
import 'package:omnilight2/model.dart';
import 'package:omnilight2/network.dart';
import 'package:package_info/package_info.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrcode_forked/qrcode_forked.dart';

import 'blescanner.dart';
import 'config.dart';
import 'events.dart';
import 'global.dart';
import 'inventory.dart';
import 'map.dart';
import 'master.dart';
import 'messages.dart';
import 'trade.dart';

void main() {
  loadManager = LoadManager();
  loadManager.startLoading();
  initEvents();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    if (!kIsWeb) {
      List<Permission> perms = [
        Permission.camera,
        Permission.notification,
//      Permission.ignoreBatteryOptimizations,
        Permission.locationWhenInUse,
//      Permission.locationAlways,
      ];
      perms.request().then((value) {
        for (var kv in value.entries) {
          if (!kv.value.isGranted) {
            lastState.addError(
                Error.system("Для корректной работы необходимо разрешние ${kv.key}. Но оно имеет статус ${kv.value} "));
          }
        }
      });
      /*  Permission.locationAlways.serviceStatus.then((value) {
      if (value.isDisabled)
        lastState.addError(Error.system(
            "Для корректной работы необходимо разрешние доступа к местоположению Но оно имеет статус $value "));
    });*/
    }
    return MaterialApp(
      title: "Omnilight",
      theme: ThemeData.dark(),
      home: const OmniHome(),
      routes: <String, WidgetBuilder>{
        "/login": (BuildContext ctx) => LoginMenu(),
        "/loginMaster": (BuildContext ctx) => MasterLoginMenu(),
        "/register": (BuildContext ctx) => RegistrationMenu(),
        "/charmenu": (BuildContext ctx) => const CharacterMenu(),
        "/character/selector": (BuildContext ctx) => const Selector(),
        "/character/crossclass": (BuildContext ctx) => const SelectCrossClass(),
        "/inventory": (BuildContext ctx) => InventoryMenu(),
        //Restures: PickResult
        "/inventory/pick": (BuildContext ctx) => InventoryPickMenu(),
        //Argument: FeatureDesc
        "/inventory/details": (BuildContext ctx) => InventoryDetails(),
        "/inventory/signature": (BuildContext ctx) => FileSignature(),
        "/trade": (BuildContext ctx) => const TradeMenu(),
        "/trade/deal": (BuildContext ctx) => const DealMenu(),
        "/trade/new": (BuildContext ctx) => const NewDealMenu(),
        "/trade/rest": (BuildContext ctx) => const RestaurantDeal(),
        "/qrscanner": (BuildContext ctx) => const QRScanner(),
        "/qractions": (BuildContext ctx) => const QRActionMenu(),
        "/qrtest": (BuildContext ctx) => const QRTest(),
        "/blescan": (BuildContext ctx) => const ScanMenu(),
        "/settings": (BuildContext ctx) => const SettingsWidget(),
        "/hacking": (BuildContext ctx) => const Hacking(),
        "/skills": (BuildContext ctx) => const Skills(),
        "/bleperipherals": (BuildContext ctx) => const BLEPeripheralsMenu(),
        "/chats": (BuildContext ctx) => const ChatsMenu(),
        "/chats/chat": (BuildContext ctx) => ChatMenu(),
        "/chats/masterNew": (BuildContext ctx) => const MasterChatNew(),
        "/orders": (BuildContext ctx) => const OrdersMenu(),
        "/map": (BuildContext ctx) => const MapMenu(),
        "/map/orderMenu": (BuildContext ctx) => const MapOrderQueueMenu(),
        "/map/addOrder": (BuildContext ctx) => const AddMapOrderMenu(),
        "/map/fleets": (BuildContext ctx) => const FleetMenu(),
        "/map/details": (BuildContext ctx) => const MapDetails(),
        "/map/instantOrder": (BuildContext ctx) => const InstantOrderMenu(),
        "/containers": (BuildContext ctx) => const ContainersMenu(),
        "/containers/detail": (BuildContext ctx) => const ContainerMenu(),
        "/dice": (BuildContext ctx) => const DiceMenu(),
        "/master/players": (BuildContext ctx) => const PlayersMenu(),
        "/master/player": (BuildContext ctx) => const PlayerMenu(),
        "/master/characters": (BuildContext ctx) => const CharactersMenu(),
        "/master/character": (BuildContext ctx) => const MasterCharacterMenu(),
        "/master/locations": (BuildContext ctx) => const LocationMenu(),
        "/master/location": (BuildContext ctx) => const MasterLocationMenu(),
        "/master/transitions": (BuildContext ctx) => const TransitionsList(),
        "/master/transition": (BuildContext ctx) => const TransitionDetail(),
        "/master/qrcode": (BuildContext ctx) => const MasterQRCodeDetail(),
        "/master/masssend": (BuildContext ctx) => const MasterMassSend(),
        "/master/selectto": (BuildContext ctx) => const SelectToMenu(),
      },
      navigatorKey: navigatorKey,
    );
  }
}

class OmniHome extends StatelessWidget {
  const OmniHome({super.key});

  @override
  Widget build(BuildContext context) {
    if (platform.hasForeground) checkForegroundService();
    return Scaffold(
      appBar: AppBar(title: const Text("Главное меню"), actions: <Widget>[
        loadIndicatorBuilder(),
        IconButton(
            icon: const Icon(
              Icons.qr_code,
            ),
            onPressed: () => captureQrCode(context)),
      ]),
      body: const Text("Выберете пункт действия из выпадащего меню, в левом верхнем углу экрана"),
      drawer: getCommonDrawer(context),
    );
  }
}

enum LoginState { nologin, authorize, loading, done, error }

class LoginMenu extends StatefulWidget {
  const LoginMenu({super.key});

  @override
  State<StatefulWidget> createState() => LoginMenuState();
}

class LoginMenuState extends State<LoginMenu> {
  final _loginController = TextEditingController();
  final _pwController = TextEditingController();
  LoginState state = LoginState.nologin;

  Future<void> login() async {
    lastState.logout();
    setState(() {
      state = LoginState.authorize;
    });
    try {
      await net.requestCookie(int.parse(_loginController.text), _pwController.text);
    } on DioError catch (e) {
      setState(() {
        state = LoginState.error;
      });
      return;
    }
    setState(() {
      state = LoginState.loading;
    });

    await lastState.loadData();
    setState(() {
      state = LoginState.done;
    });
    Navigator.of(navigatorKey.currentContext!).pushReplacementNamed("/");
  }

  @override
  Widget build(BuildContext context) {
    final RegistrationAnswer? answer = ModalRoute.of(context)!.settings.arguments as RegistrationAnswer?;
    if (answer != null) {
      _loginController.text = answer.pk.toString();
      _pwController.text = answer.password;
    }
    List<Widget> items = [
      TextFormField(
        controller: _loginController,
        decoration: const InputDecoration(hintText: "Введите ID пользователя"),
        inputFormatters: [FilteringTextInputFormatter.digitsOnly],
        keyboardType: TextInputType.number,
      ),
      TextFormField(
        decoration: const InputDecoration(hintText: "Введите пароль"),
        obscureText: answer == null,
        controller: _pwController,
      )
    ];
    if (state == LoginState.nologin) {
      items.add(
        ElevatedButton(onPressed: login, child: const Text("Войти")),
      );
    }
    if (state == LoginState.authorize) {
      items.add(const ListTile(title: Text("Авторизация....")));
    }
    if (state == LoginState.error) {
      items.add(const ListTile(
        title: Text("Ошибка входа. Логин или пароль неправильные",
            style: TextStyle(color: Color.fromARGB(255, 255, 0, 0))),
      ));
      items.add(
        ElevatedButton(onPressed: login, child: const Text("Войти")),
      );
    }
    if (state == LoginState.loading) {
      items.add(const ListTile(
        title: Text("Загрузка данных...."),
      ));
    }
    return Scaffold(
        appBar: AppBar(title: const Text("Введите данные для подключения к серверу")),
        drawer: getCommonDrawer(context),
        body: ListView(children: items));
  }
}

class MasterLoginMenu extends StatefulWidget {
  const MasterLoginMenu({super.key});

  @override
  State<StatefulWidget> createState() => MasterLoginMenuState();
}

class MasterLoginMenuState extends State<MasterLoginMenu> {
  final _loginController = TextEditingController();
  final _pwController = TextEditingController();
  LoginState state = LoginState.nologin;

  Future<void> login() async {
    lastState.logout();
    setState(() {
      state = LoginState.authorize;
    });
    try {
      await net.requestCookie(_loginController.text, _pwController.text, master: true);
    } on DioError catch (e) {
      setState(() {
        state = LoginState.error;
      });
      return;
    }
    setState(() {
      state = LoginState.loading;
    });

    await lastState.loadData();
    setState(() {
      state = LoginState.done;
    });
    Navigator.of(navigatorKey.currentContext!).pushReplacementNamed("/");
  }

  @override
  Widget build(BuildContext context) {
    final RegistrationAnswer? answer = ModalRoute.of(context)!.settings.arguments as RegistrationAnswer?;
    if (answer != null) {
      _loginController.text = answer.pk.toString();
      _pwController.text = answer.password;
    }
    List<Widget> items = [
      TextFormField(
        controller: _loginController,
        decoration: const InputDecoration(hintText: "Введите имя пользователя"),
      ),
      TextFormField(
        decoration: const InputDecoration(hintText: "Введите пароль"),
        obscureText: answer == null,
        controller: _pwController,
      )
    ];
    if (state == LoginState.nologin) {
      items.add(
        ElevatedButton(onPressed: login, child: const Text("Войти")),
      );
    }
    if (state == LoginState.authorize) {
      items.add(const ListTile(title: Text("Авторизация....")));
    }
    if (state == LoginState.error) {
      items.add(const ListTile(
        title: Text("Ошибка входа. Логин или пароль неправильные",
            style: TextStyle(color: Color.fromARGB(255, 255, 0, 0))),
      ));
      items.add(
        ElevatedButton(onPressed: login, child: const Text("Войти")),
      );
    }
    if (state == LoginState.loading) {
      items.add(const ListTile(
        title: Text("Загрузка данных...."),
      ));
    }
    return Scaffold(
        appBar: AppBar(title: const Text("Введите данные для подключения к серверу")),
        drawer: getCommonDrawer(context),
        body: ListView(children: items));
  }
}

class RegistrationMenu extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _nicknameController = TextEditingController();
  final _nameController = TextEditingController();
  final _charnameController = TextEditingController();

  RegistrationMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text("Введите данные регистрации")),
        body: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(hintText: "Введите имя"),
                ),
                TextFormField(
                  decoration: const InputDecoration(hintText: "Введите никнейм"),
                  controller: _nicknameController,
                ),
                TextFormField(
                  decoration: const InputDecoration(hintText: "Введите имя пресонажа"),
                  controller: _charnameController,
                ),
                Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: StreamBuilder<StateStore?>(
                      initialData: lastState,
                      stream: stateStreamController.stream,
                      builder: (BuildContext ctx, AsyncSnapshot<StateStore?> state) => ElevatedButton(
                        child: const Text("Зарегистрироваться"),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            net
                                .register(_nameController.text, _nicknameController.text, _charnameController.text, "")
                                .then((ans) {
                              Navigator.of(context).pushNamed("/login", arguments: ans);
                            });
                          }
                        },
                      ),
                    ))
              ],
            )));
  }
}

class CharacterMenu extends StatelessWidget {
  static const TextStyle style = TextStyle(fontSize: 16.0);

  const CharacterMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          actions: [loadIndicatorBuilder()],
          title: const Text("Карточка персонажа"),
        ),
        drawer: getCommonDrawer(context),
        body: StreamBuilder<StateStore?>(
          initialData: lastState,
          stream: stateStreamController.stream,
          builder: (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
            if (state.data == null) {
              return const Text("Нет данных");
            }
            List<Widget> elems = [];
            if (state.data!.char != null) {
              for (CharPropertyBase prop in configuration.charProperty) {
                Widget? w = prop.getWidget(state.data!, ctx);
                //getWidget может вернуть null если ничего рисовать не надо
                if (w != null) elems.add(w);
              }
            }

            return ListView(children: elems);
          },
        ));
  }

//
}

class Selector extends StatelessWidget {
  const Selector({super.key});

  @override
  Widget build(BuildContext context) {
    CharPropertySelector prop =
        ModalRoute.of(context)?.settings.arguments as CharPropertySelector? ?? CharPropertySelector.undefined();

    return Scaffold(
        appBar: AppBar(
          actions: [loadIndicatorBuilder()],
          title: Text("Выбор ${prop.label}"),
        ),
        drawer: getCommonDrawer(context),
        body: StreamBuilder<StateStore?>(
            initialData: lastState,
            stream: stateStreamController.stream,
            builder: (BuildContext ctx, AsyncSnapshot<StateStore?> state) {
              if (state.data == null) {
                return const Text("Нет данных");
              }
              List<Widget> elems = [];
              List<String> path = prop.transitionPrefix.split("-");
              log("Loading races from $path}");
              for (TransitionDesc tr in state.data?.transTree.getElement(path)?.getList() ?? []) {
                late Widget trailer;
                if (tr.isLoading()) {
                  trailer = const CircularProgressIndicator();
                } else {
                  if (tr.passed) {
                    trailer = ElevatedButton(
                        onPressed: () {
                          tr.apply();
                          Navigator.of(context).pop();
                        },
                        child: const Text("Взять"));
                  } else {
                    trailer = Text(tr.getFailedCmp());
                  }
                }
                elems.add(ListTile(
                  title: Text(tr.title),
                  subtitle: Text(tr.description),
                  trailing: trailer,
                ));
              }

              return ListView(
                children: elems,
              );
            }));
  }
}

class QRScanner extends StatefulWidget {
  const QRScanner({super.key});

  @override
  State<StatefulWidget> createState() => QRScannerState();
}

class QRScannerState extends State<QRScanner> with TickerProviderStateMixin {
  final QRCaptureController _captureController = QRCaptureController();
  late Animation<Alignment> _animation;
  late AnimationController _animationController;
  bool isCaptured = false;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(vsync: this, duration: const Duration(seconds: 1));
    _animation = AlignmentTween(begin: Alignment.topCenter, end: Alignment.bottomCenter).animate(_animationController);
    _animation.addListener(() {
      setState(() {});
    });
    _animation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _animationController.forward();
      }
    });
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _captureController.onCapture((str) => captured(context, str));
    Image? imgbg;
    if (configuration.assets.containsKey("sao.png")) {
      imgbg = Image.file(configuration.assets["sao.png"]!.file);
    }
    List<Image> imgline = [];
    if (configuration.assets.containsKey("tiao.png")) {
      imgline.add(Image.file(configuration.assets["tiao.png"]!.file));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Сканируем QR"),
      ),
      body: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          QRCaptureView(
            controller: _captureController,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 56),
            child: AspectRatio(
              aspectRatio: 254 / 258.0,
              child: imgbg,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 56),
            child: AspectRatio(
              aspectRatio: 254 / 258.0,
              child: Stack(
                alignment: _animation.value,
                children: imgline,
              ),
            ),
          )
        ],
      ),
    );
  }

  void captured(BuildContext ctx, String str) {
    if (isCaptured) return;
    QRScannerResult res = QRScannerResult();
    if (str.length != 7) {
      log("QRCode check error: string $str not 7 letters length");
      return;
    }
    if (!str.startsWith(RegExp('[a-z]'))) {
      log("QR code check  error: code shut be start from letter $str  ");
      return;
    }
    res.code = str[0];
    res.number = int.tryParse(str.substring(1));
    if (res.number == null) {
      log("QR code check error: can`t parse number $str");
      return;
    }
    isCaptured = true;
    Navigator.of(ctx).pop(res);
  }
}

class QRActionMenu extends StatelessWidget {
  const QRActionMenu({super.key});

  @override
  Widget build(BuildContext context) {
    QRScannerResult res = ModalRoute.of(context)!.settings.arguments as QRScannerResult;

    return Scaffold(
      appBar: AppBar(title: Text("Дейсвтия с ${res.name}")),
      body: StreamBuilder(
        stream: res.actions,
        builder: (BuildContext ctx, AsyncSnapshot<List<QRAction>> snapshot) {
          List<Widget> widgets = [];
          if (snapshot.data == null) {
            widgets.add(ListTile(
                leading: const Icon(Icons.error),
                title: Text("Не могу найти действия для кода ${res.code} ${res.number}")));
          } else {
            for (QRAction act in snapshot.data!) {
              if (act.description != null) {
                widgets.add(ListTile(
                  title: Text(act.description!),
                ));
              } else {
                widgets.add(ElevatedButton(
                  onPressed: (act.func == null ? null : () => act.func!(res.number!, context)),
                  child: Text(act.name),
                ));
              }
            }
          }

          return ListView(children: widgets);
        },
      ),
    );
  }
}

class QRTest extends StatefulWidget {
  const QRTest({super.key});

  @override
  State<StatefulWidget> createState() {
    return QRTestState();
  }
}

class QRTestState extends State<QRTest> {
  String letterNow = "";
  TextEditingController edit = TextEditingController();

  @override
  Widget build(BuildContext context) {
    List<DropdownMenuItem<String>> items = [];
    var letters = configuration.qrActions.keys;
    for (String f in letters) {
      items.add(DropdownMenuItem(
        value: f,
        child: Text(f),
      ));
    }
    if (letterNow.isEmpty) letterNow = letters.first;
    return Scaffold(
        appBar: AppBar(
          title: const Text("Тест qr кодов"),
        ),
        drawer: getCommonDrawer(context),
        body: ListView(
          children: [
            const ListTile(title: Text("Буква QR кода")),
            DropdownButton<String>(
                value: letterNow,
                items: items,
                onChanged: (n) {
                  setState(() {
                    letterNow = n ?? "";
                  });
                }),
            const ListTile(title: Text("Цифра QR кода")),
            TextField(
              controller: edit,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
            ),
            ElevatedButton(
                onPressed: () {
                  var res = QRScannerResult();
                  res.code = letterNow;
                  res.number = int.tryParse(edit.text) ?? 0;
                  if (configuration.isMaster) {
                    Navigator.of(context).pushNamed("/master/qrcode", arguments: res);
                  } else {
                    configuration.qrActions[res.code!]?.updateQResult(res);
                    Navigator.of(context).pushNamed("/qractions", arguments: res);
                  }
                },
                child: const Text("Проверить"))
          ],
        ));
  }
}

class SettingsWidget extends StatelessWidget {
  const SettingsWidget({super.key});

  void onExit(BuildContext ctx) {
    showDialog(
        context: ctx,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Вы действительно хотите выйти?"),
            actions: <Widget>[
              TextButton(
                child: const Text("Да"),
                onPressed: () {
                  net.resetCookie();
                  lastState.reset();
                  loadManager.startLoading();
                },
              ),
              TextButton(
                child: const Text("Нет"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  void onLogout(BuildContext ctx) {
    showDialog(
        context: ctx,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Вы действительно хотите  сменить пользователя?"),
            actions: <Widget>[
              TextButton(
                child: const Text("Да"),
                onPressed: () {
                  net.resetCookie();
                  lastState.logout();
                  Navigator.of(context).pushReplacementNamed("/");
                },
              ),
              TextButton(
                child: const Text("Нет"),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> elements = [];
    elements.add(ListTile(
      title: const Text("Версия приложения"),
      subtitle: StreamBuilder<PackageInfo?>(
        stream: PackageInfo.fromPlatform().asStream(),
        builder: (BuildContext ctx, AsyncSnapshot<PackageInfo?> state) {
          if (!state.hasData || state.data == null) {
            return const Text("Неизвестно");
          }
          PackageInfo p = state.data!;
          return Text("${p.version} ${p.buildNumber}");
        },
      ),
    ));

    elements.add(ListTile(
      title: Text("Префикс игры ${configuration.prefsPrefix}"),
      subtitle: Text("Serial ${configuration.serial}"),
    ));

    for (var s in configuration.servers) {
      Widget caption;
      if (s.isActive) {
        caption = const Text("Активный сервер");
      } else {
        caption = const Text("Сервер");
      }
      elements.add(ListTile(
        title: caption,
        subtitle: Text("${s.path} rq ${s.ok}/${s.error} "),
      ));
    }
    elements.add(ListTile(title: Text("${net.webSocketStatus.toString()} ${net.webSocket?.readyState}")));
    if (kDebugMode) {
      elements.add(ListTile(
        title: const Text("===Тест ==="),
        onTap: () {
          var res = QRScannerResult();
          res.code = "a";
          res.number = 1;
          configuration.qrActions[res.code!]?.updateQResult(res);
          Navigator.of(context).pushNamed("/qractions", arguments: res);

          //net.addToQueue("GET", ["wrong", "path"], "");
        },
      ));
      elements.add(ListTile(
        title: const Text("===Тест2 ==="),
        onTap: () {
          var res = QRScannerResult();
          res.code = "m";
          res.number = 1;
          configuration.qrActions[res.code!]?.updateQResult(res);
          Navigator.of(context).pushNamed("/qractions", arguments: res);
        },
      ));
    }
    elements.add(ListTile(
      leading: const Icon(Icons.exit_to_app),
      title: const Text("Разлогиниться"),
      onTap: () => onLogout(context),
    ));
    elements.add(ListTile(
      leading: const Icon(Icons.exit_to_app),
      title: const Text("Выйти из игры"),
      onTap: () => onExit(context),
    ));
    return Scaffold(
        appBar: AppBar(title: const Text("Настройки")),
        body: ListView(
          children: elements,
        ));
  }
}

class Hacking extends StatefulWidget {
  const Hacking({super.key});

  @override
  State<StatefulWidget> createState() => HackingState();
}

var _random = math.Random();

class HackingElement {
  late double x, y;
  late int img;
  bool open = false;
  bool locked = false;
  late HackingElement peer;

  HackingElement.generate(int i) {
    img = i;
    x = 30 + _random.nextInt(300).toDouble();
    y = 10 + _random.nextInt(500).toDouble();
  }
}

enum HackingResult { DONE, CANCELED, TIMEOUT }

//TODO: рефактор логики взлома
class HackingRequest {
  Function? onDone;
  DateTime? timeout;

  HackingRequest.def() {
    onDone = null;
  }

  HackingRequest.timeout(int seconds) {
    timeout = DateTime.now().add(Duration(seconds: seconds));
  }

  HackingRequest(Function f) {
    onDone = f;
  }

  void done() {
    if (onDone != null) onDone!();
  }
}

class HackingState extends State<Hacking> {
  List<HackingElement> elements = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 5; i++) {
      var a = HackingElement.generate(i);
      var b = HackingElement.generate(i);
      a.peer = b;
      b.peer = a;
      elements.add(a);
      elements.add(b);
    }
  }

  void end(BuildContext context, HackingResult result, HackingRequest request) {
    String caption = "";
    String button = "";
    if (result == HackingResult.DONE) {
      caption = "Взлом удался";
      button = "Ура!";
    } else if (result == HackingResult.TIMEOUT) {
      caption = "Взлом НЕ удался";
      button = "понятно";
    }
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text(caption),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(ctx).pop();
                    Navigator.of(ctx).pop(result);
                    if (result == HackingResult.DONE) request.done();
                  },
                  child: Text(button))
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    HackingRequest request = ModalRoute.of(context)!.settings.arguments as HackingRequest? ?? HackingRequest.def();
    var background = Image.asset(
      "images/background.png",
    );
    var unknown = Image.asset(
      "images/unknown.png",
      scale: 2,
    );
    var known = [
      Image.asset(
        "images/Antenna.png",
        scale: 2,
      ),
      Image.asset(
        "images/capasitor.png",
        scale: 2,
      ),
      Image.asset(
        "images/cerber.png",
        scale: 2,
      ),
      Image.asset(
        "images/ground.png",
        scale: 2,
      ),
      Image.asset(
        "images/resistor.png",
        scale: 2,
      ),
    ];

    bool isSuccess() {
      for (var e in elements) {
        if (!e.locked) return false;
      }
      return true;
    }

    List<Widget> widgets = [background];
    for (var e in elements) {
      widgets.add(Padding(
        padding: EdgeInsets.fromLTRB(e.x, 0, 0, e.y),
        child: GestureDetector(
            onTap: () {
              setState(() {
                if (e.peer.open) {
                  e.peer.locked = true;
                  e.locked = true;
                }
                elements.forEach((element) {
                  element.open = element.locked;
                });
                e.open = true;
                if (isSuccess()) {
                  end(context, HackingResult.DONE, request);
                }
              });
            },
            child: e.open ? known[e.img] : unknown),
      ));
    }
    String header = "Взлом";
    if (request.timeout != null) {
      Duration delta = request.timeout!.difference(DateTime.now());

      header += " (осталось ${delta.inSeconds} c)";
      Timer(
          const Duration(seconds: 1),
          () => setState(() {
                if (delta.inSeconds <= 0) {
                  end(context, HackingResult.TIMEOUT, request);
                  request.timeout = null;
                }
              }));
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(header),
      ),
      drawer: getCommonDrawer(context),
      body: Stack(
        alignment: Alignment.bottomLeft,
        children: widgets,
      ),
    );
  }
}

class SkillContext {
  late List<String> path;

  SkillContext.empty() {
    path = [];
  }

  SkillContext(List<String> p) {
    path = p;
  }
}

//TODO: где то должна отображаться стоимость скила
class Skills extends StatelessWidget {
  const Skills({super.key});

  List<Widget> generateSkillButtons(List<TransitionDesc> trans) {
    List<Widget> widgets = [];
    for (TransitionDesc? t in trans) {
      Widget? traling;
      if (!t!.isLoading()) {
        if (t.passed) {
          traling = ElevatedButton(onPressed: () => t.apply(), child: const Text("Взять"));
        } else {
          traling = null; //Text(t.getFailedCmp(sep: "\n"));
        }
      } else {
        traling = const CircularProgressIndicator();
      }
      String title = t.title;
      if (!t.passed) {
        title += "(невозможно взять: ${t.getFailedCmp()})";
      }
      widgets.add(ListTile(
        title: Text(title),
        subtitle: Text(t.description),
        trailing: traling,
      ));
    }
    return widgets;
  }

  @override
  Widget build(BuildContext context) {
    SkillContext ctx = ModalRoute.of(context)!.settings.arguments as SkillContext? ?? SkillContext.empty();
    return StreamBuilder<StateStore?>(
        initialData: lastState,
        stream: stateStreamController.stream,
        builder: (BuildContext context, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];
          String classFeature = configuration.skills.classFeature;
          String crossClassFeature = configuration.skills.crossClassFeature;

          if (classFeature == "" || ctx.path.isNotEmpty) {
            var prefix = [configuration.skills.transPrefix];

            if (ctx.path.isNotEmpty) prefix = ctx.path;

            var trans = shot.data!.transTree.getElement(prefix)?.getList();
            widgets = generateSkillButtons(trans ?? []);
          } else {
            widgets += getSkillWidgets(shot.data!, context, classFeature, "", configuration.skills.transPrefix);

            if (crossClassFeature != "") {
              widgets += getSkillWidgets(shot.data!, context, crossClassFeature, " (дополнительный)",
                  configuration.skills.crossSkillGetPrefix);
            }
          }

          if (widgets.isEmpty) {
            widgets.add(const ListTile(title: Text("Нет доступных навыков")));
          }

          var fd = shot.data!.char!.featuresByTypeName[configuration.skills.expFeature]?.first;

          String exp = "";
          if (fd != null) exp = "${fd.valueStr} ед. опыта свободно";

          return Scaffold(
              appBar: AppBar(
                title: Text("Навыки $exp"),
              ),
              drawer: getCommonDrawer(context),
              body: ListView(children: widgets));
        });
  }

  List<Widget> getSkillWidgets(
      StateStore state, BuildContext context, String classFeature, String suffix, String transPrefix) {
    List<Widget> widgets = [];
    List<String> classes = state.char!.featuresByTypeName[classFeature]?.map((e) => e.valueStr).toList() ?? [];

    TreeElement<String, TransitionDesc>? skills = state.transTree.getElement([transPrefix]);

    if (skills == null) return [];

    for (var kv in skills.trunk!.entries) {
      if (!classes.contains(kv.key)) continue;

      widgets.add(ListTile(
        title: Text(kv.key + suffix),
      ));

      for (var skills in kv.value.trunk!.entries) {
        widgets.add(ElevatedButton(
            child: Text(skills.key),
            onPressed: () {
              List<String> path = [transPrefix, kv.key, skills.key];
              Navigator.of(context).pushNamed("/skills", arguments: SkillContext(path));
            }));
      }
    }
    return widgets;
  }
}

class SelectCrossClass extends StatelessWidget {
  const SelectCrossClass({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Выберете доп класс")),
      drawer: getCommonDrawer(context),
      body: StreamBuilder(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];

          if (configuration.skills.crossTransPrefix == "") {
            return const Text("В данной конфигурации нет дополнительных классов");
          }
          List<String> prefixPath = configuration.skills.crossTransPrefix.split("-");
          var tree = shot.data!.transTree.getElement(prefixPath);
          if (tree == null) return const Text("Доступных классов не обнаружено");
          for (var classTrans in tree.getList()) {
            late Widget button;
            if (classTrans.isLoading()) {
              button = const CircularProgressIndicator();
            } else {
              if (classTrans.passed) {
                button = ElevatedButton(onPressed: () => classTrans.apply(), child: const Text("Взять"));
              } else {
                button = Text("Невозможно " + classTrans.getFailedCmp());
              }
            }

            widgets
                .add(ListTile(title: Text(classTrans.title), subtitle: Text(classTrans.description), trailing: button));
          }

          return ListView(children: widgets);
        },
      ),
    );
  }
}

class BLEPeripheralsMenu extends StatelessWidget {
  const BLEPeripheralsMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Оборудование"),
      ),
      drawer: getCommonDrawer(context),
      body: StreamBuilder(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> shot) {
          List<Widget> widgets = [];
          if (configuration.blePeripheralsUUID == "") {
            return const Text("Внешнеее устройство не настроено");
          }
          StateStore state = shot.data!;
          if (state.bleConnection == null) {
            if (state.bleDeviceMAC == null || state.bleDeviceMAC!.valueStr.isEmpty) {
              widgets.add(ListTile(
                title: const Text("Соедение не установлено"),
                trailing: ElevatedButton(
                  child: const Text("Соеденить"),
                  onPressed: () => Navigator.of(context).pushNamed("/blescan"),
                ),
              ));
            } else {
              widgets.add(ListTile(
                title: Text("Нет соединения с ${state.bleDeviceMAC!.valueStr}"),
                trailing: ElevatedButton(
                  child: const Text("Поиск другого"),
                  onPressed: () => Navigator.of(context).pushNamed("/blescan"),
                ),
              ));
            }
            return ListView(children: widgets);
          }

          widgets.add(ListTile(title: Text("Соединение с ${state.bleConnection!.dev.id}")));
          for (var ch in state.bleConnection!.links) {
            String text = ch.conf.label;

            text += " " + ch.valueStr;

            if (ch.feat != null) {
              text += " " + ch.feat!.valueStr;
            }

            widgets.add(ListTile(title: Text(text)));
          }
          return ListView(children: widgets);
        },
      ),
    );
  }
}

class OrdersMenu extends StatelessWidget {
  const OrdersMenu({super.key});

  Widget getTransitionWidget(Order order) {
    TransitionDesc? trans = lastState.transitions[order.transitionName];
    if (trans != null) {
      if (trans.isLoading()) {
        return const CircularProgressIndicator();
      } else {
        if (trans.passed) {
          return ElevatedButton(
              child: Text(order.label),
              onPressed: () async {
                await trans.apply();
              });
        } else {
          return ElevatedButton(onPressed: null, child: Text("${order.label} [${trans.getFailedCmp()}])"));
        }
      }
    } else {
      return ElevatedButton(onPressed: null, child: Text("${order.label} [не найдено]"));
    }
  }

  List<Widget> getOrderGroupWidgets(OrderGroup group, Map<String, bool> defaults) {
    if (group.type == OrderGroupType.transition) {
      return group.orders.map(getTransitionWidget).toList();
    }
    List<Widget> ret = [];
    if (group.type == OrderGroupType.decisionDefault) {
      for (FeatureDesc feat in lastState.char!.featuresByTypeName[group.defaultFeature] ?? []) {
        String label = group.defaultLabel;
        label += feat.valueSimple.getFirstObjectName("system") ?? "";
        bool now = feat.valueSimple.getFirstObjectBool("answer") ?? false;
        ret.add(SwitchListTile(
          title: Text(label),
          subtitle: Text(group.defaultSublabel),
          value: now,
          onChanged: (bool value) async {
            feat.valueSimple.setFirstObjectName("answer", value ? "yes" : "no");
            await feat.setData();
          },
        ));
        return ret;
      }
    }
    if (group.type == OrderGroupType.decision) {
      for (FeatureDesc feat in lastState.char!.featuresByTypeName[group.decisionFeature] ?? []) {
        String label = feat.valueSimple.replaceByFields(group.decisionLabel);
        bool? ans = feat.valueSimple.getFirstObjectBool("answer");
        if (ans == null) {
          String? ansStr = feat.valueSimple.getFirstObjectName("answer");
          if (ansStr == null) {
            ans = defaults[feat.valueSimple.getFirstObjectName("system") ?? ""];
          } else {
            if (ansStr == "passed") {
              ret.add(ListTile(
                title: Text(label),
                trailing: const Text("Прошел"),
              ));
              continue;
            }
            if (ansStr == "denied") {
              ret.add(ListTile(
                title: Text(label),
                trailing: const Text("ОТКАЗАНО"),
              ));
              continue;
            }
          }
        }
        ret.add(SwitchListTile(
          title: Text(label),
          value: ans ?? false,
          onChanged: (bool value) async {
            feat.valueSimple.setFirstObjectName("answer", value ? "yes" : "no");
            await feat.setData();
          },
        ));
      }
      return ret;
    }
    return [];
  }

  Map<String, bool> loadDefaults() {
    Map<String, bool> ret = {};
    for (var g in configuration.ordergorups) {
      if (g.type != OrderGroupType.decisionDefault) continue;
      for (FeatureDesc feat in lastState.char!.featuresByTypeName[g.defaultFeature] ?? []) {
        bool val = feat.valueSimple.getFirstObjectBool("answer") ?? false;
        String system = feat.valueSimple.getFirstObjectName("system") ?? "";
        ret[system] = val;
      }
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          if (!snap.hasData) return const Text("Нет доступных данных");
          var defaults = loadDefaults();
          return DefaultTabController(
              length: configuration.ordergorups.length,
              child: Scaffold(
                  appBar: AppBar(
                    title: const Text("Приказы"),
                    actions: [loadIndicatorBuilder()],
                    bottom: TabBar(
                        isScrollable: true,
                        tabs: configuration.ordergorups.map((og) => Tab(text: og.label)).toList(growable: false)),
                  ),
                  drawer: getCommonDrawer(context),
                  body: TabBarView(
                      children: configuration.ordergorups
                          .map((e) => ListView(children: getOrderGroupWidgets(e, defaults)))
                          .toList(growable: false))));
        });
  }
}

class DiceMenu extends StatefulWidget {
  const DiceMenu({super.key});

  @override
  State<StatefulWidget> createState() => DiceMenuState();
}

class DiceMenuState extends State<DiceMenu> {
  int dice = 0;
  int val = 0;
  List<int> prob = [];

  DiceMenuState() {
    String pfName = configuration.dice.probabilityFeature;
    FeatureTypeDesc? pf = lastState.featureTypesByName[pfName];
    if (pf != null) {
      String val = pf.values.values.first;
      prob = val.split(" ").map((s) => int.tryParse(s) ?? 0).toList();
    }
  }

  getProb(int val) {
    int i = 1;
    for (var p in prob) {
      if (p > val) return i;
      i++;
    }
    return i;
  }

  void onUp(PointerUpEvent event) {
    if (dice != 0) return;

    if (prob.isNotEmpty) {
      setState(() {
        val = random.nextInt(100);
        dice = getProb(val);
      });
    } else {
      setState(() {
        dice = random.nextInt(configuration.dice.images.length);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> col = [];
    String assetName = configuration.dice.images[dice];
    Asset? diceasset = configuration.assets[assetName];
    if (diceasset == null) {
      col.add(Text("Не могу найти asset $assetName"));
    } else {
      col.add(Image.file(diceasset.file));
    }
    if (dice == 0) {
      col.add(const Text("Нажмите на кубик для броска"));
    }
    if (kDebugMode) {
      col.add(Text(prob.join(" ")));
      col.add(Text("val=$val"));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Бросок кубика"),
      ),
      drawer: getCommonDrawer(context),
      body: Listener(
          onPointerUp: onUp,
          child: Column(
            children: col,
          )),
    );
  }
}
