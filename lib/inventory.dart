import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:omnilight2/main.dart';

import 'global.dart';
import 'model.dart';

enum InventoryMode { List, Pick }

class InventorySettings {
  InventoryMode mode = InventoryMode.List;
  Set<int> resourceFilter = Set();
  Set<int> featureFilter = Set();
  String label = configuration.getMenuElementName("/inventory") ?? "Инвентарь";
  List<FeatureDesc> features = lastState.char!.features;
  String addToDealButtonText = "Добавить в сделку";
}

class InventoryMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InventoryMenuState();
  }
}

class PickResult {
  final Set<int> checked = Set();
  final Map<int, int> amount = Map();
}

class InventoryPickMenu extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InventoryPickState();
  }
}

class InventoryPickRequest {
  InventoryPickRequest(int? f, int? v) {
    feature = f;
    value = v;
  }

  int? feature;
  int? value = 0;
}

class InventoryPickState extends State<InventoryPickMenu> {
  final TextEditingController txtControl = TextEditingController();

  @override
  Widget build(BuildContext context) {
    InventoryPickRequest? request = ModalRoute.of(context)!.settings.arguments as InventoryPickRequest?;
    if (request == null) {
      return const Text("Не выбран желаемый элемент");
    }
    var ft = lastState.featureTypesByID[request.feature];
    if (ft == null) return Text("Нет такого элемента ${request.feature}");
    if (txtControl.text == "") txtControl.text = request.value.toString();

    return Scaffold(
      appBar: AppBar(
        title: const Text("Укажите нужное количество"),
      ),
      drawer: getCommonDrawer(context),
      body: ListView(
        children: <Widget>[
          Text("Выберете желаемое количество ${ft.name}"),
          TextField(keyboardType: const TextInputType.numberWithOptions(decimal: true), controller: txtControl),
          ElevatedButton(
            child: const Text("Сохранить"),
            onPressed: () {
              var ret = int.tryParse(txtControl.text);
              Navigator.of(context).pop(ret);
            },
          )
        ],
      ),
    );
  }
}

class InventoryMenuState extends State<InventoryMenu> {
  final PickResult pickResult = PickResult();

  Widget getPickInteger(BuildContext ctx, InventorySettings? settings, StateStore store, FeatureDesc ft, FeatureTypeDesc typeDesc) {
    if (!pickResult.amount.containsKey(ft.type)) pickResult.amount[ft.type] = 0;
    return ListTile(
      title: Text(typeDesc.name),
      subtitle: Text("Всего: ${ft.valueInt} добавлено: ${pickResult.amount[ft.type]}"),
      trailing: IconButton(
        icon: const Icon(Icons.edit),
        onPressed: () {
          Navigator.of(ctx).pushNamed("/inventory/pick", arguments: InventoryPickRequest(ft.type, 0)).then((nw) {
            int? n = nw as int?;
            if (n != null) {
              setState(() {
                pickResult.amount[ft.type] = n;
              });
            }
          });
        },
      ),
    );
  }

  Widget getPickCheckbox(FeatureTypeDesc tp, FeatureDesc ft) {
    return CheckboxListTile(
        value: pickResult.checked.contains(ft.resource),
        onChanged: (n) {
          setState(() {
            if (n!) {
              pickResult.checked.add(ft.resource);
            } else {
              pickResult.checked.remove(ft.resource);
            }
          });
        },
        title: Text("${tp.name}: ${ft.valueStr}"));
  }

  Map<String, List<FeatureDesc>> sortFeatures(InventorySettings settings, StateStore state) {
    Map<String, List<FeatureDesc>> ret = Map();
    for (FeatureDesc feat in settings.features) {
      FeatureTypeDesc ft = feat.typeDesc;
      if (!ft.tradable) continue;
      if (ft.name.endsWith("Token")) continue;

      if (!ret.containsKey(ft.group)) ret[ft.group] = [];
      List<FeatureDesc> group = ret[ft.group]!;
      group.add(feat);
    }
    for (var group in ret.values) {
      group.sort((f1, f2) {
        var e1 = configuration.getInventoryElement(f1.name);
        var e2 = configuration.getInventoryElement(f2.name);
        return e1.getTitleText(f1, state).compareTo(e2.getTitleText(f2, state));
      });
    }
    return ret;
  }

  List<Widget> mapTiles(BuildContext context, InventorySettings settings, List<FeatureDesc> features, StateStore state) {
    List<Widget> ret = [];

    for (FeatureDesc feat in features) {
      FeatureTypeDesc ft = feat.typeDesc;
      if (settings.mode == InventoryMode.List) {
        ret.add(configuration.getInventoryElement(feat.name).getListTile(context, feat, state));
      }
      if (settings.mode == InventoryMode.Pick) {
        if (settings.resourceFilter.contains(feat.resource)) continue;
        if (settings.featureFilter.contains(feat.type)) continue;
        if (ft.integer) {
          ret.add(getPickInteger(context, settings, state, feat, ft));
        } else {
          ret.add(getPickCheckbox(ft, feat));
        }
      }
    }

    return ret;
  }

  Widget? getBottom(InventorySettings settings, BuildContext context) {
    if (settings.mode == InventoryMode.Pick) {
      return BottomAppBar(
          child: ElevatedButton(
              child: Text(settings.addToDealButtonText),
              onPressed: () {
                Navigator.of(context).pop(pickResult);
              }));
    }
    if (configuration.createFiles && settings.mode == InventoryMode.List) {
      return BottomAppBar(
        child: ElevatedButton(
          child: const Text("Создать файл"),
          onPressed: () async {
            FeatureDesc? feat = lastState.char!.findFeatureByStrValue("File", "--NEW-FILE--");
            if (feat == null) {
              TransitionDesc? trans = lastState.transitions["Get-File"];
              await trans?.apply();
              feat = lastState.char!.findFeatureByStrValue("File", "--NEW-FILE--");
            }
            if (feat != null) {
              Navigator.of(context).pushNamed("/inventory/details", arguments: feat);
            }
          },
        ),
      );
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    InventorySettings settings =
        ModalRoute.of(context)!.settings.arguments as InventorySettings? ?? InventorySettings(); //Load default settings
    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          if (!snap.hasData) return const Text("Нет доступных данных");

          var features = sortFeatures(settings, snap.data!);

          return DefaultTabController(
              length: features.length,
              child: Scaffold(
                  appBar: AppBar(
                    title: Text(settings.label),
                    actions: [loadIndicatorBuilder()],
                    bottom: TabBar(
                        isScrollable: true, tabs: features.keys.map((name) => Tab(text: name)).toList(growable: false)),
                  ),
                  drawer: getCommonDrawer(context),
                  bottomNavigationBar: getBottom(settings, context),
                  body: TabBarView(
                      children: features.values
                          .map((e) => ListView(children: mapTiles(context, settings, e, snap.data!)))
                          .toList(growable: false))));
        });
  }
}

class InventoryDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FeatureDesc? feature = ModalRoute.of(context)!.settings.arguments as FeatureDesc?;

    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          if (!snap.hasData) return const Text("Нет доступных данных");
          if (feature == null) return const Text("Нет объекта для отображения");
          var element = configuration.getInventoryElement(feature.typeDesc.name);
          return Scaffold(
            appBar: AppBar(
              title: const Text("Инвентарь"),
              actions: [loadIndicatorBuilder()],
            ),
            drawer: getCommonDrawer(context),
            body: element.getDetail(context, snap.data, feature),
            bottomNavigationBar: element.getBottom(ctx, feature),
          );
        });
  }
}

abstract class InventoryElementBase {
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Text(feature.typeDesc.name),
        ),
        ListTile(
          title: Text(feature.valueStr),
        ),
      ],
    );
  }

  Widget getListTile(BuildContext ctx, FeatureDesc feature, StateStore state) {
    return ListTile(
      title: Text(getTitleText(feature, state)),
      onTap: () => Navigator.of(ctx).pushNamed("/inventory/details", arguments: feature),
    );
  }

  String getTitleText(FeatureDesc feature, StateStore state) {
    return "";
  }

  Widget? getBottom(BuildContext ctx, FeatureDesc feature) {
    return null;
  }
}

class InventoryElementDefault extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    return "${feature.typeDesc.name}: ${feature.valueStr}";
  }
}

class InventoryElementReport extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    String display = feature.valueStr.trim();
    int len = display.indexOf("\n");
    if (len == -1) len = display.length;
    display = display.substring(0, len).trim();
    return "Отчет: $display";
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    return ListView(
      children: <Widget>[
        const ListTile(
          title: Text("Отчет"),
        ),
        ListTile(
          title: Text(feature.valueStr.trim()),
        ),
      ],
    );
  }
}

class InventoryElementContact extends InventoryElementBase {
  Future<void> hack(BuildContext ctx, int targetid, int offlevel) async {
    int deflevel = await lastState.getHackDefensiveLevel(targetid);
    if (deflevel == -1) {
      lastState.addError(Error.system("Не удалось выяснить уровень защиты"));
      return;
    }
    if (deflevel > offlevel) {
      lastState.addError(Error.system("Недостаточный уровень для взлома"));
      return;
    }

    HackingRequest hr = HackingRequest.timeout((offlevel - deflevel) * 30);
    var miniGameObj = await Navigator.of(ctx).pushNamed("/hacking", arguments: hr);
    if (miniGameObj != HackingResult.DONE) {
      return;
    }
    List<FeatureDesc> items = await lastState.hackInspect(targetid);
    InventorySettings settings = InventorySettings();
    settings.features = items;
    settings.mode = InventoryMode.Pick;
    settings.label = "Инвентарь цели";
    settings.addToDealButtonText = "Украсть";
    var ret = await Navigator.of(ctx).pushNamed("/inventory", arguments: settings);
    PickResult? result = ret as PickResult?;
    if (result == null) return;
    bool hackres = await lastState.doHack(targetid, result);
    String restext = "";
    if (hackres) {
      restext = "Взлом успешен. ${result.amount.length + result.checked.length} объектов украдено";
    } else {
      restext = "Не удалось переместить объекты";
    }
    showDialog(
        context: ctx,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text("Результат взлома"),
            content: Text(restext),
            actions: [TextButton(onPressed: () => Navigator.of(context).pop(), child: const Text("ok"))],
          );
        });
  }

  void toDialog(BuildContext context, int targetid) async {
    for (Chat c in lastState.chats.values) {
      for (ChatMember m in c.members.values) {
        if (m.id == targetid) {
          Navigator.of(context).pushNamed("/chats/chat", arguments: c.pk);
          return;
        }
      }
    }
    int chat_id = await lastState.newChat(targetid.toString(), [targetid], []);
    Navigator.of(context).pushNamed("/chats/chat", arguments: chat_id);
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    List<Widget> buttons = [
      ListTile(
        title: Text(getTitleText(feature, state!)),
      )
    ];
    buttons.add(ElevatedButton(
      child: const Text("Создать сделку"),
      onPressed: () {
        state.newDeal(feature.valueInt).then((deal) {
          Navigator.of(ctx).pushNamed("/trade/deal", arguments: deal.pk);
        });
      },
    ));
    buttons.add(ElevatedButton(onPressed: () => toDialog(ctx, feature.valueInt), child: const Text("к диалогу")));
    int hoff = state.char!.featuresByTypeName["Hack-Offensive"]?.first.valueInt ?? -1;
    if (hoff > 0) {
      buttons.add(ElevatedButton(onPressed: () => hack(ctx, feature.valueInt, hoff), child: const Text("Взломать")));
    }
    return ListView(children: buttons);
  }

  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    var id = feature.valueInt;
    var c = state.getCharacterById(id);
    if (c == null) return "Загрузка контакта...(id=$id)";
    return "Контакт с " + c.name;
  }
}

class InventoryElementProgram extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    int level = feature.valueJson["level"] ?? 0;
    return "Защитная прогремма $level уровня ";
  }

  Future<void> installToOther(TransitionDesc trans, BuildContext ctx) async {
    var other = await Navigator.of(ctx).pushNamed("/trade/new");
    FeatureDesc? otherFeat = other as FeatureDesc?;
    if (otherFeat == null) return;
    DealDesc deal = await lastState.newDeal(otherFeat.valueInt);
    lastState.autoTransitionInhibited = true;
    await trans.apply();
    FeatureDesc? token;
    int retry = 5;
    while (token == null) {
      token = lastState.char!.featuresByTypeName[trans.description]?.first;
      if (token == null) {
        await new Future.delayed(const Duration(seconds: 1));
        if ((retry--) == 0) {
          lastState.autoTransitionInhibited = false;
          lastState.addError(Error.system("Не дождался токена от ${trans.key}"));
          return;
        }
      }
    }
    deal.addOperation(resource: token.resource);
    deal.accept();
    while (deal.status == DealStatus.created) {
      await new Future.delayed(const Duration(seconds: 1));
      if ((retry--) == 0) {
        lastState.addError(Error.system("Не дождался завершения сделки ${deal.pk}"));
      }
    }
    lastState.autoTransitionInhibited = false;
    Navigator.of(ctx).pop();
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) {
    List<Widget> buttons = [];
    String transname = feature.valueJson["trans"] ?? "";
    TransitionDesc? trans = state!.transitions[transname];
    if (trans != null) {
      if (!trans.isLoading()) {
        if (trans.passed) {
          buttons.add(ElevatedButton(
              onPressed: () async {
                await trans.apply();
                Navigator.of(ctx).pop();
              },
              child: const Text("Установить себе")));
          buttons.add(
              ElevatedButton(onPressed: () => installToOther(trans, ctx), child: const Text("Установить другому")));
        } else {
          buttons.add(ListTile(title: const Text("Не могу установить"), subtitle: Text(trans.getFailedCmp(sep: " "))));
        }
      } else {
        buttons.add(const CircularProgressIndicator());
      }
    } else {
      buttons.add(const ListTile(
        title: Text("Действий не обаружено"),
      ));
    }
    return ListView(children: buttons);
  }
}

class InventoryElementFile extends InventoryElementBase {
  @override
  String getTitleText(FeatureDesc feature, StateStore state) {
    String display = feature.valueStr.trim();
    int len = display.indexOf("\n");
    if (len == -1) len = display.length;
    display = display.substring(0, len).trim();
    return display;
  }

  @override
  Widget getDetail(BuildContext ctx, StateStore? state, FeatureDesc feature) => InvElFileTextField(feature);

  @override
  Widget? getBottom(BuildContext ctx, FeatureDesc feature) {
    String authName = "";
    int? authid = feature.valueJson["fakeAuthor"] ?? feature.valueJson["realAuthor"];
    if (authid == null) {
      authName = "Неизвестен";
    } else {
      var achar = lastState.getCharacterById(authid);
      if (achar == null) {
        authName = "Загрузка...";
      } else {
        authName = achar.name;
      }
    }
    return BottomAppBar(
        child: ElevatedButton(
            child: Text("Автор:" + authName),
            onPressed: () => Navigator.of(ctx).pushNamed("/inventory/signature", arguments: feature)));
  }
}

class InvElFileTextField extends StatefulWidget {
  final FeatureDesc target;

  InvElFileTextField(FeatureDesc _t) : target = _t;

  @override
  State<StatefulWidget> createState() {
    var ret = InvElFileTextFieldState();
    ret.setTarget(target);
    return ret;
  }
}

class InvElFileTextFieldState extends State<InvElFileTextField> {
  late FeatureDesc target;

  void setTarget(FeatureDesc t) {
    target = t;
    if (!target.valueJson.containsKey("realAuthor")) {
      target.valueJson["realAuthor"] = lastState.char!.id;
    }
  }

  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
    _controller.text = target.valueStr;
  }

  @override
  void dispose() {
    if (_controller.value.text != target.valueStr) {
      target.valueJson["realAuthor"] = lastState.char!.id;
      if (target.valueJson.containsKey("fakeAuthor")) {
        target.valueJson.remove("fakeAuthor");
      }
      target.set(_controller.value.text);
      target.setJson();
    }
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: _controller,
      expands: true,
      maxLines: null,
      minLines: null,
    );
  }
}

class FileSignature extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    FeatureDesc? feature = ModalRoute.of(context)!.settings.arguments as FeatureDesc?;

    String getNameById(int? authid) {
      if (authid == null) {
        return "Неизвестен";
      } else {
        var achar = lastState.getCharacterById(authid);
        if (achar == null) {
          return "Загрузка...";
        } else {
          return achar.name;
        }
      }
    }

    Future<void> makeFake(FeatureDesc feature, BuildContext ctx, int hacklevel) async {
      var other = await Navigator.of(ctx).pushNamed("/trade/new");
      FeatureDesc? otherFeat = other as FeatureDesc?;
      if (otherFeat == null) return;
      int otherId = otherFeat.valueInt;

      feature.valueJson["fakeAuthor"] = otherId;
      feature.valueJson["fakeLevel"] = hacklevel;
      feature.setJson();
    }

    return StreamBuilder<StateStore?>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore?> snap) {
          if (!snap.hasData) return const Text("Нет доступных данных");
          if (feature == null) return const Text("Нет объекта для отображения");
          List<Widget> widgets = [];
          widgets.add(ListTile(title: Text(InventoryElementFile().getTitleText(feature, snap.data!))));
          bool fake = false;
          int hacklevel = snap.data!.char!.featuresByTypeName["Hack-Offensive"]?.first.valueInt ?? -1;
          int checklevel = snap.data!.char!.featuresByTypeName["DocumentsCheck"]?.first.valueInt ?? 0;
          int? authid = feature.valueJson["realAuthor"];
          int? fakeid = feature.valueJson["fakeAuthor"];
          if (fakeid != null && authid != fakeid) {
            int level = feature.valueJson["fakeLevel"] ?? 0;
            if (checklevel >= level) {
              fake = true;
            } else {
              authid = fakeid;
            }
          }
          widgets.add(ListTile(title: Text("Автор:" + getNameById(fakeid ?? authid))));
          if (fake) {
            widgets.add(ListTile(title: Text("Настоящий автор:" + getNameById(authid))));
          }
          widgets.add(ListTile(title: Text("Статус:" + (fake ? "ПОДДЕЛЬНЫЙ" : "Подлинный"))));
          if (hacklevel > 0) {
            widgets.add(
                ElevatedButton(onPressed: () => makeFake(feature, ctx, hacklevel), child: const Text("Подделать")));
          }
          return Scaffold(
              appBar: AppBar(
                title: const Text("Свойства файла"),
                actions: [loadIndicatorBuilder()],
              ),
              drawer: getCommonDrawer(context),
              body: ListView(
                children: widgets,
              ));
        });
  }
}

abstract class ContainerCreatorElement {
  Widget? getWidget();

  String label = "";

  ContainerCreatorElement(this.label);

  Function()? onComplete;
}

class ContainerCreatorElementEdit extends ContainerCreatorElement {
  var editController = TextEditingController();

  ContainerCreatorElementEdit(super.label);

  @override
  Widget? getWidget() => TextField(
        controller: editController,
        readOnly: true,
      );
}

class ContainerCreatorElementSwitch extends ContainerCreatorElement {
  bool value = false;

  ContainerCreatorElementSwitch(super.label);

  @override
  Widget? getWidget() => StatefulBuilder(
      builder: (ctx, setState) => Switch(
          value: value,
          onChanged: (v) => setState(() {
            value = v;
          })));
}

class ContainerCreatorElementSelectFeature extends ContainerCreatorElement {
  String filedName;
  late FeatureTypeDesc featType;
  List<String> variants = [];
  late String selected;

  void genVariants(Iterable<DataSimple> ds) {
    for (var d in ds) {
      variants.add(d.getFirstObjectName(filedName) ?? "UNKNOWN");
    }
  }

  ContainerCreatorElementSelectFeature(super.label, String feature, this.filedName) {
    featType = lastState.featureTypesByName[feature]!;
    if (featType.static) {
      genVariants(featType.valuesSimple.values);
    }
    variants.sort();
    selected = variants.first;
  }

  @override
  Widget? getWidget() => StatefulBuilder(
      builder: (BuildContext ctx, StateSetter setState) => DropdownButton<String>(
          value: selected,
          items: variants.map((e) => DropdownMenuItem(value: e, child: Text(e))).toList(),
          onChanged: (value) => setState(() {
            selected = value ?? "UNKNOWN";
          })));
}

class ContainerCreator {
  bool pressedOk = false;
  List<ContainerCreatorElement> elems = [];
  DataSimple simple = DataSimple();
  String name = "";
  bool public = true;

  ContainerCreator() {
    var nameedit = ContainerCreatorElementEdit("Имя");
    elems.add(nameedit);
    //  var publicSwitch = ContainerCreatorElementSwitch("Публичный");
    //  elems.add(publicSwitch);
    for (var e in configuration.containerConf.extra) {
      String field = e.name;
      String action = e.args[0];
      if (action == "default") {
        simple.setFirstObjectName(field, e.args[1]);
      }

      if (action == "feature") {
        String featrue = e.args[1];
        String fieldName = e.args[2];
        var f = ContainerCreatorElementSelectFeature("Выбирите $featrue", featrue, fieldName);
        f.onComplete = () {
          simple.setFirstObjectName(field, f.selected);
        };
        elems.add(f);
      }
    }
    nameedit.editController.text = simple.replaceByFields(configuration.containerConf.creationName);
    nameedit.onComplete = () {
      name = nameedit.editController.text;
    };
    // publicSwitch.onComplete = () {
    //   public = publicSwitch.value;
    // };
  }

  Future<void> ask(BuildContext ctx) async {
    List<Widget> widgets = [];
    for (var e in elems) {
      widgets.add(Text(e.label));
      Widget? w = e.getWidget();
      if (w != null) widgets.add(w);
    }
    await showDialog(
        context: ctx,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: const Text("Введите пармерты контейнера"),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widgets,
            ),
            actions: [
              TextButton(
                  onPressed: () {
                    pressedOk = false;
                    Navigator.of(ctx).pop();
                  },
                  child: const Text("Отмена")),
              TextButton(
                  onPressed: () {
                    pressedOk = true;
                    Navigator.of(ctx).pop();
                  },
                  child: const Text("ok"))
            ],
          );
        });
    if (pressedOk) {
      for (var e in elems) {
        e.onComplete?.call();
      }
    }
  }
}

class ContainersMenu extends StatelessWidget {
  const ContainersMenu({super.key});

  Future<String?> getLookupName(BuildContext ctx) async {
    var editController = TextEditingController();
    return await showDialog(
        context: ctx,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: const Text("Введите имя контейнера"),
            content: TextField(controller: editController),
            actions: [
              TextButton(onPressed: () => Navigator.of(ctx).pop(), child: const Text("Отмена")),
              TextButton(onPressed: () => Navigator.of(ctx).pop(editController.text), child: const Text("ok"))
            ],
          );
        }) as String?;
  }

  @override
  Widget build(BuildContext context) {
    FeatureContainer.loadKnown();
    return StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> snap) {
          Map<String, List<Widget>> tiles = {};
          List<FeatureContainer> conts = snap.data!.containers.values.toList();
          conts.sort((a, b) => a.name.compareTo(b.name));
          for (var cont in conts) {
            String subtitle = "Содержит:";
            for (var i in cont.items.values) {
              subtitle += "${i.amount} ${i.featureType.name}, ";
            }

            String? loc = cont.extraSimple?.getFirstObjectName("location");
            if (loc != null && !cont.name.startsWith("Система")) subtitle += " находится в $loc";
            //TODO: красная, зеленая желтый кружжок для контрактов
            String tabname = "Другое";
            if (cont.name.contains(" ")) tabname = cont.name.split(" ")[0];

            if (!tiles.containsKey(tabname)) tiles[tabname] = [];
            tiles[tabname]!.add(ListTile(
              title: Text(cont.name),
              subtitle: Text(subtitle),
              onTap: () => Navigator.of(context).pushNamed("/containers/detail", arguments: cont),
            ));
          }
          return DefaultTabController(
              length: tiles.length,
              child: Scaffold(
                appBar: AppBar(
                  title: const Text("Доступные контейнеры"),
                  actions: [
                    loadIndicatorBuilder(),
                    IconButton(
                        onPressed: () async {
                          String? name = await getLookupName(ctx);
                          if (name != null) {
                            if (!name.startsWith("Ящик")) {
                              name = "Ящик $name";
                            }
                            List<int> pks = await FeatureContainer.find(name);
                            if (pks.isEmpty) return;
                            for (var pk in pks) {
                              FeatureContainer c = await FeatureContainer.load(pk);
                              if (!c.isKnown()) await c.saveKnown();
                            }
                            FeatureContainer cnt = lastState.containers[pks.first]!;
                            await Navigator.of(navigatorKey.currentContext!)
                                .pushNamed("/containers/detail", arguments: cnt);
                          }
                        },
                        icon: const Icon(Icons.search))
                  ],
                  bottom: TabBar(
                    tabs: tiles.keys.map((e) => Tab(text: e)).toList(),
                  ),
                ),
                drawer: getCommonDrawer(context),
                body: TabBarView(children: tiles.values.map((e) => ListView(children: e)).toList()),
                floatingActionButton: FloatingActionButton(
                  onPressed: () async {
                    var crt = ContainerCreator();
                    await crt.ask(context);
                    if (crt.pressedOk) {
                      FeatureContainer ncont =
                      await FeatureContainer.create(crt.name, crt.public, extra: crt.simple.toString());
                      Navigator.of(navigatorKey.currentContext!).pushNamed("/containers/detail", arguments: ncont);
                    }
                  },
                  child: const Icon(Icons.add),
                ),
              ));
        });
  }
}

class FeatureAndAmount {
  late FeatureTypeDesc feat;
  late int amount;
  FeatureContainer? cont;

  FeatureAndAmount(this.feat, this.amount);
}

class ContainerMenu extends StatelessWidget {
  const ContainerMenu({super.key});

  Future<int?> getAmount(BuildContext ctx, FeatureTypeDesc feat, int maximum, String action) async {
    var editController = TextEditingController(text: "$maximum");
    return await showDialog(
        context: ctx,
        builder: (BuildContext ctx) {
          return AlertDialog(
            title: Text("Какое количество ${feat.name} $action"),
            content: TextField(
              controller: editController,
              inputFormatters: [FilteringTextInputFormatter.digitsOnly],
              keyboardType: TextInputType.number,
            ),
            actions: [
              TextButton(onPressed: () => Navigator.of(ctx).pop(), child: const Text("Отмена")),
              TextButton(
                  onPressed: () => Navigator.of(ctx).pop(int.parse(editController.text)), child: const Text("ok"))
            ],
          );
        }) as int?;
  }

  String getContainerName(FeatureContainer fc, ContainerItem? item) {
    if (fc.name.startsWith("Контракт") && item != null) {
      for (DataSimpleObject o in fc.extraSimple?.getList("request") ?? []) {
        if (o.name == item.featureType.name) {
          return "${fc.name}(Требуется ${o.args[0]})";
        }
      }
    }
    return fc.name;
  }

  Future<FeatureAndAmount?> getFeatureAndAmount(BuildContext ctx, FeatureContainer ourCont, ContainerItem? ourItem, bool needTgtContainer) async {
    var editController = TextEditingController();
    String errors = "";
    List<FeatureTypeDesc> features = [];
    if (configuration.isMaster) {
      for (var f in lastState.featureTypesByID.values) {
        if (f.integer) features.add(f);
      }
    } else {
      for (var f in lastState.char!.features) {
        if (f.typeDesc.integer) features.add(f.typeDesc);
      }
    }
    List<FeatureContainer> containers = [];
    Map<int, int> limits = {};
    for (var c in lastState.containers.values) {
      if (c.pk == ourCont.pk) continue;
      if (!configuration.containerConf.move.check(ourCont, c)) continue;
      if (c.extraSimple?.containsKey("request") ?? false) {
        for (DataSimpleObject r in c.extraSimple!.getList("request") ?? []) {
          if (r.name == ourItem?.featureType.name) {
            int rAmount = int.tryParse(r.args[0]) ?? 0;
            FeatureTypeDesc cft = lastState.featureTypesByName[r.name]!;
            int cAmount = c.items[cft.pk]?.amount ?? 0;
            if (rAmount > cAmount) {
              containers.add(c);
              limits[c.pk] = rAmount - cAmount;
              continue;
            }
          }
        }
      } else {
        containers.add(c);
      }
    }
    late FeatureTypeDesc selected;
    if (!needTgtContainer) {
      selected = features.first;
    } else {
      selected = ourItem!.featureType;
    }
    FeatureContainer? tgtContainer;
    late String title;
    if (!needTgtContainer) {
      title = "Выберете ресурс и его количество";
    } else {
      title = "Выберите контейнер и количество";
    }
    return await showDialog(
        context: ctx,
        builder: (BuildContext ctx) => StatefulBuilder(builder: (BuildContext ctx, StateSetter setState) {
              List<Widget> buttons = [];
              if (needTgtContainer) {
                buttons.add(DropdownButton<FeatureContainer>(
                    value: tgtContainer,
                    items: containers
                        .map((e) => DropdownMenuItem(value: e, child: Text(getContainerName(e, ourItem))))
                        .toList(),
                    onChanged: (value) => setState(() {
                          tgtContainer = value;
                          errors = "";
                          if (ourItem != null) {
                            int? limit = limits[tgtContainer!.pk];
                            if (limit != null) {
                              editController.text = limit.toString();
                            } else {
                              editController.text = ourItem.amount.toString();
                            }
                          }
                        })));
              } else {
                buttons.add(DropdownButton<FeatureTypeDesc>(
                    value: selected,
                    items: features
                        .map((e) => DropdownMenuItem(
                      value: e,
                      child: Text(e.name),
                    ))
                        .toList(growable: false),
                    onChanged: (value) => setState(() {
                          selected = value!;
                          int m = lastState.char?.featuresByTypeID[value.pk]?.first.valueInt ?? 0;
                          editController.text = m.toString();
                        })));
              }
              buttons.add(TextField(
                controller: editController,
                inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                keyboardType: TextInputType.number,
              ));
              if (errors.isNotEmpty) {
                buttons.add(Text(errors));
              }
              Widget content = Column(crossAxisAlignment: CrossAxisAlignment.start, children: buttons);
              List<Widget> actions = [
                TextButton(onPressed: () => Navigator.of(ctx).pop(), child: const Text("Отмена")),
                TextButton(
                    onPressed: () {
                      var ret = FeatureAndAmount(selected, int.tryParse(editController.text) ?? 0);
                      if (needTgtContainer) ret.cont = tgtContainer;
                      int? limit = limits[tgtContainer?.pk ?? 0];
                      if (limit != null) {
                        if (ret.amount > limit) {
                          log("Limit overhead ${ret.amount} > $limit");
                          setState(() {
                            errors = "Привышение требуемого количества ресусров ${ret.amount} > $limit";
                          });
                          return;
                        }
                      }
                      Navigator.of(ctx).pop(ret);
                    },
                    child: const Text("ok"))
              ];
              return AlertDialog(
                title: Text(title),
                content: content,
                actions: actions,
              );
            })) as FeatureAndAmount?;
  }

  List<Widget> getButtons(BuildContext ctx, ContainerItem item, FeatureContainer container) {
    List<Widget> ret = [];
    if (!configuration.isMaster) {
      if (configuration.containerConf.get.show) {
        ret.add(IconButton(
          icon: const Icon(Icons.arrow_downward),
          onPressed: () async {
            int? a = await getAmount(ctx, item.featureType, item.amount, "извлечь");
            if (a != null) await item.put(-a);
          },
        ));
      }
      if (configuration.containerConf.put.show) {
        ret.add(IconButton(
            onPressed: () async {
              int m = lastState.char!.featuresByTypeID[item.featureTypePk]?.first.valueInt ?? 0;
              int? a = await getAmount(ctx, item.featureType, m, "добавить");
              if (a != null) {
                await item.put(a);
                if (container.isAnonymous()) container.reload();
              }
            },
            icon: const Icon(Icons.arrow_upward)));
      }
      if (configuration.containerConf.move.show) {
        ret.add(IconButton(
            onPressed: () async {
              FeatureAndAmount? fa = await getFeatureAndAmount(ctx, container, item, true);
              if (fa != null && fa.cont != null) {
                await item.move(fa.cont!.pk, fa.amount);
                if (container.isAnonymous()) container.reload();
              }
            },
            icon: const Icon(Icons.arrow_right_alt_rounded)));
      }
    } else {
      ret.add(ElevatedButton(
          onPressed: () async {
            int? a = await getAmount(ctx, item.featureType, item.amount, "установить");
            if (a != null) {
              item.set(a);
            }
          },
          child: const Text("Установить")));
      ret.add(ElevatedButton(
          onPressed: () async {
            FeatureAndAmount? fa = await getFeatureAndAmount(ctx, container, item, true);
            if (fa != null && fa.cont != null) {
              await item.move(fa.cont!.pk, fa.amount);
            }
          },
          child: const Text("Переместить")));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    FeatureContainer origcontainer = ModalRoute.of(context)!.settings.arguments as FeatureContainer;
    return StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> snap) {
          List<Widget> tiles = [];
          final DateFormat format = DateFormat.Hms();
          if (!lastState.containers.containsKey(origcontainer.pk)) {
            //Мы пытаемся получить доступ к удаленному контейнеру
            return const Text("Контейнер не усуществует");
          }
          FeatureContainer container = lastState.containers[origcontainer.pk]!;
          if (configuration.isMaster) {
            CharacterDesc? owner = lastState.characters[container.ownerID];
            List<DropdownMenuItem<CharacterDesc?>> chars = [];
            chars.add(const DropdownMenuItem(value: null, child: Text("-----")));
            for (var c in lastState.characters.values) {
              chars.add(DropdownMenuItem(
                value: c,
                child: Text(c.name),
              ));
            }
            tiles.add(ListTile(
              title: const Text("Котейнер пренадлежит"),
              trailing: StatefulBuilder(
                builder: (context, setState) {
                  return DropdownButton<CharacterDesc?>(
                    value: owner,
                    items: chars,
                    onChanged: (newval) => setState(() {
                      container.setOwner(newval);
                    }),
                  );
                },
              ),
            ));
          }
          String? location = container.extraSimple?.getFirstObjectName("location");
          if (location != null) {
            tiles.add(ListTile(
              title: Text("Контейнер находится в $location"),
            ));
          }
          if (container.name.startsWith("Контракт")) {
            String cost = container.extraSimple?.getFirstObjectName("cost") ?? "0";
            String deadline = container.extraSimple?.getFirstObjectName("deedline") ?? "0";
            int timestamp = int.tryParse(deadline) ?? 0;
            DateTime ttimestamp = DateTime.fromMillisecondsSinceEpoch(timestamp * 1000);
            tiles.add(ListTile(
              title: Text("контракт на $cost кредитов. Небходимо закрыть к ${format.format(ttimestamp)}. Необходимо:"),
            ));
          }

          if (container.name.startsWith("Проект")) {
            String desc = "";
            for (DataSimpleObject o in container.extraSimple?.getList("description") ?? []) {
              desc += "${o.name},";
            }
            tiles.add(ListTile(
              title: Text(desc),
            ));
          }
          for (DataSimpleObject r in container.extraSimple?.getList("request") ?? []) {
            tiles.add(ListTile(
              title: Text("${r.name} ${r.args[0]}"),
            ));
          }
          if (container.name.startsWith("Система")) {
            String strNextGen = container.extraSimple?.getFirstObjectName("next_gen") ?? "0";
            int intNextGen = int.tryParse(strNextGen) ?? 0;
            DateTime timeNextGen = DateTime.fromMillisecondsSinceEpoch(intNextGen * 1000);
            tiles.add(ListTile(
              title: Text("Следующая генерация  в ${format.format(timeNextGen)}"),
            ));
          }
          bool isEmpty = true;
          for (var item in container.items.values) {
            tiles.add(ListTile(
              title: Text(item.featureType.name),
              subtitle: Text("${item.amount}"),
              trailing: Wrap(children: getButtons(ctx, item, container)),
            ));
            if (item.amount > 0) isEmpty = false;
          }
          Widget? flatActButton;
          if (configuration.containerConf.put.show) {
            flatActButton = FloatingActionButton(
                onPressed: () async {
                  FeatureAndAmount? fa = await getFeatureAndAmount(ctx, container, null, false);
                  if (fa != null) {
                    if (configuration.isMaster) {
                      await container.set(fa.feat.pk, fa.amount);
                    } else {
                      await container.put(fa.feat.pk, fa.amount);
                      if (container.isAnonymous()) container.reload();
                    }
                  }
                },
                child: const Icon(Icons.add));
          }
          if (isEmpty && container.name.startsWith("Ящик")) {
            tiles.add(ElevatedButton(
                onPressed: () async {
                  Navigator.of(navigatorKey.currentContext!).pop();
                  if (container.ownerID != null) {
                    await lastState.deleteContainer(container);
                  } else {
                    for (FeatureDesc feat in lastState.char!.featuresByTypeName["ContainerLink"] ?? []) {
                      if (feat.valueInt == container.pk) {
                        await feat.set("0");
                        break;
                      }
                    }
                    lastState.containers.remove(container.pk);
                    lastState.updated = true;
                  }
                },
                child: const Text("Удалить контейнер")));
          }
          return Scaffold(
            appBar: AppBar(
              title: Text(container.name),
              actions: [loadIndicatorBuilder()],
            ),
            body: ListView(
              children: tiles,
            ),
            floatingActionButton: flatActButton,
          );
        });
  }
}
