import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:omnilight2/config.dart';
import 'package:omnilight2/main.dart';
import 'package:omnilight2/model.dart';
import 'package:omnilight2/network.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'global.dart';

class GameDesc {
  String? name;
  bool? isTest;
  String? url;
  String? description;

  GameDesc.fromJson(Map<String, dynamic> json) {
    name = loadAndCheck(json, 'name');
    isTest = loadOrDefault(json, "isTest", false);
    url = loadAndCheck(json, 'url');
    description = loadOrDefault(json, "description", "");
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> ret = {};
    ret['name'] = name;
    ret['isTest'] = isTest;
    ret['url'] = url;
    ret['description'] = description;
    return ret;
  }
}

class LoadStatus {
  late String status;
  bool isSelect = false;
  bool isError = false;

  LoadStatus() {
    status = "";
  }

  LoadStatus.status(String st) {
    status = st;
  }

  LoadStatus.select() {
    status = "Select";
    isSelect = true;
  }

  LoadStatus.error(String st) {
    isError = true;
    status = st;
  }
}

class LoadManager {
  late StreamController<LoadStatus> status;

  late GameDesc currentGame;
  String? loadUri = "";
  int gameListSerial = -1;
  List<GameDesc> games = [];

  void dispose() {
    status.close();
  }

  void startLoading() {
    status = StreamController();
    runApp(LoadApp());
    status.add(LoadStatus.status("Загрузка настроек"));
    SharedPreferences.getInstance().then((i) {
      preferences = i;
      loadPrefStatus();
      //lastState.loadPrefsData();
      //lastState.onTimer();
    });
  }

  void loadPrefStatus() {
    loadUri = preferences!.getString("GameURI");
    loadUri ??= "https://games.mg-lpoint.ru/games.json";

    String? g = preferences!.getString("CurrentGame");
    if (g == null) {
      startSelectGame();
    } else {
      try {
        currentGame = GameDesc.fromJson(jsonDecode(g));
        loadGame();
      } catch (e) {
        log("Error loading currentGame $e");
        startSelectGame();
        return;
      }
    }
    status.add(LoadStatus.status("Настройки загружены"));
  }

  void handleGamesDescription(Response res) {
    if (res.statusCode == 200) {
      if (res.data is Map<String, dynamic>) {
        int newserial = res.data['serial'];
        if (newserial <= gameListSerial) {
          log("No updates. Serial $newserial $gameListSerial");
          status.add(LoadStatus.select());
          return;
        }
        gameListSerial = newserial;
        log("new serial is $newserial");
        games.clear();
        for (Map<String, dynamic> game in res.data['games']) {
          games.add(GameDesc.fromJson(game));
        }
        status.add(LoadStatus.select());
      }
    } else {
      status.add(LoadStatus.error("Ошибка ${res.statusCode} ${res.statusMessage}"));
    }
  }

  void handleGameDescription(Response res) async {
    if (res.statusCode == 200) {
      if (res.data is Map<String, dynamic>) {
        try {
          Configuration config = Configuration.fromJson(res.data);
          //TODO: здесь дожно быть кешировние конфига
          configuration = config;

          net = NetworkEngine();
          lastState = StateStore();
          stateStreamController = StreamController.broadcast();
          lastState.loadPrefsData();
          lastState.loadData();
          for (var asset in configuration.assets.values) {
            status.add(LoadStatus.status("Загружаем ${asset.name}"));
            await asset.load();
          }
          runApp(MyApp());
        } on TypeError catch (e) {
          String msg = "handleGameDescription error $e ${e.stackTrace}";
          status.add(LoadStatus.error(msg));
        } catch (e) {
          String msg = "handleGameDescription error $e ";
          status.add(LoadStatus.error(msg));
        }
      }
    } else {
      status.add(LoadStatus.error("Ошибка ${res.statusCode} ${res.statusMessage}"));
    }
  }

  void handleError(Object obj) {
    log("$obj");
    if (obj is DioError) {
      DioError err = obj;
      status.add(LoadStatus.error(err.message ?? ""));
      return;
    }
    status.add(LoadStatus.error("Ошибка загрузки данных"));
  }

  void loadGamesDescription() {
    var client = Dio();
    client.get(loadUri!).then(handleGamesDescription, onError: handleError);
  }

  void startSelectGame() {
    status.add(LoadStatus.select());
    loadGamesDescription();
  }

  void selectGame(GameDesc game) {
    currentGame = game;
    preferences!.setString("GameURI", loadUri!);
    preferences!.setString("CurrentGame", jsonEncode(currentGame.toJson()));
    runApp(LoadApp());
    loadGame();
  }

  void loadGame() {
    var gameuri = "${currentGame.url!}/game.json";
    status.add(LoadStatus.status("Загрука описания игры c адреса $gameuri"));
    var client = Dio();
    client.get(gameuri).then(handleGameDescription, onError: handleError);
  }
}

class LoadApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: LoadAppHome(), title: "OmnilightLoader", routes: <String, WidgetBuilder>{
      "/details": (BuildContext ctx) => const GameDetails(),
    });
  }
}

class LoadAppHome extends StatelessWidget {
  Widget showProgress(String message) {
    return Scaffold(
        body: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [const CircularProgressIndicator(), Text(message)])));
  }

  List<Widget> getGamesList(BuildContext context) {
    List<Widget> ret = [];
    for (var g in loadManager.games) {
      ret.add(ListTile(
        title: Text(g.name!),
        subtitle: Text(g.description!),
        onTap: () => Navigator.of(context).pushNamed("/details", arguments: g),
      ));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      initialData: null,
      stream: loadManager.status.stream,
      builder: (BuildContext ctx, AsyncSnapshot<LoadStatus?> status) {
        if (status.connectionState != ConnectionState.active || status.data == null) return showProgress("Загрузка...");
        if (status.data!.isSelect) {
          return Scaffold(
              appBar: AppBar(title: const Text("Список игр")), body: ListView(children: getGamesList(context)));
        }
        if (status.data!.isError) {
          return AlertDialog(
            title: const Text("Ошибка"),
            content: Text(status.data!.status),
            actions: [TextButton(onPressed: () => loadManager.startSelectGame(), child: const Text("Закрыть"))],
          );
        }
        return showProgress(status.data!.status);
      },
    );
  }
}

//Details
class GameDetails extends StatelessWidget {
  const GameDetails({super.key});

  @override
  Widget build(BuildContext context) {
    GameDesc game = ModalRoute.of(context)!.settings.arguments as GameDesc;
    return Scaffold(
      appBar: AppBar(
        title: Text(game.name!),
      ),
      body: Text(game.description!),
      bottomNavigationBar: BottomAppBar(
          child: ElevatedButton(
            child: const Text("Выбрать"),
        onPressed: () {
          loadManager.selectGame(game);
          Navigator.of(context).pop();
        },
      )),
    );
  }
}
