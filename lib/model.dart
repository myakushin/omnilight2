import 'dart:async';
import 'dart:collection';
import 'dart:convert';
import 'dart:developer';
import 'dart:math' as math;

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:omnilight2/blescanner.dart';
import 'package:omnilight2/global.dart';

import 'events.dart';
import 'inventory.dart';
import 'network.dart';

typedef FeatureUpdatedFunc = void Function(FeatureDesc feat);

enum DataFormat { none, simple, json }

var random = math.Random();

class DataSimpleObject {
  String name = "";
  List<String> args = [];

  DataSimpleObject.empty();

  DataSimpleObject(String raw) {
    if (raw.contains(":")) {
      var l = raw.split(":");
      name = l.removeAt(0);
      args = l;
    } else {
      name = raw;
    }
  }

  @override
  String toString() {
    if (args.isEmpty) {
      return name;
    } else {
      List<String> ret = [name];
      ret.addAll(args);
      return ret.join(":");
    }
  }
}

class DataSimple {
  Map<String, List<DataSimpleObject>> data = {};

  DataSimple();

  DataSimple.fromString(String str) {
    List<String> lines = LineSplitter.split(str).toList();
    if (lines[0] == "!simple") {
      lines.removeAt(0);
    }
    for (var line in lines) {
      if (line.contains("=")) {
        var lline = line.split("=");
        if (lline.length > 2) {
          throw FormatException("To many = in line $line");
        }
        String name = lline[0].trim();
        String value = lline[1].trim();
        List<String> ldata = [];
        if (value.isEmpty) {
          ldata = [];
        } else {
          if (value.contains(",")) {
            ldata = value.split(",").map((e) => e.trim()).toList();
          } else {
            ldata = [value];
          }
        }
        data[name] = ldata.map((e) => DataSimpleObject(e)).toList();
      } else {
        data[line] = [];
      }
    }
  }

  static bool isDataSimple(String str) {
    return str.startsWith("!simple");
  }

  bool containsKey(String key) {
    return data.containsKey(key);
  }

  List<DataSimpleObject>? getList(String key) {
    return data[key];
  }

  DataSimpleObject? getFistObject(String key) {
    return data[key]?.first;
  }

  String? getFirstObjectName(String key) {
    return getFistObject(key)?.name;
  }

  bool? getFirstObjectBool(String key) {
    String? val = getFirstObjectName(key);
    if (val == null) return null;
    if (["yes", "true", "1", "y", "t"].contains(val.toLowerCase())) {
      return true;
    }
    if (["no", "false", "0", "n", "f"].contains(val.toLowerCase())) {
      return false;
    }
    return null;
  }

  void setList(String key, List<DataSimpleObject> lobj) {
    data[key] = lobj;
  }

  void setFirstObject(String key, DataSimpleObject obj) {
    data[key] = [obj];
  }

  void setFirstObjectName(String key, String name) {
    data[key] = [DataSimpleObject(name)];
  }

  void addObject(String key, DataSimpleObject obj) {
    if (!data.containsKey(key)) {
      data[key] = [obj];
      return;
    }
    data[key]!.add(obj);
  }

  void removeObjectFromList(String key, DataSimpleObject obj) {
    data[key]?.remove(obj);
  }

  @override
  String toString() {
    List<String> ret = ["!simple"];
    data.forEach((key, value) {
      ret.add("$key=${value.join(",")}");
    });
    return ret.join("\n");
  }

  String replaceByFields(String pattern) => pattern.replaceAllMapped(RegExp("\\\$[a-z]+"), (m) {
        String key = m[0]?.replaceAll("\$", "") ?? "";
        if (key == "random") {
          return random.nextInt(100000).toString().padLeft(5, '0');
        }
        var l = getList(key);
        if (l == null) return "[no $key value found]";
        if (l.isEmpty) return "";
        if (l.length < 2) return l.first.name;
        return l.map((e) => e.name).join(",");
      });
}

enum GeoForm { point, rect, circle, polygon }

class GeoValues {
  late int x;
  late int y;
  late GeoForm form;
  late String formData;
  final Map<String, GeoForm> geoFormNames = {
    "point": GeoForm.point,
    "rect": GeoForm.rect,
    "circle": GeoForm.circle,
    "polygon": GeoForm.polygon
  };

  GeoValues() {
    x = 0;
    y = 0;
    form = GeoForm.point;
    formData = "";
  }

  GeoValues.formJson(Map<String, dynamic> json) {
    x = loadOrDefault(json, "geo_x", 0);
    y = loadOrDefault(json, "geo_y", 0);
    String sform = loadOrDefault(json, "geoform", "point");
    form = geoFormNames[sform]!;
    formData = loadOrDefault(json, "geoform_data", "");
  }

  @override
  String toString() {
    return "x=$x y=$y form=$form formData=$formData";
  }
}

class FeatureTypeDesc {
  late int pk;
  late String name;
  late bool tradable;
  late bool replicable;
  late bool expectsValue;
  late bool integer;
  late bool nonNegative;
  late bool unique;
  late bool uniqueWithValue;
  late bool editable;
  late bool static;
  late bool geo;
  Map<String, dynamic> values = {};
  Map<String, DataSimple> valuesSimple = {};
  Map<String, GeoValues> geoform = {};
  StreamController<Map<String, dynamic>>? scGlobalUpdates;
  String group = "";
  Set<TransitionDesc> relatedTransitions = {};

  FeatureTypeDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    tradable = loadAndCheck(json, "tradable");
    replicable = loadAndCheck(json, "replicable");
    expectsValue = loadAndCheck(json, "expects_value");
    integer = loadAndCheck(json, "integer");
    nonNegative = loadAndCheck(json, "non_negative");
    unique = loadAndCheck(json, "non_negative");
    uniqueWithValue = loadAndCheck(json, "non_negative");
    editable = loadAndCheck(json, "non_negative");
    static = loadAndCheck(json, "static");
    geo = loadOrDefault(json, "geo", false);
    if (tradable) {
      for (var kv in configuration.inventoryGroups.entries) {
        if (kv.value.contains(name)) group = kv.key;
      }
      if (group.isEmpty) {
        if (integer) {
          group = "Ресурсы";
        } else {
          group = "Предметы";
        }
      }
    }
    if (static) reloadValues();
  }

  Stream<Map<String, dynamic>> getUpdates() {
    scGlobalUpdates ??= StreamController.broadcast(onListen: () {
      if (kDebugMode) {
        log("$name updates lisened");
      }
      lastState.globalListeners.add(this);
    }, onCancel: () {
      log("$name updates canceled");
      lastState.globalListeners.remove(this);
    });
    return scGlobalUpdates!.stream;
  }

  Future<void> reloadValues() async {
    Request rq = await net.addToQueue("GET", ["character", "feature-types", pk.toString()], "");
    Map<String, dynamic>? json = rq.response!.data;
    if (json == null) {
      lastState.addError(Error.network(0, "Wrong data on feature type update ${rq.response!.data}"));
      return;
    }
    values = json["values"] ?? {};
    values.forEach((key, value) {
      if (DataSimple.isDataSimple(value)) {
        valuesSimple[key] = DataSimple.fromString(value);
      }
    });
    if (geo) {
      Map<String, dynamic> geoval = json["geo_values"] ?? {};
      geoform = geoval.map((key, value) => MapEntry(key, GeoValues.formJson(value)));
    }
    log("Loaded global values $values geo $geoform");

    scGlobalUpdates?.add(values);
  }

  bool updateValue(String id, dynamic val) {
    if (values.containsKey(id) && values[id] == val) return false;
    values[id] = val;
    scGlobalUpdates?.add(values);
    return true;
  }

  void invalidateRelatedTransitions() {
    log("$name updated: all related transition invalidated");
    for (var t in relatedTransitions) {
      t.invalidate();
    }
    relatedTransitions.clear();
  }
}

class Location extends Loadable {
  late int pk;
  late String name;
  late int leaderPk;
  late String description;
  List<String> comment = [];
  List<int> members = [];

  Location.fromJson(Map<String, dynamic> json) {
    fillFromJson(json);
  }

  void fillFromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    leaderPk = loadAndCheck(json, "leader");
    description = loadOrDefault(json, "description", "");
    List<dynamic> jcomments = json["comments"] ?? [];
    comment = jcomments.map((e) => e as String).toList();
    List<dynamic> jmembers = json["members"] ?? [];
    members = jmembers.map((e) => e as int).toList();
  }

  @override
  Future<Loadable> load() async {
    var resp = await net.addToQueue("get", ["location", pk.toString()], "");
    fillFromJson(resp.response!.data);
    loaded = LoadableStatus.loaded;
    return this;
  }
}

class PlayerDesc extends Loadable {
  late int id;
  late String nickname;
  late String realName;
  late List<String> contacts = [];
  late bool paid;
  late String medicalInfo;
  late int character;

  PlayerDesc(this.id);

  PlayerDesc.fromJson(Map<String, dynamic> json) {
    fillFromJson(json);
  }

  void fillFromJson(Map<String, dynamic> json) {
    id = loadAndCheck(json, "pk");
    nickname = loadAndCheck(json, "nickname");
    realName = loadOrDefault(json, "real_name", "");
    paid = loadOrDefault(json, "paid", false);
    medicalInfo = loadOrDefault(json, "medical_info", "");
    character = loadOrDefault(json, "character", 0);
  }

  @override
  Future<PlayerDesc> load() async {
    loadingComplete = Completer();
    var resp = await net.addToQueue("get", ["player", id.toString()], "");
    fillFromJson(resp.response!.data!);
    loaded = LoadableStatus.loaded;
    loadingComplete.complete(this);
    lastState.updated = true;
    return this;
  }

  static Future<PlayerDesc> loadPlayer(int id) async {
    PlayerDesc p = PlayerDesc(id);
    return await p.load();
  }
}

class FeatureDesc {
  late int type;
  late String name;
  String valueStr = "";
  Map<dynamic, dynamic> valueJson = {};
  int valueInt = 0;
  int valuePk = 0;
  late int resource;
  late FeatureTypeDesc typeDesc;
  late String extra;
  GeoValues geo = GeoValues();
  DataFormat valueFormat = DataFormat.none;
  late DataSimple valueSimple;

  FeatureDesc.fromJson(Map<String, dynamic> json) {
    type = loadAndCheck(json, "type");
    typeDesc = lastState.featureTypesByID[type]!;
    name = loadAndCheck(json, "name");
    if (json["value"] is String) {
      updateStr(loadAndCheck(json, "value"));
    }
    if (json["value"] is int) {
      updateInt(json["value"]);
    }
    resource = json["resource"];
    extra = loadOrDefault(json, "extra", "");
    if (typeDesc.geo) {
      geo = GeoValues.formJson(json);
    }
    updateExtra();
  }

  FeatureDesc.fromUpdate(Map<String, dynamic> json) {
    type = json["feature"];
    typeDesc = lastState.featureTypesByID[type]!;
    name = typeDesc.name;
    if (json["value"] is String) {
      updateStr(loadAndCheck(json, "value"));
    }
    if (json["value"] is int) {
      updateInt(json["value"]);
    }
    resource = json["resource"];
    extra = loadOrDefault(json, "extra", "");
    updateExtra();
  }

  void updateInt(int i) {
    valueInt = i;
    valueStr = valueInt.toString();
  }

  void updateStr(String str) {
    if (str.isEmpty) return;
    valueStr = str;
    if (str[0] == "!") {
      if (DataSimple.isDataSimple(str)) {
        valueSimple = DataSimple.fromString(str);
        valueFormat = DataFormat.simple;
        return;
      }
    }
    valueInt = int.tryParse(valueStr.trim()) ?? 0;
  }

  void updateExtra() {
    if (extra.contains("{") && extra.contains("}")) {
      valueJson = jsonDecode(extra);
    }
  }

  bool update(Map<String, dynamic> data) {
    log("update by $data");
    var value = data['value'];
    bool updated = false;
    String _extra = loadOrDefault(data, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }

    if (value == null) {
      log("no value in $data");
      return updated;
    }
    if (value is int) {
      if (valueInt != value) {
        updateInt(value);
        typeDesc.invalidateRelatedTransitions();
        return true;
      }
    }
    if (value is String) {
      if (valueStr != value) {
        updateStr(value);
        typeDesc.invalidateRelatedTransitions();
        return true;
      }
    }
    log("Unknown value type ${value.runtimeType.toString()}");
    return updated;
  }

  Future<void> set(String value) async {
    var rq = jsonEncode({"resource": resource, "value": value});
    await net.addToQueue("POST", ["character", lastState.char!.id.toString(), "set_feature"], rq);
  }

  Future<void> setJson() async {
    var rq = jsonEncode({"resource": resource, "extra": jsonEncode(valueJson)});
    await net.addToQueue("POST", ["character", lastState.char!.id.toString(), "set_feature_extra"], rq);
  }

  Future<void> setData() async {
    if (valueFormat == DataFormat.simple) {
      await set(valueSimple.toString());
      return;
    }
    if (valueFormat == DataFormat.none) {
      await set(valueStr);
      return;
    }
  }
}

class StatusHistoryDesc {
  String? status;
  DateTime? timestamp;
}

enum CharacterType { player, npc, technical, all }

class CharacterDesc extends Loadable {
  late int id;
  late String name;
  List<FeatureDesc> features = [];
  Map<String, List<FeatureDesc>> featuresByTypeName = {};
  Map<int, List<FeatureDesc>> featuresByTypeID = {};
  Map<int, FeatureDesc> featureByResource = {};
  List<int> locationIDs = [];
  late String currentStatus;
  List<StatusHistoryDesc> statusHistory = [];
  late DateTime lastModified;
  late String extra;
  Map<String, dynamic> extraJson = {};
  late CharacterType type;

  CharacterDesc(this.id);

  CharacterDesc.fromJson(Map<String, dynamic> json) {
    fillFromJson(json);
  }

  void fillFromJson(Map<String, dynamic> json) {
    id = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    if (json.containsKey("features")) {
      var jf = json["features"];
      jf.forEach((j) {
        var ft = FeatureDesc.fromJson(j);
        features.add(ft);
      });
    }
    List<dynamic> jloc = json["location"] ?? [];
    locationIDs = jloc.map((e) => e as int).toList();
    currentStatus = loadOrDefault(json, "current_status", "UNKNOWN");
    lastModified = DateTime.parse(json["last_modified"] + "Z");
    extra = loadOrDefault(json, "extra", "");
    String stype = loadOrDefault(json, "type", "PLAYER");
    if (stype == "PLAYER") {
      type = CharacterType.player;
    } else if (stype == "NPC") {
      type = CharacterType.npc;
    } else if (stype == "TECHNICAL") {
      type = CharacterType.technical;
    } else {
      throw FormatException("Unknown character type $stype");
    }
    updateFeatureIndices();
  }

  void updateFeatureIndices() {
    featuresByTypeID.clear();
    featureByResource.clear();
    featuresByTypeName.clear();
    for (FeatureDesc ft in features) {
      if (!featuresByTypeName.containsKey(ft.name)) {
        featuresByTypeName[ft.name] = [];
      }
      featuresByTypeName[ft.name]!.add(ft);
      if (!featuresByTypeID.containsKey(ft.type)) {
        featuresByTypeID[ft.type] = [];
      }
      featuresByTypeID[ft.type]!.add(ft);
      featureByResource[ft.resource] = ft;
    }
    if (extra.contains("{") && extra.contains("}")) {
      extraJson = jsonDecode(extra);
    }
  }

  static Future<CharacterDesc> loadCharacter(int id) async {
    var resp = await net.addToQueue("get", ["character", id.toString()], "");
    return CharacterDesc.fromJson(resp.response!.data);
  }

//{event: character, id: 1, name: char1, status: In game, location_id: null, player_id: 1, user_id: 2, last_modified: 2021-09-04 13:32:30}
  bool update(Map<String, dynamic> data) {
    bool updated = false;
    log("Update character by $data");
    String _name = loadOrDefault(data, "name", name);
    if (_name != name) {
      name = _name;
      updated = true;
    }

    String _currentStatus = loadOrDefault(data, "status", currentStatus);
    if (_currentStatus != currentStatus) {
      currentStatus = _currentStatus;
      updated = true;
    }
    /*  String _location=loadOrDefault(data, "location", location);
    if(_location!=location) {
      location = _location;
      updated=true;
    }
*/
    String _extra = loadOrDefault(data, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }
    DateTime dlastModified = DateTime.parse(data["last_modified"]);
    if (dlastModified != lastModified) {
      lastModified = dlastModified;
      updated = true;
    }
    return updated;
  }

  bool newer(CharacterDesc? other) {
    if (other == null) return true;
    return lastModified.compareTo(other.lastModified) < 0;
  }

  void _notifyByList(Iterable<int> lst, eventType et) {
    for (var i in lst) {
      var f = featureByResource[i];
      if (f == null) continue;
      if (f.typeDesc.tradable) {
        notifyUserFeature(et, f.typeDesc);
      } else {
        notifyUser(eventType.CHARACTER_UPDATED);
      }
    }
  }

  bool generateNotifyForDiff(CharacterDesc? other) {
    if (other == null) return true;
    var isChanged = false;
    var ourRes = featureByResource.keys.toSet();
    var otherRes = other.featureByResource.keys.toSet();
    var added = ourRes.difference(otherRes);
    var removed = otherRes.difference(ourRes);
    isChanged = added.isNotEmpty || removed.isNotEmpty;
    _notifyByList(added, eventType.INVENTORY_ADDED);
    other._notifyByList(removed, eventType.INVENTORY_REMOVED);

    for (var i in ourRes.intersection(otherRes)) {
      var ours = featureByResource[i]!;
      var others = other.featureByResource[i]!;
      if (ours.valueStr == others.valueStr) continue;
      isChanged = true;
      if (ours.typeDesc.tradable) {
        notifyUserFeature(eventType.INVENTORY_UPDATED, ours.typeDesc);
      } else {
        notifyUser(eventType.CHARACTER_UPDATED);
      }
      ours.typeDesc.invalidateRelatedTransitions();
      if (lastState.featureUpdateListeners.containsKey(ours.name)) {
        lastState.featureUpdateListeners[ours.name]!(ours);
      }
    }
    return isChanged;
  }

  FeatureDesc? findFeatureByIntValue(String type, int id) {
    for (FeatureDesc c in featuresByTypeName[type] ?? []) {
      if (c.valueInt == id) return c;
    }
    return null;
  }

  FeatureDesc? findFeatureByStrValue(String type, String s) {
    for (FeatureDesc c in featuresByTypeName[type] ?? []) {
      if (c.valueStr == s) return c;
    }
    return null;
  }

  Future<void> setJson() async {
    var rq = jsonEncode({"extra": jsonEncode(extraJson)});
    await net.addToQueue("POST", ["character", lastState.char!.id.toString(), "set_extra"], rq);
  }

  Future<int> newFeature(int ftype, String value, {String extra = "{}"}) async {
    var rq = jsonEncode({"type": ftype, "value": value, "extra": extra});
    Request resp = await net.addToQueue("POST", ["character", lastState.char!.id.toString(), "new_feature"], rq);
    return resp.response?.data["resource"] ?? 0;
  }

  FeatureDesc? checkResource(StateStore state, int resource) => state.char!.featureByResource[resource];

  Future<FeatureDesc> waitForResource(int resource) {
    Completer<FeatureDesc> cmlp = Completer();
    StreamSubscription? subscription;
    var feat = checkResource(lastState, resource);
    if (feat != null) {
      cmlp.complete(feat);
    } else {
      subscription = stateStreamController.stream.listen((event) {
        feat = checkResource(event, resource);
        if (feat != null) {
          cmlp.complete(feat);
          subscription?.cancel();
          subscription = null;
        }
      });
    }
    return cmlp.future;
  }

  @override
  Future<CharacterDesc> load() async {
    loaded = LoadableStatus.loading;
    loadingComplete = Completer();
    var resp = await net.addToQueue("get", ["character", id.toString()], "");
    fillFromJson(resp.response!.data!);
    loadingComplete.complete(this);
    lastState.updated = true;
    return this;
  }
}

class DealOperationDesc {
  late int pk;
  late int type;
  late int deal;
  late int buyer;
  late String operation;
  late int feature;
  late int amount;
  late int resource;

  late bool isBuyer;

  DealOperationDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    type = loadOrDefault(json, "type", -1);
    deal = loadAndCheck(json, "deal");
    buyer = loadAndCheck(json, "buyer");
    operation = loadAndCheck(json, "operation");
    feature = loadOrDefault(json, "feature", -1);
    amount = loadOrDefault(json, "amount", -1);
    resource = loadOrDefault(json, "resource", -1);
    isBuyer = lastState._char?.id == buyer;
  }

  Future<void> remove() async {
    var path = ["character", "deal-operations", pk.toString()];
    await net.addToQueue("delete", path, "");
  }

  Future<void> edit(int amount) async {
    Map<String, dynamic> json = {};
    json["pk"] = pk;
    json["type"] = type;
    json["deal"] = deal;
    json["buyer"] = buyer;
    json["operation"] = operation;
    json["feature"] = feature;
    json["amount"] = amount;
    var path = ["character", "deal-operations", pk.toString()];
    await net.addToQueue("Put", path, jsonEncode(json));
  }

  bool update(Map<String, dynamic> json) {
    bool updated = false;
    int _amount = loadOrDefault(json, "amount", -1);
    if (_amount != amount) {
      amount = _amount;
      updated = true;
    }
    int _resource = loadOrDefault(json, "resource", -1);
    if (_resource != resource) {
      resource = _resource;
      updated = true;
    }
    int _feature = loadOrDefault(json, "feature", -1);
    if (feature != _feature) {
      feature = _feature;
      updated = true;
    }
    return updated;
  }

  bool compare(DealOperationDesc other) {
    if (amount != other.amount) return true;
    if (type != other.type) return true;
    if (buyer != other.buyer) return true;
    if (operation != other.operation) return true;
    if (feature != other.feature) return true;
    if (resource != other.resource) return true;

    return isBuyer != other.isBuyer;
  }
}

enum DealStatus { created, Accepted, Failed }

class DealDesc {
  late int pk;
  late int initiator;
  late int acceptor;
  List<DealOperationDesc> operations = [];
  late bool initiator_accepted;
  late bool acceptor_accepted;
  late DealStatus status;
  late DateTime last_modified;

  late bool isWeInitiator;
  late bool isWeAcceptor;
  late bool isWeAccept;
  late bool isOtherAccept;
  late String extra;
  Map<String, dynamic> extraJson = {};

  //Для быстрого доступа к опирациям с вещественными объектами
  Map<int, DealOperationDesc> operationsByRes = {};

  //Для быстрого доступа к опирациям с количественными объектами(Например деньги)
  Map<int, DealOperationDesc> operationsByFeature = {};

  get otherId {
    if (isWeAcceptor) return initiator;
    return acceptor;
  }

  DealDesc.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    initiator = loadAndCheck(json, "initiator");
    acceptor = loadAndCheck(json, "acceptor");
    if (json["operations"] is List<dynamic>) {
      for (var js in json["operations"]) {
        var op = DealOperationDesc.fromJson(js);
        operations.add(op);
      }
    }
    initiator_accepted = loadAndCheck(json, "initiator_accepted");
    acceptor_accepted = loadAndCheck(json, "acceptor_accepted");
    parseStatus(json);
    last_modified = DateTime.parse(json["last_modified"]);
    extra = loadOrDefault(json, "extra", "");
    updateDealOperationIndices();
    updateAccept();
  }

  void updateDealOperationIndices() {
    operationsByFeature.clear();
    operationsByRes.clear();
    for (DealOperationDesc op in operations) {
      if (op.resource != -1) {
        operationsByRes[op.resource] = op;
      }
      if (op.feature != -1) operationsByFeature[op.feature] = op;
    }
    if (extra.contains("{") && extra.contains("}")) {
      extraJson = jsonDecode(extra);
    }
  }

  void updateAccept() {
    if (lastState._char!.id == initiator) {
      isWeInitiator = true;
      isWeAcceptor = false;
      isWeAccept = initiator_accepted;
      isOtherAccept = acceptor_accepted;
    }
    if (lastState._char!.id == acceptor) {
      isWeInitiator = false;
      isWeAcceptor = true;
      isWeAccept = acceptor_accepted;
      isOtherAccept = initiator_accepted;
    }
  }

  void parseStatus(Map<String, dynamic> json) {
    switch (json["status"]) {
      case "Created":
        status = DealStatus.created;
        break;
      case "Failed":
        status = DealStatus.Failed;
        break;
      case "Accepted":
        status = DealStatus.Accepted;
        break;
      default:
        throw Exception("ERROR: unknown error state ${json["status"]}");
    }
  }

  bool update(Map<String, dynamic> json) {
    bool updated = false;
    int _initiator = loadAndCheck(json, "initiator");
    if (_initiator != initiator) {
      initiator = _initiator;
      updated = true;
    }
    int _acceptor = loadAndCheck(json, "acceptor");
    if (acceptor != _acceptor) {
      acceptor = acceptor;
      updated = true;
    }

    bool InitiatorAccepted = loadAndCheck(json, "initiator_accepted");
    if (InitiatorAccepted != initiator_accepted) {
      initiator_accepted = InitiatorAccepted;
      updated = true;
    }

    bool AcceptorAccepted = loadAndCheck(json, "acceptor_accepted");
    if (AcceptorAccepted != acceptor_accepted) {
      acceptor_accepted = AcceptorAccepted;
      updated = true;
    }

    String _extra = loadOrDefault(json, "extra", extra);
    if (_extra != extra) {
      extra = _extra;
      updated = true;
    }
    var _lastModified = last_modified;
    last_modified = DateTime.parse(json["last_modified"]);
    if (_lastModified != last_modified) updated = true;

    var _status = status;
    parseStatus(json);
    if (status != _status) updated = true;

    if (updated) {
      updateAccept();
      updateDealOperationIndices();
    }
    return updated;
  }

  bool newer(DealDesc other) {
    return last_modified.compareTo(other.last_modified) > 0;
  }

  Future<int> addOperation({int? resource, int? feature, int? amount}) async {
    if (resource == null && (feature == null || amount == null)) {
      throw Exception("Invalid addDeal. resource or feature and amount should be not null");
    }
    var json = <String, dynamic>{};
    int buyer = 0;
    if (isWeInitiator) {
      buyer = acceptor;
    } else {
      buyer = initiator;
    }
    json["deal"] = pk;
    json["buyer"] = buyer;
    bool replicable = lastState.char?.featureByResource[resource]?.typeDesc.replicable ?? false;
    if (replicable) {
      json["operation"] = "Copy";
    } else {
      json["operation"] = "Trade";
    }
    if (resource != null) json["resource"] = resource;
    if (feature != null) json["feature"] = feature;
    if (amount != null) json["amount"] = amount;
    Request rq = await net.addToQueue("post", ["character", "deal-operations"], jsonEncode(json));
    log(rq.response?.data);

    if (rq.response!.data is Map<String, dynamic>) {
      return rq.response!.data["pk"] ?? -1;
    }

    return 0;
  }

  DealOperationDesc? getOperationById(int opID) {
    for (DealOperationDesc op in operations) {
      if (op.pk == opID) return op;
    }
    return null;
  }

  void updateDeal(PickResult result) {
    int? buyer = 0;
    if (isWeInitiator) {
      buyer = acceptor;
    } else {
      buyer = initiator;
    }
    for (int? i in result.checked) {
      var json = <String, dynamic>{};
      json["deal"] = pk;
      json["buyer"] = buyer;
      json["operation"] = "Trade";
      json["resource"] = i;
      net.addToQueue("post", ["character", "deal-operations"], jsonEncode(json)).then((r) {
        log(r.response!.data);
      });
    }
    for (var kv in result.amount.entries) {
      var json = <String, dynamic>{};
      json["deal"] = pk;
      json["buyer"] = buyer;
      json["operation"] = "Trade";
      json["feature"] = kv.key;
      json["amount"] = kv.value;
      net.addToQueue("post", ["character", "deal-operations"], jsonEncode(json)).then((r) {
        log(r.response!.data);
      });
    }
  }

  Future<void> accept() async {
    var json = <String, dynamic>{};
    json["accept"] = true;
    var path = ["character", "deals", pk.toString()];
    await net.addToQueue("put", path, jsonEncode(json));
  }

  Future<void> updateExtra() async {
    if (extraJson.isNotEmpty) {
      extra = jsonEncode(extraJson);
    }
    var json = <String, dynamic>{};
    json["accept"] = isWeAccept;
    json["extra"] = extra;
    var path = ["character", "deals", pk.toString()];
    await net.addToQueue("put", path, jsonEncode(json));
  }

  bool compare(DealDesc other) {
    if (pk != other.pk) return true;
    if (initiator != other.initiator) return true;
    if (acceptor != other.acceptor) return true;
    if (operations.length != other.operations.length) return true;
    if (initiator_accepted != other.initiator_accepted) return true;
    if (acceptor_accepted != other.acceptor_accepted) return true;
    if (status != other.status) return true;
    if (last_modified != other.last_modified) return true;
    for (var op in operations) {
      var otherop = other.getOperationById(op.pk);
      if (otherop == null || op.compare(otherop)) {
        return true;
      }
    }
    return false;
  }

  DealOperationDesc? getDealOpByPk(int oppk) {
    for (var op in operations) {
      if (op.pk == oppk) return op;
    }
    return null;
  }
}

class TreeElement<key, value> {
  Map<key, TreeElement<key, value>>? trunk;
  value? leaf;

  void put(List<key> path, value val) {
    if (path.isEmpty) {
      leaf = val;
      return;
    }
    var next = path[0];
    trunk ??= SplayTreeMap();
    if (!trunk!.containsKey(next)) trunk![next] = TreeElement<key, value>();
    trunk![next]!.put(path.sublist(1), val);
  }

  TreeElement<key, value>? getElement(List<key> path) {
    if (path.isEmpty) return this;
    if (trunk == null) return null;
    if (!trunk!.containsKey(path[0])) return null;
    return trunk![path[0]]!.getElement(path.sublist(1));
  }

  List<value> getList() {
    List<value> ret = [];
    if (leaf != null) ret.add(leaf as value);
    if (trunk == null) return ret;
    for (var t in trunk!.values) {
      ret = ret + t.getList();
    }
    return ret;
  }

  value getMaximum() {
    if (trunk == null) return leaf!;
    return trunk!.values.last.getMaximum();
  }

  value? getMaximumCond(bool Function(value?) test) {
    for (var e in getList().reversed) {
      if (test(e)) return e;
    }
    return null;
  }

  value? getMinimum() {
    if (leaf != null) return leaf;
    return trunk!.values.first.getMaximum();
  }
}

class TransitionComponentDesc {
  late String title;
  late int feature_type;
  late String feature_type_name;
  late String feature_value;
  late String type;
  late bool any_value;
  bool? condition_passed;

  TransitionComponentDesc.fromJson(Map<String, dynamic> json) {
    title = loadAndCheck(json, "title");
    feature_type = loadOrDefault(json, "feature_type", -1);
    type = loadOrDefault(json, "type", "Неизвестно");
    feature_type_name = loadOrDefault(json, "feature_type_name", "");
    feature_value = loadOrDefault(json, "feature_value", "");
    any_value = loadOrDefault(json, "any_value", false);
    condition_passed = loadOrDefault(json, "condition_passed", null);
  }
}

enum LoadableStatus { notLoaded, loading, loaded, invalid }

abstract class Loadable {
  LoadableStatus loaded = LoadableStatus.notLoaded;
  Completer<Loadable> loadingComplete = Completer();

  bool isLoading() {
    if (loaded == LoadableStatus.loaded) {
      return false;
    }
    if (loaded != LoadableStatus.loading) {
      loaded = LoadableStatus.loading;
      load();
    }
    return true;
  }

  Future<void> thenLoaded() async {
    if (isLoading()) {
      await loadingComplete.future;
    }
  }

  void invalidate() {
    if (loaded == LoadableStatus.loaded) {
      loaded = LoadableStatus.invalid;
    }
  }

  Future<Loadable> load();
}

class TransitionDesc extends Loadable {
  late String key;
  late List<String> keySplit;
  int? level; //Часто бывает нужен
  String title = "";
  String description = "";
  late DateTime last_modified;
  List<TransitionComponentDesc> components = [];

  TransitionDesc.fromJson(Map<String, dynamic> json) {
    key = loadAndCheck(json, "key");
    keySplit = key.split("-");
    if (keySplit.last.contains('L')) {
      level = int.tryParse(keySplit.last.substring(1));
    }
    title = loadAndCheck(json, "title");
    description = loadAndCheck(json, "description");
    last_modified = DateTime.parse(json["last_modified"]);

    components = [];
    for (var c in json["components"]) {
      components.add(TransitionComponentDesc.fromJson(c));
    }
  }

  bool newer(TransitionDesc? other) {
    if (other == null) return true;
    return last_modified.compareTo(other.last_modified) > 0;
  }

  @override
  Future<TransitionDesc> load() async {
    if (loaded == LoadableStatus.loaded) {
      return this;
    }
    if (loadingComplete.isCompleted) {
      loadingComplete = Completer();
    }

    List<String> path = ["character", "transitions", key];
    var resp = await net.addToQueue("get", path, "");

    if (resp.response!.data == null) {
      return this;
    }
    Map<String, dynamic> json = resp.response!.data;

    components.clear();
    for (var c in json["components"] ?? []) {
      var tc = TransitionComponentDesc.fromJson(c);
      components.add(tc);
      FeatureTypeDesc? ft = lastState.featureTypesByID[tc.feature_type];
      ft?.relatedTransitions.add(this);
      log("Component of $key: ${tc.title} = ${tc.condition_passed}");
    }
    log("Components of $key updated");

    lastState.updated = true;
    loaded = LoadableStatus.loaded;
    loadingComplete.complete(this);
    return this;
  }

  bool get passed {
    return components.map((c) => c.condition_passed).every((b) => b ?? false);
  }

  Future<void> apply() async {
    Map<String, dynamic> json = {"key": key};
    var path = ["character", "transitions", key, "apply"];
    log("Apply transition $key");
    await net.addToQueue("post", path, jsonEncode(json));
  }

  String getFailedCmp({String sep = ","}) {
    return components.where((e) => !(e.condition_passed ?? true)).map((e) => e.title).join(sep);
  }
}

enum ChatMemberType { Unknown, Master, Character }

class ChatMember {
  int pk = 0;
  int id = 0;
  CharacterDesc? char;
  String name = "";
  ChatMemberType type = ChatMemberType.Unknown;
  int chat_id = -1;

  ChatMember.fromJSON(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    chat_id = loadOrDefault(json, "chat", -1);
    dynamic _member = json["member"]!;
    if (_member is String) {
      name = _member;
      type = ChatMemberType.Master;
    } else if (_member is int) {
      id = _member;
      type = ChatMemberType.Character;
      CharacterDesc.loadCharacter(id).then((value) {
        char = value;
        lastState.updated = true;
        if (lastState.chats.containsKey(chat_id)) {
          Chat c = lastState.chats[chat_id]!;
          c.streamController.add(null);
        }
      });
    }
  }

  ChatMember();

  String get label {
    if (type == ChatMemberType.Master) return name;
    if (type == ChatMemberType.Character) {
      return char?.name ?? "Загрузка...";
    }
    return "Неизвестно";
  }
}

class Message {
  bool isLoading = false;
  late int messageid;
  late int chat;
  late String text;
  late DateTime time;
  late int sender;
  late String type;

  Message.fromJSON(Map<String, dynamic> json) {
    messageid = loadAndCheck(json, "messageid");
    String _time = loadAndCheck(json, "time");
    time = DateTime.parse("${_time}Z");
    type = loadAndCheck(json, "type");
    sender = loadAndCheck(json, "sender");
    text = loadAndCheck(json, "text");
    chat = loadAndCheck(json, "chat");
  }

  Message.loading() {
    isLoading = true;
    messageid = sender = 0;
    text = type = "";
    time = DateTime.now();
  }
}

class Chat {
  late int pk;
  late String label;
  int messagesCount = 0;
  Map<int, Message> messages = {};
  StreamController<void> streamController = StreamController.broadcast();
  Map<int, ChatMember> members = {};
  int maxMessageID = 0;

  void dismiss() {
    streamController.close();
  }

  String getLabel() {
    if (members.length == 2) {
      if (configuration.isMaster) {
        List<String> ret = [];
        for (var m in members.values) {
          if (m.type != ChatMemberType.Master) {
            ret.add(m.label);
          } else {
            ret.add("(мы как ${m.label})");
          }
        }
        return ret.join(",");
      } else {
        for (var m in members.values) {
          if (m.type == ChatMemberType.Master || m.id != lastState.char!.id) {
            return m.label;
          }
        }
      }
    }
    return label;
  }

  Chat.fromJsom(Map<String, dynamic> json) {
    int _chatid = loadOrDefault(json, "chat", -1);
    pk = loadOrDefault(json, "pk", -1);
    if (pk == -1) pk = _chatid;
    if (pk == -1) throw FormatException("Nor pk nor chat not set in $json");

    label = loadAndCheck(json, "label");
    messagesCount = loadOrDefault(json, "messages", 0);
    List<dynamic> _members = json["members"] ?? [];
    for (var _m in _members) {
      Map<String, dynamic> m = _m;
      var member = ChatMember.fromJSON(m);
      members[member.pk] = member;
    }
  }

  Message? getMessage(int id) {
    //log("Get message $id count=$messagesCount max=$maxMessageID len=${messages.length}");
    if (id < 0 || id >= messagesCount) return null;
    if (id >= messages.length) {
      loadNext(id);
      return Message.loading();
    }
    return messages[maxMessageID - id];
  }

  Future<void> loadNext(int id) async {
    /*   Map<String, dynamic> options = {
      "after": newestMessage.toString(),
      "before": newestMessage.add(Duration(hours: 1)).toString()
    };*/

    Request resp = await net.addToQueue(
      "get",
      ["character", "chats", pk.toString()],
      "",
    );
    bool updated = false;
    for (var msg in resp.response?.data?["messages"] as List<dynamic>? ?? []) {
      var m = Message.fromJSON(msg);
      if (!messages.containsKey(m.messageid)) {
        messages[m.messageid] = m;
        updated = true;
        if (m.messageid > maxMessageID) maxMessageID = m.messageid;
      }
    }
    if (updated) {
      streamController.add(null);
    }
  }

  Future<int> send(String message) async {
    Map<String, dynamic> request = {};
    request["text"] = message;
    var resp = await net.addToQueue("post", ["character", "chats", pk.toString(), "send_message"], jsonEncode(request));
    Message m = Message.fromJSON(resp.response!.data["message"] ?? <String, dynamic>{});
    return m.messageid;
  }
}

enum TimerType { timerOnce, timerUntilComplete, timerPeriodic }

class ServerTimer {
  late int pk;
  late String title;
  late TimerType type;
  late int transition;
  late DateTime startTime;
  late int time; //in sec
  late bool performed;
  late DateTime endTime;

  ServerTimer.fromJsom(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    title = loadAndCheck(json, "title");
    String tp = loadAndCheck(json, "type");
    if (tp == "TIMER-ONCE") {
      type = TimerType.timerOnce;
    } else if (tp == "TIMER-UNTIL-COMPLETE") {
      type = TimerType.timerUntilComplete;
    } else if (tp == "TIMER-PERIODIC") {
      type = TimerType.timerPeriodic;
    } else {
      throw FormatException("Unknown timer type $tp");
    }
    transition = loadAndCheck(json, "transition");
    String start = loadAndCheck(json, "start_time") + "Z";
    startTime = DateTime.parse(start);
    time = loadAndCheck(json, "time");
    performed = loadAndCheck(json, "performed");
    recalc();
  }

  void recalc() {
    endTime = startTime.add(Duration(seconds: time));
  }

  Duration getTimeToExpire() => endTime.difference(DateTime.now());
}

class ContainerItem {
  late int featureTypePk;
  late FeatureTypeDesc featureType;
  late int amount;
  late String extra;
  late int containerPK;
  late FeatureContainer container;

  ContainerItem.fromJSON(Map<String, dynamic> json) {
    featureTypePk = loadAndCheck(json, "featureType");
    featureType = lastState.featureTypesByID[featureTypePk]!;
    amount = loadAndCheck(json, "amount");
    extra = loadOrDefault(json, "extra", "");
    containerPK = loadOrDefault(json, "container", 0);
  }

  Future<void> put(int amount) => container.put(featureTypePk, amount);

  Future<void> set(int amount) => container.set(featureTypePk, amount);

  Future<void> move(int tgtCont, int amount) async {
    Map<String, dynamic> djson = {"featureType": featureTypePk, "amount": amount, "to": tgtCont};
    await net.addToQueue("post", ["character", "containers", containerPK.toString(), "move"], json.encode(djson));
  }
}

class FeatureContainer {
  late int pk;
  late String name;
  late String extra;
  DataSimple? extraSimple;
  int? ownerID;
  Map<int, ContainerItem> items = {};

  FeatureContainer.fromJSON(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    name = loadAndCheck(json, "name");
    extra = loadOrDefault(json, "extra", "");
    ownerID = loadAndCheck(json, "owner");
    List<dynamic> jitems = json["items"] ?? [];
    for (ContainerItem i in jitems.map((e) => ContainerItem.fromJSON(e))) {
      i.containerPK = pk;
      i.container = this;
      items[i.featureTypePk] = i;
    }
    if (DataSimple.isDataSimple(extra)) {
      extraSimple = DataSimple.fromString(extra);
    }
  }

  bool isAnonymous() => ownerID == null;

  Future<void> reload() async {
    await FeatureContainer.load(pk, force: true);
  }

  Future<void> put(int featureTypePk, int amount) async {
    Map<String, dynamic> djson = {"featureType": featureTypePk, "amount": amount};
    await net.addToQueue("post", ["character", "containers", pk.toString(), "put"], json.encode(djson));
  }

  Future<void> set(int featureTypePk, int amount) async {
    Map<String, dynamic> djson = {
      "featureType": featureTypePk,
      "amount": amount,
    };
    await net.addToQueue("post", ["character", "containers", pk.toString(), "set"], json.encode(djson));
  }

  static Future<FeatureContainer> create(String name, bool isPublic, {String? extra}) async {
    Map<String, dynamic> djson = {"name": name, "public": isPublic};
    if (extra != null) {
      djson["extra"] = extra;
    }
    var resp = await net.addToQueue("post", ["character", "containers", "new"], json.encode(djson));
    var rjson = resp.response!.data as Map<String, dynamic>;
    int pk = loadAndCheck(rjson, "pk");
    if (isPublic) {
      FeatureContainer c = await FeatureContainer.load(pk);
      c.saveKnown();
    }
    while (!lastState.containers.containsKey(pk)) {
      log("Wanting for container $pk");
      await Future.delayed(const Duration(milliseconds: 100));
    }
    return lastState.containers[pk]!;
  }

  static Future<List<int>> find(String name) async {
    Map<String, dynamic> djson = {"request": name};
    var resp = await net.addToQueue("post", ["character", "containers", "find"], json.encode(djson));
    var rjson = resp.response!.data as Map<String, dynamic>;
    List<int> ret = [];
    for (int i in rjson["answer"] ?? []) {
      ret.add(i);
    }
    return ret;
  }

  static Future<FeatureContainer> load(int pk, {bool force = false}) async {
    if (lastState.containers.containsKey(pk) && !force) {
      return lastState.containers[pk]!;
    }
    var resp = await net.addToQueue("get", ["character", "containers", pk.toString()], "");
    var rjson = resp.response!.data as Map<String, dynamic>;
    FeatureContainer cont = FeatureContainer.fromJSON(rjson);
    lastState.containers[cont.pk] = cont;
    lastState.updated = true;
    return cont;
  }

  static Future<void> loadKnown() async {
    if (configuration.isMaster) return;
    for (FeatureDesc feat in lastState.char!.featuresByTypeName["ContainerLink"] ?? []) {
      int pk = feat.valueInt;
      if (pk == 0) continue;
      if (!lastState.containers.containsKey(pk)) {
        try {
          await load(pk);
        } on DioError catch (e) {
          log(e.toString());
          await feat.set("0");
        }
      }
    }
  }

  bool isKnown() {
    if (configuration.isMaster) return true;
    if (!isAnonymous()) return true;
    for (FeatureDesc feat in lastState.char!.featuresByTypeName["ContainerLink"] ?? []) {
      if (feat.valueInt == pk) {
        return true;
      }
    }
    return false;
  }

  Future<void> saveKnown() async {
    if (configuration.isMaster) return;
    FeatureTypeDesc cf = lastState.featureTypesByName["ContainerLink"]!;
    await lastState.char!.newFeature(cf.pk, pk.toString());
  }

  Future<void> setOwner(CharacterDesc? newOwner) async {
    Map<String, dynamic> djson = {"owner": newOwner?.id};
    await net.addToQueue("post", ["character", "containers", pk.toString(), "chown"], json.encode(djson));
  }
}

class StateStore {
  bool _updated = false;
  PlayerDesc? _player;
  CharacterDesc? _char;
  int playerID = 0;

  Map<String, FeatureTypeDesc> featureTypesByName = {};
  Map<int, FeatureTypeDesc> featureTypesByID = {};
  Map<int, DealDesc> deals = {};
  Map<int, PlayerDesc> players = {};
  final Map<int, CharacterDesc> characters = {};
  final Map<String, CharacterDesc> charactersByName = {};
  Map<String, TransitionDesc> transitions = {};
  TreeElement<String, TransitionDesc> transTree = TreeElement();
  Map<int, ServerTimer> timers = {};
  List<Error> errors = [];
  List<FeatureTypeDesc> globalListeners = [];
  BLEConnection? bleConnection;
  FeatureDesc? bleDeviceMAC;
  Map<String, FeatureUpdatedFunc> featureUpdateListeners = {};
  bool autoTransitionInhibited = false;
  Map<int, Chat> chats = {};
  Map<int, FeatureContainer> containers = {};
  Map<int, Location> locations = {};

  StateStore() {
    Timer.periodic(Duration(seconds: configuration.refreshRate), (tmx) => loadData());

    Timer.periodic(const Duration(minutes: 5), (timer) => onTimerRare());
  }

  Error? getError() {
    if (errors.isEmpty) return null;
    return errors[0];
  }

  void removeError(Error err) {
    errors.remove(err);
    stateStreamController.add(this);
  }

  void clearErrors() {
    errors.clear();
    stateStreamController.add(this);
  }

  void addError(Error e) {
    errors.insert(0, e);
    stateStreamController.add(this);
    log("ERROR: ${e.subsystem} ${e.code} ${e.message}");
  }

  Future<void> onTimerRare() async {
    await reloadFeatureTypes();
    await reloadTransitions();
    net.recheckWebsocket();
  }

  Future<void> loadData() async {
    bool firstTime = false;
    var start = DateTime.now();
    if (!net.isReady) {
      log("onTimer canceled. Network not ready");
      return;
    }
    if (featureTypesByID.isEmpty && featureTypesByName.isEmpty) {
      await reloadFeatureTypes();
      firstTime = true;
    }
    if (firstTime) {
      stateStreamController.stream.listen((event) {
        _updated = false;
      });
      if (configuration.isMaster) {
        await reloadPlayers();
        await reloadCharacters();
        await reloadLocations();
      } else {
        if (_player == null) {
          if (playerID == 0) return;
          PlayerDesc p = await PlayerDesc.loadPlayer(playerID);
          player = p;
          firstTime = true;
        }

        if (_char == null || !net.webSocketReady) {
          await reloadCharacter();
          await reloadCharacters();
        }
        await reloadDeals();
      }
      await reloadTransitions();
      await reloadChats();
      await reloadTimers();
      await reloadContainers();
    }

    if (!net.webSocketReady) {
      for (var ft in globalListeners) {
        ft.reloadValues();
      }
    }
    if (!configuration.isMaster && !autoTransitionInhibited) {
      for (String transname in configuration.autoTransitions) {
        TransitionDesc? trans = transitions[transname];
        if (trans == null) continue;
        if (trans.isLoading()) {
          continue;
        }
        if (trans.passed) {
          trans.apply();
          break; //1 транзишен за раз. Они могут быть связаны
        }
      }
    }
    updated = true;
    //{ "type": "once",  "time": 300, "trans": "Decrease-health"}
    log("Update done in ${DateTime.now().difference(start).inMilliseconds} ms");
    net.recheckWebsocket();
  }

  Future<void> reloadAll() async {
    await reloadFeatureTypes();
    if (configuration.isMaster) {
      await reloadPlayers();
      await reloadCharacters();
      await reloadLocations();
    } else {
      if (_char == null) return;
      await reloadCharacter(forced: true);
      await reloadDeals(force: true);
    }
    await reloadTransitions(force: true);
    await reloadChats();
    await reloadTimers();
    await reloadContainers();
    net.recheckWebsocket();
  }

  Future<void> reloadTransitions({bool force = false}) async {
    var resp = await net.addToQueue("get", ["character", "transitions"], "");
    if (resp.response!.data is List<dynamic>) {
      for (var r in resp.response!.data) {
        var tran = TransitionDesc.fromJson(r);

        if (force || tran.newer(transitions[tran.key])) {
          log("Transtion ${tran.key} updated");
          transitions[tran.key] = tran;
          transTree.put(tran.keySplit, tran);
          updated = true;
        }
      }
      if (!configuration.isMaster &&
          featureTypesByName.containsKey("Inited") &&
          transitions.containsKey("Init") &&
          !_char!.featuresByTypeName.containsKey("Inited")) {
        transitions['Init']!.apply();
      }
    }
  }

  Future<void> reloadDeals({bool force = false}) async {
    Request resp = await net.addToQueue("get", ["character", "deals"], "");
    if (resp.response!.data is List<dynamic>) {
      for (var r in resp.response!.data) {
        var deal = DealDesc.fromJson(r);
        if (deals.containsKey(deal.pk)) {
          if ((force && deal.compare(deals[deal.pk]!)) || deal.newer(deals[deal.pk]!)) {
            log("Deal ${deal.pk} updated");
            notifyUser(eventType.DEAL_UPDATED);
            deals[deal.pk] = deal;
            updated = true;
          }
        } else {
          log("Deal ${deal.pk} added");
          deals[deal.pk] = deal;
          notifyUser(eventType.NEW_DEAL);
          updated = true;
        }
      }
    }
  }

  Future<void> reloadCharacter({bool forced = false}) async {
    int pk = _player!.character;

    CharacterDesc chr = await CharacterDesc.loadCharacter(pk);
    if (_char == null || forced || (chr.lastModified.compareTo(_char!.lastModified) > 0)) {
      if (chr.generateNotifyForDiff(char)) {
        notifyUser(eventType.CHARACTER_UPDATED);
      }
      _char = chr;
      updated = true;
    }
  }

  Future<void> reloadCharacters() async {
    Request resp = await net.addToQueue(
      "get",
      ["character"],
      "",
    );
    List<dynamic> jcharacters = resp.response!.data;
    for (Map<String, dynamic> jchar in jcharacters) {
      CharacterDesc chr = CharacterDesc.fromJson(jchar);
      characters[chr.id] = chr;
      charactersByName[chr.name] = chr;
    }
  }

  Future<void> reloadPlayers() async {
    Request resp = await net.addToQueue(
      "get",
      ["player"],
      "",
    );
    List<dynamic> jplayers = resp.response!.data;
    for (Map<String, dynamic> jplayer in jplayers) {
      PlayerDesc pl = PlayerDesc.fromJson(jplayer);
      players[pl.id] = pl;
    }
  }

  Future<void> reloadFeatureTypes() async {
    Request resp = await net.addToQueue(
      "get",
      ["character", "feature-types"],
      "",
    );
    lastState.updateFeatureTypes(resp.response!.data);
  }

  Future<void> reloadChats() async {
    var resp = await net.addToQueue("get", ["character", "chats"], "");
    List<dynamic> _chats = resp.response?.data ?? [];
    for (var _c in _chats) {
      Map<String, dynamic> c = _c;
      var chat = Chat.fromJsom(c);
      if (!chats.containsKey(chat.pk)) {
        chats[chat.pk] = chat;
      } else {
        chats[chat.pk]!.members = chat.members;
      }
    }
  }

  Future<void> reloadTimers() async {
    var resp = await net.addToQueue("get", ["character", "timers"], "");
    List<dynamic> _timers = resp.response?.data ?? [];
    timers.clear();
    for (var _t in _timers) {
      Map<String, dynamic> t = _t;
      var timer = ServerTimer.fromJsom(t);
      timers[timer.pk] = timer;
    }
  }

  Future<void> reloadContainers() async {
    var resp = await net.addToQueue("get", ["character", "containers"], "");
    List<dynamic> jcontainers = resp.response?.data ?? [];
    for (var jdyn in jcontainers) {
      Map<String, dynamic> c = jdyn;
      var cont = FeatureContainer.fromJSON(c);
      containers[cont.pk] = cont;
    }
    if (!configuration.isMaster) {
      for (FeatureContainer c in containers.values) {
        if (c.isAnonymous()) await c.reload();
      }
    }
  }

  Future<void> reloadLocations() async {
    var resp = await net.addToQueue("get", ["location"], "");
    List<dynamic> jlocations = resp.response?.data ?? [];
    for (var jdyn in jlocations) {
      Map<String, dynamic> l = jdyn;
      var loc = Location.fromJson(l);
      locations[loc.pk] = loc;
    }
  }

//TODO: возможно надо распилить
  void update(Map<String, dynamic> data) {
    try {
      String event = data["event"];
      switch (event) {
        case "character":
          int charid = data["id"];
          log("update charid $charid");
          if (_char?.id == charid) {
            if (_char?.update(data) ?? false) {
              updated = true;
              notifyUser(eventType.CHARACTER_UPDATED);
            }
            return;
          }
          if (characters.containsKey(charid)) {
            if (characters[charid]?.update(data) ?? false) updated = true;
            return;
          }
          log("Unknown user id $charid");
          break;
        case "feature":
          log("update feature $data");
          int? character = data["character_id"];
          int res = data["resource"];
          if (character == null) //Global variable
          {
            int featureId = data["feature"];
            FeatureTypeDesc? ft = featureTypesByID[featureId];
            if (ft == null) {
              log("Global Feature type $featureId not found");
              reloadAll();
              return;
            }
            if (ft.updateValue(res.toString(), data["value"])) {
              updated = true;
              ft.invalidateRelatedTransitions();
            }
            log("Global feature ${ft.name} $res updated to ${data["value"]}");
            return;
          }
          if (!_char!.featureByResource.containsKey(res)) {
            FeatureDesc feature = FeatureDesc.fromUpdate(data);
            _char!.features.add(feature);
            _char!.updateFeatureIndices();
            notifyUserFeature(eventType.INVENTORY_UPDATED, feature.typeDesc);
            feature.typeDesc.invalidateRelatedTransitions();
            updated = true;
            return;
          }
          FeatureDesc feature = _char!.featureByResource[res]!;
          if (feature.update(data)) {
            notifyUserFeature(eventType.INVENTORY_UPDATED, feature.typeDesc);
            if (featureUpdateListeners.containsKey(feature.name)) {
              featureUpdateListeners[feature.name]!(feature);
            }
            updated = true;
          }
          break;
        case "featureDelete":
          log("delete feature $data");
          int? character = data["character_id"];
          int res = data["resource"];
          if (character == null) //Global variable
          {
            int featureId = data["feature"];
            FeatureTypeDesc? ft = featureTypesByID[featureId];
            if (ft == null) {
              log("Global Feature type $featureId not found");
              reloadAll();
              return;
            }
            ft.values.remove(res.toString());
            log("Global feature ${ft.name} $res removed");
            return;
          }
          if (!_char!.featureByResource.containsKey(res)) {
            log("Resource $res not found $data reloading all");
            reloadAll();
            return;
          }
          FeatureDesc feature = _char!.featureByResource[res]!;
          _char!.features.remove(feature);
          _char!.updateFeatureIndices();
          feature.typeDesc.invalidateRelatedTransitions();
          updated = true;
          break;
        case "deal":
          log("Update deal $data");
          int dealID = data["pk"]!;
          if (!deals.containsKey(dealID)) {
            DealDesc d = DealDesc.fromJson(data);
            deals[dealID] = d;
            notifyUser(eventType.NEW_DEAL);
            updated = true;
            return;
          } else {
            DealDesc d = deals[dealID]!;
            if (d.update(data)) {
              notifyUser(eventType.DEAL_UPDATED);
              updated = true;
            }
          }
          break;
        case "dealOperation":
          log("Update deal operation $data");
          DealDesc deal = deals[data["deal"]!]!;
          int opID = data["pk"]!;
          DealOperationDesc? op = deal.getOperationById(opID);
          if (op == null) {
            DealOperationDesc o = DealOperationDesc.fromJson(data);
            deal.operations.add(o);
            notifyUser(eventType.DEAL_UPDATED);
            updated = true;
          } else {
            if (op.update(data)) {
              notifyUser(eventType.DEAL_UPDATED);
              updated = true;
            }
          }
          deal.updateDealOperationIndices();
          break;
        case "dealOperationDelete":
          log("Delete deal operation $data");
          DealDesc deal = deals[data["deal"]!]!;
          int opID = data["pk"]!;
          DealOperationDesc? op = deal.getOperationById(opID);
          if (op != null) {
            deal.operations.remove(op);
            deal.updateDealOperationIndices();
            notifyUser(eventType.DEAL_UPDATED);
            updated = true;
          }

          break;
        case "message":
          log("new message $data");
          Message m = Message.fromJSON(data);
          if (chats.containsKey(m.chat)) {
            Chat chat = chats[m.chat]!;
            if (!chat.messages.containsKey(m.messageid)) {
              chat.messages[m.messageid] = m;
              chat.messagesCount++;
              if (m.messageid > chat.maxMessageID) {
                chat.maxMessageID = m.messageid;
              }
              chat.streamController.add(null);
              notifyUserMessage(chat.pk);
            }
          }
          break;
        case "chat":
          log("new chat $data");
          Chat c = Chat.fromJsom(data);
          if (!chats.containsKey(c.pk)) {
            chats[c.pk] = c;
            updated = true;
          }
          break;
        case "chatMember":
          log("new chat member $data");
          ChatMember m = ChatMember.fromJSON(data);
          if (chats.containsKey(m.chat_id)) {
            Chat c = chats[m.chat_id]!;
            if (!c.members.containsKey(m.pk)) {
              c.members[m.pk] = m;
              c.streamController.add(null);
              updated = true;
            }
          }
          break;
        case "timer":
          log("new timer $data");
          var t = ServerTimer.fromJsom(data);
          timers[t.pk] = t;
          updated = true;
          break;
        case "containerItem":
          log("Update container item $data");
          var i = ContainerItem.fromJSON(data);
          FeatureContainer? cont = containers[i.containerPK];
          if (cont == null) {
            log("Error: container ${i.containerPK} not found");
          } else {
            i.container = cont;
            cont.items[i.featureTypePk] = i;
            updated = true;
          }
          break;
        case "container":
          log("Update continaer $data");
          var c = FeatureContainer.fromJSON(data);
          if (containers.containsKey(c.pk)) {
            containers[c.pk]!.ownerID = c.ownerID;
            containers[c.pk]!.name = c.name;
            containers[c.pk]!.extra = c.extra;
            if (DataSimple.isDataSimple(c.extra)) {
              containers[c.pk]!.extraSimple = c.extraSimple;
              ;
            }
            updated = true;
          } else {
            containers[c.pk] = c;
            updated = true;
          }
          break;
        default:
          log("Unknown event type $event");
      }
    } on TypeError catch (e) {
      log("Wrong update message $data");
      log("${e.stackTrace}");
      log(e.toString());
    }
  }

  set player(PlayerDesc? plr) {
    if (_player == null || plr != _player) {
      _player = plr;
      updated = true;
    }
  }

  PlayerDesc? get player => _player;

  CharacterDesc? get char => _char;

  set char(CharacterDesc? d) {
    if (_char == null || (d!.lastModified.compareTo(_char!.lastModified)) > 0) {
      updated = true;
      _char = d;
    }
  }

  bool get updated => _updated;

  set updated(bool n) {
    if (n && !_updated) {
      stateStreamController.add(this);
    }
    _updated = n;
  }

  void updateFeatureTypes(List<dynamic> json) {
    json.forEach((ft) {
      if (ft is! Map<String, dynamic>) {
        log("wrong type $ft");
        return;
      }
      var f = FeatureTypeDesc.fromJson(ft);
      featureTypesByID[f.pk] = f;
      featureTypesByName[f.name] = f;
    });
  }

  void logout() {
    playerID = 0;
    configuration.isMaster = false;
    _player = null;
    _char = null;
    deals.clear();
    characters.clear();
    transitions.clear();
    transTree = TreeElement();
    containers.clear();
    chats.clear();
    featureTypesByID.clear();
    featureTypesByName.clear();
    featureUpdateListeners.clear();
    timers.clear();
    stateStreamController.add(this);
    net.closeWebSocket();
  }

  void reset() {
    preferences!.remove("CurrentGame");
    logout();
  }

  void loadPrefsData() {
    playerID = preferences?.getInt(configuration.prefsPrefix + PLAYER_ID_KEY) ?? 0;
    configuration.isMaster = preferences?.getBool(configuration.prefsPrefix + masterModeKey) ?? false;
  }

  CharacterDesc? getCharacterById(int id) {
    var ret = characters[id];
    if (ret == null) {
      CharacterDesc.loadCharacter(id).then((loaded) {
        if (loaded.newer(characters[id])) {
          characters[id] = loaded;
          updated = true;
        }
      });
    }
    return ret;
  }

  Future<DealDesc> newDeal(int other) async {
    var json = <String, dynamic>{};
    json["initiator"] = _char!.id;
    json["acceptor"] = other;
    var request = jsonEncode(json);
    var r = await net.addToQueue("post", ["character", "deals"], request);
    var deal = DealDesc.fromJson(r.response!.data);
    deals[deal.pk] = deal;
    updated = true;
    return deal;
  }

  Future<int> getHackDefensiveLevel(int target) async {
    var ret = await net.addToQueue("GET", ["hack", target.toString()], "");
    Map<String, dynamic> json = ret.response?.data as Map<String, dynamic>;
    int deflevel = loadAndCheck(json, "defensive_hack_level");
    int pk = loadAndCheck(json, "pk");
    if (pk != target) {
      log("Wrong pk number $deflevel and $pk");
      return -1;
    }
    return deflevel;
  }

  Future<List<FeatureDesc>> hackInspect(int target) async {
    List<FeatureDesc> ret = [];
    var req = await net.addToQueue("GET", ["hack", target.toString(), "inspect"], "");
    List<dynamic> items = req.response?.data;
    for (Map<String, dynamic> i in items) {
      ret.add(FeatureDesc.fromJson(i));
    }
    return ret;
  }

//[{
//     "buyer": <pk>,
//     "operation": Trade|Copy,
//     "resource": <pk>,
//     "feature": <pk>,
//     "amount": <int>
// }]
  Future<bool> doHack(int target, PickResult pick) async {
    List<Map<String, dynamic>> request = [];

    for (var p in pick.checked) {
      Map<String, dynamic> m = {};
      m["operation"] = "Trade";
      m["buyer"] = char!.id;
      m["resource"] = p;
      request.add(m);
    }
    for (var p in pick.amount.entries) {
      Map<String, dynamic> m = {};
      m["operation"] = "Trade";
      m["buyer"] = char!.id;
      m["feature"] = p.key;
      m["amount"] = p.value;
      request.add(m);
    }
    var resp = await net.addToQueue("POST", ["hack", target.toString(), "hack"], json.encode(request));
    Map<String, dynamic>? respjson = resp.response?.data;
    return respjson?["success"] ?? false;
  }

//{
//     "label": "",
//     "members": []
// }
  Future<int> newChat(String label, List<int> chars, List<String> masters) async {
    if (!configuration.isMaster) {
      masters.add(char!.id.toString());
    }
    for (int c in chars) {
      masters.add(c.toString());
    }
    Map<String, dynamic> json = {"label": label, "members": masters};
    var resp = await net.addToQueue("POST", ["character", "chats"], jsonEncode(json));
    Chat c = Chat.fromJsom(resp.response!.data);
    chats[c.pk] = c;
    updated = true;
    return c.pk;
  }

  ServerTimer? getTimerByName(String name, {bool onlyNotPerformed = true}) {
    for (var t in timers.values) {
      if (onlyNotPerformed && t.performed) continue;
      if (t.title == name) return t;
    }
    return null;
  }

  ServerTimer? getTimerStartByName(String name, {bool onlyNotPerformed = true}) {
    for (var t in timers.values) {
      if (onlyNotPerformed && t.performed) continue;
      if (t.title.startsWith(name)) return t;
    }
    return null;
  }

  Future<void> deleteContainer(FeatureContainer cont) async {
    await net.addToQueue("DELETE", ["character", "containers", cont.pk.toString()], "");
    containers.remove(cont.pk);
    updated = true;
  }
}

enum ErrorSubsystem {
  Unknown,
  Network,
  Bluetooth,
  System,
  NeedUpdate,
}

class Error {
  late String message;
  int? code;
  ErrorSubsystem? subsystem;

  Error() {
    message = "";
    code = 0;
    subsystem = ErrorSubsystem.Unknown;
  }

  Error.bluetooth(String m) {
    code = 0;
    message = m;
    subsystem = ErrorSubsystem.Bluetooth;
  }

  Error.system(String m) {
    code = 0;
    message = m;
    subsystem = ErrorSubsystem.System;
  }

  Error.network(int? c, String m) {
    code = c;
    message = m;
    subsystem = ErrorSubsystem.Network;
  }

  Error.needUpdate(int newversion) {
    code = newversion;
    message = "Необходимо обновление";
    subsystem = ErrorSubsystem.NeedUpdate;
  }
}
