import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'config.dart';
import 'load.dart';
import 'model.dart';
import 'network.dart';

const String PLAYER_ID_KEY = "_player_id";
const String masterModeKey = "_is_master";

late Configuration configuration;
late NetworkEngine net;

late StateStore lastState;

late StreamController<StateStore> stateStreamController;
SharedPreferences? preferences;
late LoadManager loadManager;
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
bool errorShowed = false;

void addError(Error e) {
  lastState.addError(e);
  stateStreamController.add(lastState);
  if (navigatorKey.currentContext != null && !errorShowed) {
    showDialog(context: navigatorKey.currentContext!, builder: (ctx) => _getErrorWidget(e, ctx, lastState));
  }
}

T loadAndCheck<T>(Map<String, dynamic> json, String name) {
  if (!json.containsKey(name)) throw FormatException("Key $name not found");
  var ret = json[name];
  if (ret is! T) throw FormatException("Key $name wrong type");
  return ret;
}

T loadOrDefault<T>(Map<String, dynamic> json, String name, T def) {
  if (!json.containsKey(name)) return def;
  var ret = json[name];
  if (ret is! T) return def;
  return ret;
}

Widget loadIndicatorBuilder() {
  return StreamBuilder<NetworkStatus>(
      initialData: NetworkStatus.idle(),
      stream: net.status.stream as Stream<NetworkStatus>?,
      builder: (BuildContext context, AsyncSnapshot<NetworkStatus> snapshot) {
        if (!snapshot.hasData) return const SizedBox.shrink();
        if (snapshot.data!.status == EnumNetworkStatus.IDLE) {
          return IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: () {
                lastState.reloadAll();
              });
        }
        if (snapshot.data!.status == EnumNetworkStatus.LOADING) {
          return Stack(
              alignment: Alignment.center,
              children: [const CircularProgressIndicator(), Text("${snapshot.data!.packetsQueued}")]);
        }
        return const SizedBox.shrink();
      });
}

//Widget? getErrorWidgetSnap(AsyncSnapshot<StateStore?> state) =>
//    getErrorWidget(state.data!);

Widget _getErrorWidget(Error error, BuildContext context, StateStore state) {
  if (error.subsystem == ErrorSubsystem.NeedUpdate) {
    return AlertDialog(
      title: const Text("Необходимо обновление"),
      content: Text("Даннное приложение было обновлено. Необходимо скачать новую версию ${error.code}."),
      actions: [
        TextButton(
          child: const Text("Скачать"),
          onPressed: () {
            launchUrl(Uri.parse("https://${configuration.activeServer.path}/static/client/${error.code}/app.apk"));
            state.removeError(error);
          },
        ),
        TextButton(
          child: const Text("Отмена"),
          onPressed: () {
            state.removeError(error);
          },
        )
      ],
    );
  }

  List<Widget> actions = [];
  actions.add(TextButton(
    child: const Text("ok"),
    onPressed: () {
      state.removeError(error);
      Navigator.of(context).pop();
      errorShowed = false;
    },
  ));
  int errors = state.errors.length;
  if (errors > 1) {
    actions.add(TextButton(
        onPressed: () {
          state.clearErrors();
          Navigator.of(context).pop();
          errorShowed = false;
        },
        child: Text("Закрыть все $errors ошибок ")));
  }
  if (error.subsystem == ErrorSubsystem.Network && error.code == 403) {
    actions.insert(
        0,
        TextButton(
            onPressed: () {
              state.removeError(error);
              state.logout();
              Navigator.of(context).pop();
            },
            child: const Text("Ввести")));
    errorShowed = true;
    return AlertDialog(
        title: const Text("Ошибка авторизации"),
        content: const Text("Прозошла ошибка авторизации. Возможно нужно ввести пароль заново"),
        actions: actions);
  }
  return AlertDialog(
    title: Text("Ошибка ${error.code}"),
    content: Text(error.message),
    actions: actions,
  );
}

typedef MainMenuElementCheck = bool Function(StateStore? state);

class MainMenuElement {
  String? path;
  String? label;
  MainMenuElementCheck? checkFunc;

  MainMenuElement(p, l, {MainMenuElementCheck? onCheck}) {
    path = p;
    label = l;
    checkFunc = onCheck;
  }

  bool check(StateStore? st) {
    if (checkFunc == null) return true;
    return checkFunc!(st);
  }
}

Future<void> captureQrCode(BuildContext context) async {
  var dynres = await Navigator.of(context).pushNamed("/qrscanner");
  QRScannerResult? res = dynres as QRScannerResult?;
  if (res == null) {
    log("QRCode scanner return no data");
    return;
  }
  if (configuration.isMaster) {
    Navigator.of(context).pushNamed("/master/qrcode", arguments: res);
  } else {
    configuration.qrActions[res.code!]?.updateQResult(res);
    Navigator.of(context).pushNamed("/qractions", arguments: res);
  }
}

Widget getCommonDrawer(BuildContext context) {
  List<MainMenuElement> elements = [];
  elements.add(MainMenuElement("/login", "Вход игрока", onCheck: (st) {
    return st!.char == null;
  }));
  elements.add(MainMenuElement("/loginMaster", "Вход мастера", onCheck: (st) => st!.char == null));
  elements.add(MainMenuElement("/register", "Регистрация", onCheck: (st) {
    return st!.char == null && configuration.selfRegister;
  }));
  for (var e in configuration.menuElements) {
    elements.add(MainMenuElement(e.menu, e.label, onCheck: (st) {
      return st!.char != null;
    }));
  }
  elements.add(MainMenuElement("/qrtest", "Тестировние QR", onCheck: (st) => kDebugMode));

  return Drawer(
      child: StreamBuilder<StateStore?>(
          initialData: lastState,
          stream: stateStreamController.stream,
          builder: (BuildContext context, AsyncSnapshot<StateStore?> shot) {
            List<Widget> widgets = [];
            List<Widget> icons = [];
            icons.add(IconButton(
                onPressed: () {
                  captureQrCode(context);
                },
                icon: const Icon(Icons.qr_code)));
            icons.add(loadIndicatorBuilder());
            icons.add(
              IconButton(
                icon: const Icon(Icons.settings),
                onPressed: () {
                  Navigator.of(context).pushNamed("/settings");
                },
              ),
            );

            widgets.add(ListTile(
                title: Row(
              children: icons,
            )));
            if (configuration.isMaster) {
              widgets.add(ListTile(
                title: const Text("Персонажи"),
                onTap: () => Navigator.of(context).pushNamed("/master/characters"),
              ));
              widgets.add(ListTile(
                title: const Text("Игроки"),
                onTap: () => Navigator.of(context).pushNamed("/master/players"),
              ));
              widgets.add(ListTile(
                title: const Text("Локации"),
                onTap: () => Navigator.of(context).pushNamed("/master/locations"),
              ));
              widgets.add(ListTile(
                title: const Text("Транзишены"),
                onTap: () => Navigator.of(context).pushNamed("/master/transitions"),
              ));
              widgets.add(ListTile(
                title: const Text("Сообщения"),
                onTap: () => Navigator.of(context).pushNamed("/chats"),
              ));
              widgets.add(ListTile(
                title: const Text("Массовая рассылка"),
                onTap: () => Navigator.of(context).pushNamed("/master/masssend"),
              ));
              widgets.add(ListTile(
                title: const Text("Контейнеры"),
                onTap: () => Navigator.of(context).pushNamed("/containers"),
              ));
              widgets.add(ListTile(
                title: const Text("Карта"),
                onTap: () => Navigator.of(context).pushNamed("/map"),
              ));
              widgets.add(ListTile(
                title: const Text("Проверка QRcode"),
                onTap: () => Navigator.of(context).pushNamed("/qrtest"),
              ));
              widgets.add(ListTile(
                title: const Text("Дайс"),
                onTap: () => Navigator.of(context).pushNamed("/dice"),
              ));
            } else {
              for (var e in elements) {
                if (e.check(shot.data)) {
                  widgets.add(ListTile(
                      onTap: () {
                        Navigator.of(context).pushNamed(e.path!);
                      },
                      title: Text(e.label!)));
                }
              }
            }
            return ListView(children: widgets);
          }));
}

class OmniPlatform {
  late String slash;
  late bool hasForeground;
  late bool hasBluetooth;
  late bool hasNotification;

  OmniPlatform() {
    if (kIsWeb) {
      slash = "/";
      hasBluetooth = false;
      hasForeground = false;
      hasNotification = false;
    } else {
      if (Platform.isAndroid) {
        slash = "/";
        hasForeground = true;
        hasBluetooth = true;
        hasNotification = true;
      }
      if (Platform.isLinux) {
        slash = "/";
        hasForeground = false;
        hasBluetooth = false;
        hasNotification = false;
      }
      if (Platform.isWindows) {
        slash = "\\";
        hasForeground = false;
        hasBluetooth = false;
        hasNotification = false;
      }
    }
  }

  Future<String> getFilePath(String name) async {
    if (kIsWeb) {
      return "/assets/$name";
    }
    if (Platform.isWindows) {
      final directory = await getApplicationDocumentsDirectory();
      return "${directory.path}\\omnilight\\$name";
    }
    final directory = await getApplicationDocumentsDirectory();
    return "${directory.path}${platform.slash}$name";
  }
}

var platform = OmniPlatform();
