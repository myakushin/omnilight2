import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:omnilight2/config.dart';
import 'package:omnilight2/global.dart';
import 'package:omnilight2/model.dart';

enum RequestStatus { Queued, Runned, Failed, Done }

enum ResponseStatus {
  OK,
  NoConnectionToServer,
  ServerError,
  RequestError,
  UnknownError,
  InvalidLogin,
  BadCookie,
  BadTransition
}

typedef OnDoneFunction = void Function(Request);
typedef OnSuccessFunction = void Function();

class Request {
  final Completer _completer = Completer<Request>();
  List<String> path = [];
  ResponseStatus? responseStatus;
  String? respMessage;
  String? method;
  late DateTime startTime, endTime;
  Response? response;
  Headers? retHeaders;
  String? request;
  ServerConfig? server;
  int retries = 3;
  DioError? lastError;
  Map<String, dynamic> options = {};
  int dedupLevel = 0;

  get future => _completer.future;

  Uri getUri() {
    server = configuration.activeServer;
    String upath = "/api/";
    for (var e in path) {
      upath = "$upath$e";
      if (!upath.endsWith("/")) upath = upath + "/";
    }
    return Uri.https(server!.path, upath, options);
  }

  Future<bool> run(Dio client, Cookie? auth) async {
    var opt = Options(method: method, contentType: "application/json");
    if (auth != null) opt.headers = {'Cookie': auth};
    startTime = DateTime.now();

    try {
      Response resp = await client.requestUri(getUri(), data: request, options: opt);

      retHeaders = resp.headers;
      response = resp;
      endTime = DateTime.now();
      log("Response to ${getUri()} in ${endTime.difference(startTime).inMilliseconds} ms");

      server!.doneOk();
      _completer.complete(this);
    } on DioError catch (err) {
      log("Error");
      log("${err.error}");
      lastError = err;
      return false;
    }
    return true;
  }

  bool compare(Request other) {
    if (method != other.method) return false;
    if (request != other.request) return false;

    if (path.length != other.path.length) return false;

    for (int i = 0; i < path.length; i++) {
      if (path[i] != other.path[i]) return false;
    }
    if (options.length != other.options.length) return false;

    for (var o in options.entries) {
      if (other.options[o.key] != o.value) return false;
    }

    return true;
  }
}

enum EnumNetworkStatus { IDLE, LOADING, ERROR, UNKNOWN }

class NetworkStatus {
  int packetsQueued = 0;
  EnumNetworkStatus status = EnumNetworkStatus.UNKNOWN;

  NetworkStatus();

  NetworkStatus.idle() {
    status = EnumNetworkStatus.IDLE;
  }

  NetworkStatus.error() {
    status = EnumNetworkStatus.ERROR;
  }

  NetworkStatus.loading(int queued) {
    status = EnumNetworkStatus.LOADING;
    packetsQueued = queued;
  }
}

enum WebSocketStatus { INIT, CONNECTING, ONLINE, FAILED }

class NetworkEngine {
  static const String AUTH_COOKIE_KEY = "_authcookie";

  var client = Dio();
  List<Request> requests = [];
  late Timer timer;
  Cookie? authCookie;
  bool _tryLoaded = false;
  StreamController status = StreamController<NetworkStatus>.broadcast();
  DateTime? lastProgramUpdateCheck;
  WebSocket? webSocket;
  WebSocketStatus webSocketStatus = WebSocketStatus.INIT;

  get webSocketReady => webSocketStatus == WebSocketStatus.ONLINE;
  StreamController<Request> streamRequests = StreamController();

  NetworkEngine() {
    checkCookie();
    streamRequests.stream.listen(processRequest);
  }

  void dismiss() {
    status.close();
  }

  void resetCookie() {
    preferences!.remove(configuration.prefsPrefix + AUTH_COOKIE_KEY);
    authCookie = null;
  }

  get isReady => authCookie != null;

  void checkCookie() {
    if (isReady) return;
    if (preferences != null && !_tryLoaded) {
      _tryLoaded = true;
      var cookie = preferences!.getString(configuration.prefsPrefix + AUTH_COOKIE_KEY);

      if (cookie != null) {
        authCookie = Cookie.fromSetCookieValue(cookie);
      }
    }
  }

  void processError(Request r) {
    String errors = r.lastError?.message ?? "";
    var data = r.lastError?.response?.data;
    List<dynamic> listErrors = [];

    if (data is Map<String, dynamic>) {
      for (var dr in data.values) {
        if (dr is List<dynamic>) {
          listErrors = dr;
        }
        if (dr is Map<String, dynamic>) {
          listErrors = dr.values.toList();
        }
      }
    }
    if (data is List<dynamic>) listErrors = data;

    for (var e in listErrors) {
      if (e is String) {
        errors += e;
      }
      if (e is List<dynamic>) {
        errors += e.join(" ");
      }
    }
    addError(Error.network(r.lastError?.response?.statusCode, "${r.getUri().toString()} $errors"));
  }

  Future<void> processRequest(Request r) async {
    checkCookie();
    if (await r.run(client, authCookie)) {
      requests.remove(r);
    } else {
      if (r.retries > 0) {
        r.retries--;
        streamRequests.add(r);
      } else {
        processError(r);
        requests.remove(r);
        r._completer.completeError(r.lastError!);
      }
    }

    if (requests.isNotEmpty) {
      status.add(NetworkStatus.loading(requests.length));
    } else {
      status.add(NetworkStatus.idle());
    }
/*
    if (lastProgramUpdateCheck == null ||
        lastProgramUpdateCheck.difference(DateTime.now()).inSeconds > 300) {
      lastProgramUpdateCheck = DateTime.now();
      PackageInfo.fromPlatform().then((info) {
        int myBuildID = int.tryParse(info.buildNumber);
        String server = configuration.activeServer.path;
        String path = "static/client/version";
        client.requestUri(Uri.https(server, path)).then((resp) {
          if (resp.statusCode == 200) {
            var json = jsonDecode(resp.data);
            int newver = json['version'];
            log("Check update:New version $newver");
            if (newver > myBuildID) {
              lastState.addError(Error.needUpdate(newver));
            }
          }
        });
      });
    }*/
  }

  void recheckWebsocket() {
    if (!configuration.useWebSockets) return;
    if (webSocketStatus == WebSocketStatus.FAILED) {
      webSocketStatus = WebSocketStatus.INIT;
      log("Retry websocket connection");
    }

    if (webSocketStatus == WebSocketStatus.INIT) {
      webSocketStatus = WebSocketStatus.CONNECTING;
      webSocketConnect().then((value) {
        webSocketStatus = WebSocketStatus.ONLINE;
        log("Web socket online");
      }, onError: (value) {
        log("WebSocket connection error $value");
        webSocketStatus = WebSocketStatus.FAILED;
        throw value;
      });
    }
  }

  Future<void> webSocketConnect() async {
    Map<String, String> headers = {'Cookie': authCookie.toString()};
    webSocket = await WebSocket.connect("wss://${configuration.activeServer.path}/ws/events", headers: headers);
    webSocket!.listen(webSocketData, onError: webSocketError, onDone: webSocketDone);
  }

  void webSocketError(dynamic data) {
    log("WS error $data ${data.runtimeType}");
    webSocketStatus = WebSocketStatus.FAILED;
  }

  void webSocketDone() {
    log("WebSocket done");
    webSocketStatus = WebSocketStatus.FAILED;
  }

  void webSocketData(dynamic data) {
    String? sData = data as String?;
    if (sData == null) {
      log("Wrong data type ${data.runtimeType.toString()} $data");
      return;
    }
    Map<String, dynamic> jData = json.decode(sData);
    Map<String, dynamic>? payload = jData["payload"];
    if (payload == null) {
      log("Wrong data from WebSocket: $data");
      return;
    }
    lastState.update(payload);
  }

  Future<Request> addToQueue(String action, List<String> path, String request, {Map<String, dynamic>? options}) {
    var rq = Request();
    rq.method = action;
    rq.path = path;
    rq.request = request;
    if (options != null) rq.options = options;
    for (var r in requests) {
      if (rq.compare(r)) {
        r.dedupLevel++;
        log("Request $action to ${path.join("/")} deduplicated ${r.dedupLevel} times");
        return r.future;
      }
    }
    requests.add(rq);
    status.add(NetworkStatus.loading(requests.length));
    streamRequests.add(rq);
    return rq.future;
  }

  Future<bool> requestCookie(dynamic id, String password, {bool master = false}) async {
    late Request resp;
    if (!master) {
      int iid = id as int;
      String rq = jsonEncode({"pk": iid, "password": password});
      resp = await addToQueue("post", ["login"], rq);
    } else {
      String login = id as String;
      String rq = jsonEncode({"username": login, "password": password});
      resp = await addToQueue("post", ["loginAdmin"], rq);
    }

    log("${resp.response}");
    var hmap = resp.response!.headers.map;
    if (hmap.containsKey("set-cookie")) {
      var sc = hmap['set-cookie']!;

      for (var cookie in sc) {
        if (!cookie.contains("django_cyber_master_ssid")) continue;
        var c = Cookie.fromSetCookieValue(cookie);
        authCookie = c;
        preferences!.setString(configuration.prefsPrefix + AUTH_COOKIE_KEY, cookie);
      }
      if (!master) {
        lastState.playerID = id;
        preferences!.setInt(configuration.prefsPrefix + PLAYER_ID_KEY, lastState.playerID);
      }
      await closeWebSocket();
    }
    configuration.isMaster = master;
    preferences!.setBool(configuration.prefsPrefix + masterModeKey, master);
    return true;
  }

  Future<void> closeWebSocket() async {
    if (configuration.useWebSockets && webSocketStatus == WebSocketStatus.ONLINE) {
      await webSocket?.close();
      webSocketStatus = WebSocketStatus.INIT;
    }
  }

  Future<RegistrationAnswer> register(String name, String nickname, String charname, String medical) {
    var compl = Completer<RegistrationAnswer>();
    var rq = jsonEncode({
      "player": {"real_name": name, "nickname": nickname, "mediacal_info": medical},
      "character": {
        "name": charname,
      },
    });
    addToQueue("post", ["accounts", "register"], rq).then((resp) {
      var ans = RegistrationAnswer.fromJson(resp.response!.data);
      compl.complete(ans);
    });
    return compl.future;
  }
}

class RegistrationAnswer {
  late int pk;
  late String username;
  late String password;

  RegistrationAnswer.fromJson(Map<String, dynamic> json) {
    pk = loadAndCheck(json, "pk");
    username = loadAndCheck(json, "username");
    password = loadAndCheck(json, "password");
  }
}
