import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui' as ui;

import 'package:crypto/crypto.dart' as crypto;
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:omnilight2/global.dart';
import 'package:omnilight2/main.dart';
import 'package:omnilight2/model.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'inventory.dart' as inventory;
import 'trade.dart';

typedef QRActionFunc = void Function(int id, BuildContext ctx);
typedef InvElementCreateFunc = inventory.InventoryElementBase Function();
typedef QrTypeCreate = QRTypeBase Function(Map<String, dynamic> json);
typedef CharPropCreate = CharPropertyBase Function(String feature, Map<String, dynamic> json);

class QRAction {
  QRActionFunc? func;
  late String name;
  String? description;

  QRAction(String n, QRActionFunc? f)
      : func = f,
        name = n;

  QRAction.description(String d) {
    name = "1Описание";
    description = d;
  }
}

class QRScannerResult {
  String? code;
  int? number;
  Stream<List<QRAction>>? actions;
  String? name;
}

abstract class QRTypeBase {
  QRTypeBase(Map<String, dynamic> jsonArguments);

  void updateQResult(QRScannerResult res);

  List<Widget> masterInformation(QRScannerResult res) => [
        ListTile(
          title: Text("Тип $runtimeType"),
        )
      ];
}

class MenuElement {
  late String label;
  late String menu;

  MenuElement.fromJson(Map<String, dynamic> json) {
    label = loadAndCheck<String>(json, "label");
    menu = loadAndCheck(json, 'menu');
  }
}

class ServerConfig {
  late String path;
  int ok = 0;
  int error = 0;
  bool needReselect = false;
  bool isActive = false;

  ServerConfig.fromJson(String json) {
    path = json;
  }

  void doneOk() {
    ok++;
  }

  void doneError() {
    error++;
  }
}

class BLEAction {
  late String name;
  List<String> args = [];

  BLEAction.fromString(String input) {
    var s = input.split("(");
    if (s.isEmpty) throw (FormatException("Wrong string $input"));
    name = s[0].trim();
    if (s.length > 1) {
      args = s[1].replaceAll(")", "").split(",");
    }
  }
}

class BLEShowTotal {
  late String feature;
  late String label;

  BLEShowTotal.fromJson(Map<String, dynamic> json) {
    feature = loadAndCheck(json, "feature");
    label = loadAndCheck(json, "label");
  }
}

enum BLEActionTrigger { onEnter, onExit, onTimer }

class BLEConfig {
  late bool enabled;
  late Map<String, String> uuid;
  Map<int, Map<BLEActionTrigger, List<BLEAction>>> beaconActions = {};
  late int timeout;
  BLEShowTotal? showTotal;

  BLEConfig() {
    enabled = false;
    uuid = {};
  }

  BLEConfig.fromJson(Map<String, dynamic> json) {
    enabled = loadAndCheck(json, 'enabled');
    timeout = loadOrDefault(json, 'timeout', 60);
    uuid = {};
    Map<String, dynamic>? m = json['uuids'];
    if (m != null) {
      for (var kv in m.entries) {
        uuid[kv.key] = kv.value;
      }
    }
    Map<String, dynamic>? st = json['showTotal'];
    if (st != null) showTotal = BLEShowTotal.fromJson(st);
    Map<String, dynamic>? b = json['beacons'];
    if (b != null) {
      for (var kv in b.entries) {
        int? id = int.tryParse(kv.key);
        if (id == null) throw FormatException("Wrong number ${kv.key}");
        Map<String, dynamic> t = kv.value;

        for (var tkv in t.entries) {
          BLEActionTrigger trigger;
          switch (tkv.key) {
            case "onEnter":
              trigger = BLEActionTrigger.onEnter;
              break;
            case "onExit":
              trigger = BLEActionTrigger.onExit;
              break;
            case "onTimer":
              trigger = BLEActionTrigger.onTimer;
              break;
            default:
              throw FormatException("Wrong trigger ${tkv.key}");
          }
          List<dynamic> val = tkv.value;

          if (!beaconActions.containsKey(id)) beaconActions[id] = {};
          if (!beaconActions[id]!.containsKey(trigger)) {
            beaconActions[id]![trigger] = val.map((e) => BLEAction.fromString(e)).toList();
          }
        }
      }
    }
  }
}

class CharPropertyAction {
  late String name;
  late String transition;
  late bool checkMaximum;
  late bool checkMinimum;
  late String negLabel;
  late String description;
  Map<int, String> valDescription = {};

  CharPropertyAction.fromJson(Map<String, dynamic> json) {
    name = loadAndCheck(json, "name");
    transition = loadAndCheck(json, "transition");
    checkMaximum = loadOrDefault(json, "checkMaximum", false);
    checkMinimum = loadOrDefault(json, "checkMinimum", false);
    negLabel = loadOrDefault(json, "negLabel", "");
    description = loadOrDefault(json, "description", "");
    Map<String, dynamic> jvaldesc = json["valDesc"] ?? {};
    jvaldesc.forEach((key, value) {
      var i = int.tryParse(key) ?? 0;
      String val = value;
      valDescription[i] = val;
    });
  }

  String getDescriptionByVal(int val) => valDescription[val] ?? description;
}

abstract class CharPropertyBase implements Comparable<CharPropertyBase> {
  late String featureName;
  late String label;
  late int priority;
  late String description;

  CharPropertyBase.fromJson(String feature, Map<String, dynamic> json) {
    featureName = feature;
    label = loadOrDefault(json, "label", feature);
    priority = loadOrDefault(json, "priority", 9999);
    description = loadOrDefault(json, "description", "");
  }

  CharPropertyBase.undefined() {
    label = featureName = "Неизвестно";
  }

  Widget? getWidget(StateStore state, BuildContext ctx);

  @override
  int compareTo(CharPropertyBase other) {
    var c1 = priority.compareTo(other.priority);
    if (c1 == 0) return label.compareTo(other.label);
    return c1;
  }
}

class CharPropertyCharname extends CharPropertyBase {
  CharPropertyCharname.fromJson(super.feature, super.json) : super.fromJson();

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    return ListTile(title: Text("Имя персонажа: ${lastState.char!.name}"));
  }
}

class CharPropertyPlayer extends CharPropertyBase {
  CharPropertyPlayer.fromJson(super.feature, super.json) : super.fromJson();

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    return ListTile(title: Text("Имя игрока:${lastState.player!.nickname}"));
  }
}

class CharPropertyQRCode extends CharPropertyBase {
  CharPropertyQRCode.fromJson(super.feature, super.json) : super.fromJson();

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    return QrImage(
      data: "p${lastState.player!.id.toString().padLeft(6, '0')}",
      version: QrVersions.auto,
      size: 200,
      backgroundColor: const Color.fromARGB(255, 255, 255, 255),
    );
  }
}

class CharPropertyInteger extends CharPropertyBase {
  late int maximum;
  late int minimum;
  late String negLabel;
  String? ifExist;
  String? ifNotExist;
  String? altFeature;
  String? altLabel;
  String? altNegLabel;
  bool vertical = false;
  List<CharPropertyAction> actions = [];
  Map<int, String> valDescription = {};
  bool cumulative = false;

  CharPropertyInteger.fromJson(String propName, Map<String, dynamic> json) : super.fromJson(propName, json) {
    maximum = loadOrDefault(json, "maximum", 999);
    minimum = loadOrDefault(json, "minimum", -999);
    negLabel = loadOrDefault(json, "negLabel", "");
    ifExist = loadOrDefault(json, "ifExist", null);
    ifNotExist = loadOrDefault(json, "ifNotExist", null);
    altFeature = loadOrDefault(json, "altFeature", null);
    altLabel = loadOrDefault(json, "altLabel", null);
    altNegLabel = loadOrDefault(json, "altNegLabel", null);
    vertical = loadOrDefault(json, "vertical", false);
    cumulative = loadOrDefault(json, "cumulative", false);
    List<dynamic>? dactions = json['actions'];
    if (dactions != null) {
      for (var a in dactions) {
        actions.add(CharPropertyAction.fromJson(a));
      }
    }
    Map<String, dynamic> jvaldesc = json["valDesc"] ?? {};
    jvaldesc.forEach((key, value) {
      var i = int.tryParse(key) ?? 0;
      String val = value;
      valDescription[i] = val;
    });
  }

  int getValue(CharacterDesc char) {
    var f = char.featuresByTypeName[featureName];
    if (f == null) return 0;
    return f.first.valueInt;
  }

  String? generatePropText(CharacterDesc char) {
    var m = char.featuresByTypeName[maximum]?.first;
    int value = getValue(char);
    String ret = "";
    String showLabel = label;
    bool isAlt = false;
    if (altFeature != null && char.featuresByTypeName.containsKey(altFeature)) {
      isAlt = true;
      showLabel = altLabel ?? "НЕУКАЗАНО";
    }
    if (negLabel.isNotEmpty && value < 0) {
      if (isAlt) {
        showLabel = altNegLabel ?? "NEGНЕУКАЗАНО";
      } else {
        showLabel = negLabel;
      }
      value = -value;
    }
    if (m == null) {
      ret = "$showLabel $value";
    } else {
      int maxvalue = m.valueInt;
      ret = "$showLabel $value/$maxvalue";
    }
    return ret;
  }

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    if (ifExist != null && !state.char!.featuresByTypeName.containsKey(ifExist)) {
      return null;
    }
    if (ifNotExist != null && state.char!.featuresByTypeName.containsKey(ifNotExist)) return null;

    String? text = generatePropText(state.char!);
    if (text == null) return null;
    List<Widget> buttons = [];
    for (var act in actions) {
      var trans = state.transitions[act.transition];
      if (trans == null) continue;
      bool reached = false;
      if (act.checkMaximum) {
        //Получаем текущее и максимальное значение данной фичи
        int n = state.char!.featuresByTypeName[featureName]?.first.valueInt ?? 0;
        if (n >= maximum) reached = true;
      }
      if (act.checkMinimum) {
        //Получаем текущее и минимальное значение данной фичи
        int n = state.char!.featuresByTypeName[featureName]?.first.valueInt ?? 0;
        if (n <= minimum) reached = true;
      }
      String actLabel = act.name;
      int value = getValue(state.char!);
      if (act.negLabel.isNotEmpty && value < 0) {
        actLabel = act.negLabel;
      }
      late Widget b;
      if (trans.isLoading()) {
        b = const ElevatedButton(
          onPressed: null,
          child: CircularProgressIndicator(),
        );
      } else if (trans.passed && !reached) {
        b = ElevatedButton(child: Text(actLabel), onPressed: () => trans.apply());
      } else {
        b = ElevatedButton(
          onPressed: null,
          child: Text(actLabel),
        );
      }
      if (vertical) {
        buttons.add(Row(
          children: [
            b,
            Expanded(
                child: Text(
              act.getDescriptionByVal(value),
              overflow: TextOverflow.ellipsis,
              maxLines: 10,
            ))
          ],
        ));
      } else {
        buttons.add(b);
      }
    }
    Widget? wDescription;
    if (description.isNotEmpty) {
      wDescription = Text(description);
    }
    if (valDescription.isNotEmpty) {
      if (cumulative) {
        int val = getValue(lastState.char!);
        bool polarity = !val.isNegative;
        val = val.abs();
        String text = "";
        for (int i = val; i > 0; i--) {
          int ipol = polarity ? i : -i;
          text += valDescription[ipol] ?? "";
          text += "\n";
        }
        wDescription = Text(text);
      } else {
        wDescription = Text(valDescription[getValue(lastState.char!)] ?? "");
      }
    }
    if (vertical) {
      if (wDescription != null) buttons.insert(0, wDescription);
      return ListTile(
        title: Text(text),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: buttons,
        ),
      );
    }
    return ListTile(
      title: Text(text),
      subtitle: wDescription,
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: buttons,
      ),
    );
  }
}

class CharPropertyExist extends CharPropertyInteger {
  CharPropertyExist.fromJson(String name, Map<String, dynamic> json) : super.fromJson(name, json);

  @override
  String? generatePropText(CharacterDesc char) {
    if (char.featuresByTypeName.containsKey(featureName)) return label;
    return null;
  }
}

class CharPropertySelector extends CharPropertyBase {
  late String transitionPrefix;
  String prerequisite = "";
  String prerequisiteMessage = "";

  CharPropertySelector.fromJson(String propName, Map<String, dynamic> json) : super.fromJson(propName, json) {
    transitionPrefix = loadOrDefault(json, "transitionPrefix", "");
    prerequisite = loadOrDefault(json, "prerequisite", "");
    prerequisiteMessage = loadOrDefault(json, "prerequisiteMessage", "");
  }

  CharPropertySelector.undefined() : super.undefined() {
    transitionPrefix = "";
  }

  @override
  Widget getWidget(StateStore state, BuildContext ctx) {
    if (prerequisite != "" && state.char!.featuresByTypeName[prerequisite] == null) {
      return ListTile(
        title: Text(label),
        subtitle: Text(prerequisiteMessage),
      );
    }
    var featList = state.char!.featuresByTypeName[featureName];
    if (featList == null) {
      return ListTile(
          title: Text(label),
          subtitle: const Text("Не выбран(а)"),
          trailing: ElevatedButton(
              child: const Text("Выбрать"),
              onPressed: () => Navigator.of(ctx).pushNamed("/character/selector", arguments: this)));
    } else {
      FeatureDesc feat = featList.first;
      return ListTile(
        title: Text(label),
        subtitle: Text(feat.valueStr),
      );
    }
  }
}

class CharPropertyTimer extends CharPropertyBase {
  late String timerName;
  late String hideIf;

  CharPropertyTimer.fromJson(feature, json) : super.fromJson(feature, json) {
    timerName = loadAndCheck(json, "timer");
    hideIf = loadOrDefault(json, "hideIf", "");
  }

  @override
  Widget? getWidget(StateStore state, BuildContext ctx) {
    if (hideIf.isNotEmpty) {
      var hideFeature = lastState.char?.featuresByTypeName[hideIf] ?? [];
      if (hideFeature.isNotEmpty) {
        return ListTile(title: Text(hideIf));
      }
    }
    return ListTile(
        title: Text(label),
        subtitle: StreamBuilder<int>(
          initialData: 0,
          stream: Stream.periodic(const Duration(seconds: 1), (i) => i),
          builder: (BuildContext context, AsyncSnapshot<int> shot) {
            ServerTimer? timer = state.getTimerStartByName(timerName);
            String out = "таймер $timerName не найден";
            if (timer != null) {
              var d = timer.getTimeToExpire();
              int min = d.inMinutes;
              int sec = d.inSeconds - min * 60;
              out = "$min:$sec";
            }
            return Text(out);
          },
        ));
  }
}

class CharPropertyMenu extends CharPropertyBase {
  late String path;

  CharPropertyMenu.fromJson(String perName, Map<String, dynamic> json) : super.fromJson(perName, json) {
    path = loadAndCheck(json, "path");
  }

  @override
  Widget getWidget(StateStore state, BuildContext ctx) {
    return ElevatedButton(child: Text(label), onPressed: () => Navigator.of(ctx).pushNamed(path));
  }
}

class SkillConfiguration {
  late String classFeature;
  late String expFeature;
  late String transPrefix;
  late String crossClassFeature;
  late String crossTransPrefix;
  late String crossSkillGetPrefix;

  SkillConfiguration.fromJson(Map<String, dynamic> json) {
    classFeature = loadOrDefault(json, "ClassFeature", "");
    expFeature = loadOrDefault(json, "ExpFeature", "");
    transPrefix = loadOrDefault(json, "TransPrefix", "");
    crossClassFeature = loadOrDefault(json, "CrossClassFeature", "");
    crossTransPrefix = loadOrDefault(json, "CrossTransPrefix", "");
    crossSkillGetPrefix = loadOrDefault(json, "CCSkillPrefix", "");
  }

  SkillConfiguration() {
    classFeature = "";
    expFeature = "Experience";
    transPrefix = "GetSkill";
    crossSkillGetPrefix = crossTransPrefix = crossClassFeature = "";
  }
}

enum BLEPeripherialCharDirection { inbound, outbound, both }

enum BLEPeripheralCharType { int, string }

class BLEPeripheralsServiceConf {
  late String uuid; //or not String?
  late String feature;
  late String label;
  late String transitionInc;
  late String transitionDec;
  late BLEPeripherialCharDirection direction;
  late BLEPeripheralCharType type;

  BLEPeripheralsServiceConf.fromJson(String perUuid, Map<String, dynamic> json) {
    uuid = perUuid;
    feature = loadOrDefault(json, "feature", "");
    label = loadOrDefault(json, "label", feature);
    transitionInc = loadOrDefault(json, "transitionInc", "");
    transitionDec = loadOrDefault(json, "transitionDec", "");
    String dir = json["direction"] ?? "(undefined)";
    dir = dir.toLowerCase();
    switch (dir) {
      case "in":
        direction = BLEPeripherialCharDirection.inbound;
        break;
      case "out":
        direction = BLEPeripherialCharDirection.outbound;
        break;
      case "both":
        direction = BLEPeripherialCharDirection.both;
        break;
      default:
        throw FormatException("в описании службы BLE $uuid неправильно указан direction $dir");
    }
    String t = json["type"] ?? "(undefined)";
    t = t.toLowerCase();
    switch (t) {
      case "int":
        type = BLEPeripheralCharType.int;
        break;
      case "str":
        type = BLEPeripheralCharType.string;
        break;
      default:
        throw FormatException("в описании службы BLE $uuid неправильно указан type $dir");
    }
  }
}

class Order {
  late String transitionName;
  late String label;

  Order.fromJson(this.label, Map<String, dynamic> json) {
    transitionName = loadAndCheck(json, "Transition");
  }
}

enum OrderGroupType { transition, decision, decisionDefault }

class OrderGroup {
  late OrderGroupType type;
  List<Order> orders = [];
  late String label;
  late String defaultFeature, decisionFeature;
  late String defaultLabel, defaultSublabel, decisionLabel;
  static Map<String, OrderGroupType> typeNames = {
    "transition": OrderGroupType.transition,
    "decision": OrderGroupType.decision,
    "decision-default": OrderGroupType.decisionDefault
  };

  OrderGroup.fromJson(this.label, Map<String, dynamic> json) {
    String _type = loadOrDefault(json, "type", "transition");
    type = typeNames[_type]!;
    if (type == OrderGroupType.transition) {
      json.forEach((key, value) {
        var ljson = value as Map<String, dynamic>;
        orders.add(Order.fromJson(key, ljson));
      });
    }
    if (type == OrderGroupType.decision) {
      decisionFeature = loadAndCheck(json, "DecisionFeature");
      decisionLabel = loadAndCheck(json, "DecisionLabel");
    }
    if (type == OrderGroupType.decisionDefault) {
      defaultFeature = loadAndCheck(json, "DefaultFeature");
      defaultLabel = loadAndCheck(json, "DefaultLabel");
      defaultSublabel = loadOrDefault(json, "DefaultSublabel", "");
    }
  }
}

class Asset {
  late String name;
  late String url;
  late String md5;
  late File file;
  ui.Image? image;

  Asset.fromJson(this.name, Map<String, dynamic> json) {
    url = loadAndCheck(json, "url");
    md5 = loadOrDefault(json, "md5sum", "");
  }

  Future<File> getFile() async {
    String path = await platform.getFilePath(name);
    return File(path);
  }

  Future<void> download(File toFile) async {
    Uri uri = Uri.https(configuration.activeServer.path, url);
    var resp = await net.client.downloadUri(uri, toFile.path);
    if (resp.statusCode != 200) {
      throw resp;
    }
  }

  Future<void> load() async {
    if (kIsWeb) {
      Uri uri = Uri.https(configuration.activeServer.path, url);
      var resp = await net.client.getUri(uri, options: Options(responseType: ResponseType.bytes));
      if (resp.statusCode != 200) {
        throw resp;
      }
      log(resp.data);
      ui.decodeImageFromList(resp.data!, (result) {
        image = result;
      });
      return;
    }
    file = await getFile();
    log("Loading $name to ${file.path}");
    if (!file.existsSync()) {
      log("File not exist downloading");
      await download(file);
    }
    if (md5.isNotEmpty) {
      var digest = crypto.md5.convert(await file.readAsBytes()).toString();
      if (md5 == digest) {
        log("md5 of $name is OK");
      } else {
        log("Wrong checksum $digest!=$md5. reloading");
        await file.delete();
        await download(file);
      }
    }
    if (name.endsWith(".jpg") || name.endsWith(".png")) {
      final data = await file.readAsBytes();
      ui.decodeImageFromList(data, (result) {
        image = result;
      });
    }
  }
}

enum MapObjectAttributeType { image, color }

class MapObjectAttribute {
  late MapObjectAttributeType type;
  late String data;
  late String name;
  final Map<String, MapObjectAttributeType> typeNames = {
    "img": MapObjectAttributeType.image,
    "color": MapObjectAttributeType.color
  };

  MapObjectAttribute.fromJson(this.name, String val) {
    if (!val.contains(":")) {
      throw FormatException("In $name not : in description ($val)");
    }

    List<String> lval = val.split(":");
    data = lval[1];
    if (!typeNames.containsKey(lval[0])) {
      throw FormatException("Unknown attribute type ${lval[0]} in $name");
    }
    type = typeNames[lval[0]]!;
  }
}

enum MapObjectActionType { order, orderQueue, details }

enum MapObjectOrderType { move, load, unload, start, jump, spy, explore }

class MaoObjectOrderArg {
  late String name;
  String? value;
  int? iValue;

  MaoObjectOrderArg.fromJson(String raw) {
    if (raw.contains("=")) {
      var lraw = raw.split("=");
      name = lraw[0].trim();
      value = lraw[1].trim();
      iValue = int.tryParse(value!);
    } else {
      name = raw;
    }
  }
}

class MapObjectOrder {
  late MapObjectOrderType type;
  List<MaoObjectOrderArg> args = [];
  static const Map<String, MapObjectOrderType> orderNames = {
    "move": MapObjectOrderType.move,
    "load": MapObjectOrderType.load,
    "unload": MapObjectOrderType.unload,
    "start": MapObjectOrderType.start,
    "jump": MapObjectOrderType.jump,
    "spy": MapObjectOrderType.spy,
    "explore": MapObjectOrderType.explore
  };

  MapObjectOrder.fromJSON(String order) {
    var lorder = order.split(":");
    String stype = lorder.removeAt(0);
    if (!orderNames.containsKey(stype)) {
      throw FormatException("Unknown object type $stype");
    }
    type = orderNames[stype]!;
    args = lorder
        .map(
          (e) => MaoObjectOrderArg.fromJson(e),
        )
        .toList();
  }
}

class MapObjectAction {
  late MapObjectActionType type;
  static const Map<String, MapObjectActionType> typesNames = {
    "Order": MapObjectActionType.order,
    "OrderQueue": MapObjectActionType.orderQueue,
    "Details": MapObjectActionType.details
  };

  List<MapObjectOrder> orders = [];

  MapObjectAction.fromJSON(String name, List<dynamic> json) {
    if (!typesNames.containsKey(name)) {
      throw FormatException("Unknown order type $name");
    }
    type = typesNames[name]!;
    orders = json.map((e) => MapObjectOrder.fromJSON(e)).toList();
  }

  MapObjectOrder? getOrderByName(String name) {
    MapObjectOrderType? type = MapObjectOrder.orderNames[name];
    if (type == null) {
      throw Exception("Unknown order type $name");
    }
    for (var ord in orders) {
      if (ord.type == type) {
        return ord;
      }
    }
    return null;
  }
}

class MapObject {
  late String name;
  late int prio;
  Map<String, MapObjectAttribute> attributes = {};
  MapObject? base;
  List<MapObjectAction> actions = [];

  MapObject.fromJson(this.name, Map<String, dynamic> json) {
    prio = loadOrDefault(json, "prio", 999);
    Map<String, dynamic> jattrib = json["attributes"];
    attributes = jattrib.map((key, value) => MapEntry(key, MapObjectAttribute.fromJson(key, value)));

    Map<String, dynamic> jactions = json["actions"] ?? {};
    jactions.forEach((key, value) {
      actions.add(MapObjectAction.fromJSON(key, value));
    });
  }

  MapObjectAttribute getAttr(String name) {
    if (attributes.containsKey(name)) {
      return attributes[name]!;
    }
    if (base != this && base != null) {
      return base!.getAttr(name);
    }
    throw Exception("Attribute $name not found for MapObject $name");
  }
}

class MapConfig {
  late String backgroundName;
  late String orderFeatureName;
  Rect backgroundRect = Rect.zero;
  Map<String, MapObject> objects = {};
  List<String> locationFeatures = [];
  late String mapMode;

  MapConfig.fromJson(Map<String, dynamic> json) {
    backgroundName = loadAndCheck(json, "background");
    orderFeatureName = loadOrDefault(json, "orderFeature", "");
    mapMode = loadOrDefault(json, "mapMode", "graph");
    List<dynamic> jfeatures = json["locationFeatures"] ?? [];
    locationFeatures = jfeatures.map((e) => e.toString()).toList();
    List<dynamic> bgRectData = json["backgroundRect"] ?? [];
    if (bgRectData.length >= 4) {
      backgroundRect = Rect.fromLTRB(
          bgRectData[0].toDouble(), bgRectData[1].toDouble(), bgRectData[2].toDouble(), bgRectData[3].toDouble());
    }
    Map<String, dynamic> ojson = json["objects"] ?? {};
    objects = ojson.map((key, value) => MapEntry(key, MapObject.fromJson(key, value)));
    if (objects.containsKey("base")) {
      var base = objects["base"]!;
      for (var obj in objects.values) {
        obj.base = base;
      }
    }
  }
}

class ConActionEnabled {
  bool show = true;
  late DataSimpleObject config;

  ConActionEnabled(this.config);

  bool check(FeatureContainer our, FeatureContainer other) {
    return true;
  }
}

class ConActionDisabled extends ConActionEnabled {
  ConActionDisabled(cfg) : super(cfg) {
    show = false;
  }

  @override
  bool check(FeatureContainer our, FeatureContainer other) {
    return false;
  }
}

class ConActionConstrains extends ConActionEnabled {
  ConActionConstrains(super.cfg);

  @override
  bool check(FeatureContainer our, FeatureContainer other) {
    if (config.args[0] != "same") return true;
    String field = config.args[1];
    String sour = our.extraSimple?.getFirstObjectName(field) ?? "NOSRC";
    String sother = other.extraSimple?.getFirstObjectName(field) ?? "NODST";
    if (sour != sother) return false;
    return true;
  }
}

class ContainerConfiguration {
  late ConActionEnabled put;
  late ConActionEnabled get;
  late ConActionEnabled move;
  late String creationName;
  List<DataSimpleObject> extra = [];

  /* ContainerConfiguration.defConf() {
    var en = DataSimpleObject("enabled");
    put = ConActionEnabled(en);
    get = ConActionEnabled(en);
    move = ConActionEnabled(en);
    creationName = "";
  }*/

  ConActionEnabled getAction(String str) {
    var obj = DataSimpleObject(str);
    if (obj.name == "enabled") return ConActionEnabled(obj);
    if (obj.name == "disabled") return ConActionDisabled(obj);
    if (obj.name == "constrains") return ConActionConstrains(obj);
    throw FormatException("Unknown action ${obj.name}");
  }

  ContainerConfiguration.fromJson(Map<String, dynamic> json) {
    put = getAction(json["put"] ?? "enabled");
    get = getAction(json["get"] ?? "enabled");
    move = getAction(json["move"] ?? "enabled");
    creationName = loadOrDefault(json, "creationName", "");
    List<dynamic> e = json["extra"] ?? [];
    extra = e.map((i) => DataSimpleObject(i)).toList();
  }
}

class DiceConfig {
  late String probabilityFeature;
  late List<String> images;

  DiceConfig.fromJson(Map<String, dynamic> json) {
    probabilityFeature = loadOrDefault(json, "ProbabilityFeature", "");
    List<dynamic> dimages = json["images"] ?? [];
    images = dimages.map((e) => e as String).toList();
  }
}

class Configuration {
  bool isMaster = false;
  int serial = 0;
  int refreshRate = 300;
  late String prefsPrefix;
  bool selfRegister = false;
  bool useWebSockets = true;
  bool createFiles = false;
  bool globalContactList = true;
  Map<String, QRTypeBase> qrActions = {};
  late Map<String, inventory.InventoryElementBase> inventoryElements;
  late Map<String, InvElementCreateFunc> invCreate;
  late Map<String, QrTypeCreate> qrCreate;
  late List<ServerConfig> servers;
  ServerConfig? _activeServer;
  late List<MenuElement> menuElements;
  late BLEConfig bleConfig;
  List<CharPropertyBase> charProperty = [];
  late Map<String, CharPropCreate> charPropertyCreate;
  Map<String, Set<String>> inventoryGroups = {};
  late SkillConfiguration skills;
  String blePeripheralsUUID = "";
  Map<String, BLEPeripheralsServiceConf> blePeripheralsService = {};
  String blePeripherialMACFetature = "";
  List<String> autoTransitions = [];
  List<OrderGroup> ordergorups = [];
  Map<String, Asset> assets = {};
  MapConfig? mapConfig;
  late ContainerConfiguration containerConf;
  late DiceConfig dice;

  void prepareInventoryRelations() {
    invCreate = {};
    invCreate["Default"] = () => inventory.InventoryElementDefault();
    invCreate["Contact"] = () => inventory.InventoryElementContact();
    invCreate["Report"] = () => inventory.InventoryElementReport();
    invCreate["Program"] = () => inventory.InventoryElementProgram();
    invCreate["File"] = () => inventory.InventoryElementFile();

    qrCreate = {};
    qrCreate["Player"] = (arg) => QRPlayer(arg);
    qrCreate["CustomTransition"] = (arg) => QRCustom(arg);
    qrCreate["Restaurant"] = (arg) => QRRestaurant(arg);
    qrCreate["MenuItem"] = (arg) => QRMenuItem(arg);

    charPropertyCreate = {};
    charPropertyCreate["integer"] = (feature, json) => CharPropertyInteger.fromJson(feature, json);
    charPropertyCreate["selector"] = (feature, json) => CharPropertySelector.fromJson(feature, json);
    charPropertyCreate["menu"] = (feature, json) => CharPropertyMenu.fromJson(feature, json);
    charPropertyCreate["exist"] = (feature, json) => CharPropertyExist.fromJson(feature, json);
    charPropertyCreate["timer"] = (feature, json) => CharPropertyTimer.fromJson(feature, json);
    charPropertyCreate["player"] = (feature, json) => CharPropertyPlayer.fromJson(feature, json);
    charPropertyCreate["character"] = (feature, json) => CharPropertyCharname.fromJson(feature, json);
    charPropertyCreate["qrcode"] = (feature, json) => CharPropertyQRCode.fromJson(feature, json);
  }

  Configuration.testData() {
    prepareInventoryRelations();
    prefsPrefix = "con";
    qrActions = {};
    inventoryElements = {};
    inventoryElements["Default"] = invCreate["Default"]!();
    inventoryElements["Contact"] = invCreate["Contact"]!();
  }

  Configuration.fromJson(Map<String, dynamic> json) {
    prepareInventoryRelations();
    serial = loadAndCheck<int>(json, "serial");
    prefsPrefix = loadAndCheck<String>(json, 'prefix');
    refreshRate = loadOrDefault(json, "refreshRate", 300);
    selfRegister = loadOrDefault(json, "selfRegister", false);
    useWebSockets = loadOrDefault(json, "UseWebSockets", false);
    Map<String, dynamic>? q = json['qrAction'];
    if (q != null) {
      for (var i in q.entries) {
        String qrtype = i.value["actionType"] ?? "CustomTransition";
        if (!qrCreate.containsKey(qrtype)) {
          throw FormatException("Unknown QR code type $qrtype");
        }
        qrActions[i.key] = qrCreate[qrtype]!(i.value);
      }
    }

    inventoryElements = {};
    Map<String, dynamic>? inv = json['inventoryElements'];
    if (inv != null) {
      for (var i in inv.entries) {
        inventoryElements[i.key] = invCreate[i.value]!();
      }
    }

    menuElements = [];
    List<dynamic>? l = json['menu'];
    if (l != null) {
      for (Map<String, dynamic> e in l) {
        menuElements.add(MenuElement.fromJson(e));
      }
    }

    servers = [];
    List<dynamic>? s = json['servers'];
    if (s != null) {
      for (String srv in s) {
        servers.add(ServerConfig.fromJson(srv));
      }
    }
    Map<String, dynamic>? ble = json['blescanner'];
    if (ble != null) {
      bleConfig = BLEConfig.fromJson(ble);
    } else {
      bleConfig = BLEConfig();
    }
    Map<String, dynamic>? cp = json['char-properties'];
    if (cp != null) {
      charProperty = [];
      for (var p in cp.entries) {
        var pname = p.key;
        String type = loadAndCheck(p.value, "type");
        var propCreate = charPropertyCreate[type];
        if (propCreate != null) {
          charProperty.add(propCreate(pname, p.value));
        } else {
          throw Exception("Unknown property type $pname}");
        }
      }
      charProperty.sort();
    }

    Map<String, dynamic>? grp = json["inventoryGroups"];
    if (grp != null) {
      for (var kv in grp.entries) {
        List<dynamic> items = kv.value;
        inventoryGroups[kv.key] = Set<String>.from(items);
      }
    }
    Map<String, dynamic>? dskills = json["skills"];
    if (dskills != null) {
      skills = SkillConfiguration.fromJson(dskills);
    } else {
      skills = SkillConfiguration();
    }

    Map<String, dynamic>? blep = json["BLEPeripherals"];
    if (blep != null) {
      for (var bleserv in blep.entries) {
        blePeripheralsUUID = bleserv.key;
        if (blePeripheralsUUID == "MACFeature") {
          blePeripherialMACFetature = bleserv.value;
          continue;
        }
        Map<String, dynamic> blechars = bleserv.value;
        for (var blechar in blechars.entries) {
          String uuid = blechar.key;
          blePeripheralsService[uuid] = BLEPeripheralsServiceConf.fromJson(uuid, blechar.value);
        }
      }
    }
    List<dynamic>? auto = json["autotransitions"];
    if (auto != null) {
      for (String t in auto) {
        autoTransitions.add(t);
      }
    }

    Map<String, dynamic>? orders = json["orders"];
    if (orders != null) {
      orders.forEach((key, value) {
        var ljson = value as Map<String, dynamic>;
        ordergorups.add(OrderGroup.fromJson(key, ljson));
      });
    }
    Map<String, dynamic> jassets = json["assets"] ?? {};
    jassets.forEach((key, value) {
      Map<String, dynamic> ajson = value;
      assets[key] = Asset.fromJson(key, ajson);
    });
    Map<String, dynamic>? jmap = json["map"];
    if (jmap != null) {
      mapConfig = MapConfig.fromJson(jmap);
    }
    Map<String, dynamic> jcontconf = json["container"] ?? {};
    containerConf = ContainerConfiguration.fromJson(jcontconf);

    Map<String, dynamic> jdice = json["dice"] ?? {};
    dice = DiceConfig.fromJson(jdice);
  }

  inventory.InventoryElementBase getInventoryElement(String? type) {
    inventory.InventoryElementBase? ret = inventoryElements[type!];
    ret ??= inventoryElements["Default"];
    return ret!;
  }

  ServerConfig selectActiveServer() {
    _activeServer = servers[0];
    _activeServer!.isActive = true;
    return _activeServer!;
  }

  ServerConfig get activeServer {
    if (_activeServer == null) selectActiveServer();
    return _activeServer!;
  }

  String? getMenuElementName(String path) {
    for (var e in menuElements) {
      if (e.menu == path) return e.label;
    }
    return null;
  }
}

class QRPlayer extends QRTypeBase {
  late String createTransiton;
  StreamController<List<QRAction>>? stream;
  StreamSubscription? stateSubscription;

  QRPlayer(Map<String, dynamic> jsonArguments) : super(jsonArguments) {
    createTransiton = loadAndCheck(jsonArguments, "newContactTransition");
  }

  void dispose() {
    stream!.close();
  }

  void onStateUpdate(StateStore state, charid) {
    FeatureDesc? charFD = lastState.char!.findFeatureByIntValue("Contact", charid);
    List<QRAction> ret = [];
    if (charFD != null) {
      ret.add(QRAction("${TradeMenu.getCharName(state, charid)} уже у вас в контактах. Нажмите для перехода",
          (playerid, ctx) {
        Navigator.of(ctx).pushNamed("/inventory/details", arguments: charFD);
      }));
    } else {
      ret.add(QRAction("Добавить в контакты ${TradeMenu.getCharName(state, charid)} ", (id, ctx) {
        state.char!.newFeature(state.featureTypesByName["Contact"]!.pk, charid.toString());
      }));
    }
    TransitionDesc? heal = state.transitions['Get-Heal'];
    if (heal != null && state.char!.findFeatureByStrValue("Навык", "Медицина1") != null) {
      if (!heal.isLoading()) {
        if (heal.passed) {
          ret.add(QRAction("Лечить", (id, ctx) async {
            FeatureDesc? token = state.char!.featuresByTypeName["HealToken"]?.first;
            if (token == null) {
              await heal.apply();
              token = state.char!.featuresByTypeName["HealToken"]?.first;
              if (token == null) {
                throw Exception("Не удалось получить HealToken");
              }
            }
            DealDesc deal = await state.newDeal(charid);
            deal.addOperation(resource: token.resource);
            deal.accept();
          }));
        } else {
          ret.add(QRAction("Лечить [${heal.getFailedCmp()}]", null));
        }
      }
    }

    stream!.add(ret);
  }

  Stream<List<QRAction>> getActions(int id) {
    log("Action for player $id");
    PlayerDesc(id).load().then((player) {
      if (stateSubscription != null) stateSubscription!.cancel();
      stateSubscription = stateStreamController.stream.listen((state) => onStateUpdate(state, player.character));
      onStateUpdate(lastState, player.character);
    });
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "игрок";
  }

  @override
  List<Widget> masterInformation(QRScannerResult res) {
    List<Widget> ret = [];
    PlayerDesc? player = lastState.players[res.number];
    if (player == null) {
      ret.add(ListTile(title: Text("Игрок ${res.number} не найден")));
      return ret;
    }
    player.isLoading();
    ret.add(ListTile(
        title: Text("Игрок ${res.number} ${player.realName} ${player.nickname}"),
        trailing: ElevatedButton(
          onPressed: () =>
              Navigator.of(navigatorKey.currentContext!).pushNamed("/master/player", arguments: res.number!),
          child: const Text("Перейти"),
        )));
    int charid = player.character;
    CharacterDesc? char = lastState.characters[charid];
    if (char == null) {
      ret.add(ListTile(
        title: Text("Персонаж $charid не найден"),
      ));
    } else {
      char.isLoading();
      ret.add(ListTile(
        title: Text("Перосонаж $charid ${char.name}"),
        trailing: ElevatedButton(
            onPressed: () =>
                Navigator.of(navigatorKey.currentContext!).pushNamed("/master/character", arguments: charid),
            child: const Text("Перейти")),
      ));
    }
    return ret;
  }
}

class QRCustom extends QRTypeBase {
  static const String reportFeatureName = "Report";
  StreamController<List<QRAction>>? stream;
  late String prefix;
  String? hackSuffix;
  String? descriptionSuffix;
  StreamSubscription? sub;

  QRCustom(Map<String, dynamic> jsonArguments) : super(jsonArguments) {
    prefix = loadAndCheck(jsonArguments, "prefix");
    hackSuffix = loadOrDefault(jsonArguments, "hackSuffix", null);
    descriptionSuffix = loadOrDefault(jsonArguments, "descriptionSuffix", null);
  }

  Widget successDialog(BuildContext context, TransitionDesc t) {
    return AlertDialog(
      title: const Text("Выполнено"),
      content: Text(t.description),
      actions: [
        TextButton(
            child: const Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            }),
        TextButton(
            onPressed: () {
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
            child: const Text("главное меню"))
      ],
    );
  }

  int getReportId(FeatureDesc ft) {
    var repid = ft.valueJson["ReportId"];
    if (repid is int) return repid;
    return int.parse(repid);
  }

  int getReportLevel(FeatureDesc ft) {
    var repid = ft.valueJson["ReportLevel"];
    if (repid is int) return repid;
    return int.parse(repid);
  }

  FeatureDesc? getMaxReportId(int id) {
    int maxReportLevel = -1;
    FeatureDesc? report;
    var reports = lastState.char!.featuresByTypeName[reportFeatureName];
    if (reports != null) {
      for (var ft in reports) {
        //if (ft.valueJson["Type"] != transitionPrefix) continue;

        if (getReportId(ft) != id) continue;

        if (ft.valueJson["ReportLevel"] > maxReportLevel) {
          maxReportLevel = ft.valueJson["ReportLevel"];
          report = ft;
        }
      }
    }
    return report;
  }

  QRAction defaultAction(TransitionDesc desc) => QRAction(desc.title, (id, ctx) async {
        await desc.apply();
        if (desc.description.isNotEmpty) {
          showDialog(context: ctx, builder: (BuildContext context) => successDialog(context, desc));
        }
      });

  QRAction scanAction(TransitionDesc desc) => QRAction(desc.title, (id, ctx) async {
        FeatureDesc? report = getMaxReportId(id);
        if (report == null || getReportLevel(report) < desc.level!) {
          await desc.apply();
          report = getMaxReportId(id);
        }
        if (report != null) {
          Navigator.of(ctx).pushNamed("/inventory/details", arguments: report);
        } else {
          lastState.addError(Error.system("No report for ${desc.key}"));
        }
      });

  QRAction getActionByTree(TreeElement<String, TransitionDesc> tree) {
    TransitionDesc? desc = tree.getMaximumCond((c) => c!.passed);
    if (desc == null) {
      desc = tree.getMinimum();
      if (desc == null) {
        return QRAction.description("Действий не найдено");
      }
    }
    String suffix = desc.keySplit[2];
    if (!desc.passed) {
      if (suffix == descriptionSuffix) {
        return QRAction.description("Описание недоступно");
      } else {
        return QRAction("${desc.title} [${desc.getFailedCmp()}]", null);
      }
    }

    if (suffix == descriptionSuffix) {
      return QRAction.description(desc.description);
    } else if (suffix == hackSuffix) {
      return QRAction(desc.title, (id, ctx) {
        HackingRequest rq = HackingRequest(() {
          //TODO: переделать по новому
          desc!.apply();
          Navigator.of(ctx).pop();
        });
        Navigator.of(ctx).pushNamed("/hacking", arguments: rq);
      });
    } else if (suffix == "Scan") {
      return scanAction(desc);
    } else {
      return defaultAction(desc);
    }
  }

  List<QRAction> transformState(StateStore state, int id) {
    List<QRAction> ret = [];
    var trans = lastState.transTree.getElement([prefix, id.toString()]);
    if (trans == null) {
      return [QRAction("Нет действий с данным предметом", null)];
    }

    for (TreeElement<String, TransitionDesc> e in trans.trunk!.values) {
      List<TransitionDesc> loads = [];
      for (var l in e.getList()) {
        if (l.isLoading()) loads.add(l);
      }
      if (loads.isEmpty) {
        ret.add(getActionByTree(e));
      } else {
        ret.add(QRAction("Загрузка...", null));
      }
    }
    ret.sort((a, b) => a.name.compareTo(b.name));
    return ret;
  }

  void updateStream(StateStore state, int id) => stream!.add(transformState(state, id));

  void onListen(int id) {
    if (kDebugMode) {
      log("new Listener");
    }
    if (sub != null) {
      sub!.cancel();
      sub = null;
    }
    sub = stateStreamController.stream.listen((state) => updateStream(state, id));
    updateStream(lastState, id);
  }

  void onCancel() {
    if (kDebugMode) {
      log("cancel");
    }
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController(onListen: () => onListen(res.number!), onCancel: onCancel);

    res.actions = stream!.stream;
    //stateStreamController.stream
    //    .map((event) => transformState(event!, res.number!));
    lastState.updated = true;
    res.name = "";
    if (descriptionSuffix != null) {
      TransitionDesc? trans =
          lastState.transTree.getElement([prefix, res.number.toString(), descriptionSuffix!])?.getMaximum();
      res.name = trans?.title ?? "<неизвестно>";
    }
  }

  @override
  List<Widget> masterInformation(QRScannerResult res) {
    List<Widget> ret = [];
    ret.add(const ListTile(
      title: Text("QRCustom"),
    ));
    var trans = lastState.transTree.getElement([prefix, res.number.toString()]);
    if (trans == null) {
      ret.add(const ListTile(title: Text("Нет действий с данным предметом")));
      return ret;
    }

    for (TransitionDesc t in trans.getList()) {
      ret.add(ListTile(
        title: Text(t.key),
        subtitle: Text(t.description),
        trailing: ElevatedButton(
          onPressed: () => Navigator.of(navigatorKey.currentContext!).pushNamed("/master/transition", arguments: t.key),
          child: const Text("Перейти"),
        ),
      ));
    }
    return ret;
  }
}

class TableDesc {
  late int table;
  late String restaurantName;
  late int charID;
  late TransitionDesc trans;
  late String origJson;

  TableDesc.fromTransition(TransitionDesc _t) {
    trans = _t;
    origJson = trans.description;
    Map<String, dynamic> js = json.decode(origJson);
    table = loadAndCheck(js, "table");
    restaurantName = loadAndCheck(js, "res");
    charID = loadAndCheck(js, "charid");
  }

  TableDesc.fromJSON(Map<String, dynamic> js) {
    table = loadAndCheck(js, "table");
    restaurantName = loadAndCheck(js, "res");
    charID = loadAndCheck(js, "charid");
  }
}

class MenuItemDesc {
  late TransitionDesc trans;
  late String name;
  late String restaurantName;
  late int cost;
  FeatureDesc? feat;
  DealOperationDesc? dealOp;

  MenuItemDesc.fromTransition(TransitionDesc t) {
    trans = t;
    name = trans.title;
    Map<String, dynamic> js = json.decode(trans.description);
    restaurantName = loadAndCheck(js, "res");
    cost = loadAndCheck(js, "cost");
  }

  MenuItemDesc.fromJSON(Map<String, dynamic> js) {
    restaurantName = loadAndCheck(js, "res");
    cost = loadAndCheck(js, "cost");
    name = loadAndCheck(js, "name");
  }

  String toJSON() {
    Map<String, dynamic> ret = {};
    ret["res"] = restaurantName;
    ret["cost"] = cost;
    ret["name"] = name;
    return json.encode(ret);
  }
}

class QRRestaurant extends QRTypeBase {
  StreamController<List<QRAction>>? stream;

  void dismis() {
    stream?.close();
    stream = null;
  }

  QRRestaurant(Map<String, dynamic> jsonArguments) : super(jsonArguments);

  Future<void> makeOrder(int id, BuildContext ctx, TableDesc td) async {
    DealDesc order = await lastState.newDeal(td.charID);
    order.extra = td.origJson;
    await order.updateExtra();
    Navigator.of(ctx).pushNamed("/trade/rest", arguments: order.pk);
  }

  Stream<List<QRAction>> getActions(int id) {
    List<QRAction> ret = [];

    var trans = lastState.transTree.getElement(["Order", "Create", id.toString()])?.leaf;
    if (trans != null) {
      try {
        TableDesc table = TableDesc.fromTransition(trans);
        ret.add(QRAction.description("Столик номер ${table.table} в ${table.restaurantName}"));
        DealDesc? deal = findDeal(table.restaurantName);
        if (deal == null) {
          ret.add(QRAction("Сделать заказ", (id, ctx) => makeOrder(id, ctx, table)));
        } else {
          ret.add(QRAction(
              "Перейти к заказу", (id, ctx) => Navigator.of(ctx).pushNamed("/trade/rest", arguments: deal.pk)));
        }
      } on FormatException catch (e) {
        ret.add(QRAction.description("Ошибка декодирования ${e.message}"));
      }
    } else {
      ret.add(QRAction.description("Не найдено описание для стоилка $id"));
    }
    stream!.add(ret);
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "Столик";
  }
}

DealDesc? findDeal(String restaurantName) {
  for (var deal in lastState.deals.values) {
    //Завершенные нас не итересуют
    if (deal.status != DealStatus.created) continue;
    if (deal.extraJson["res"] == restaurantName) return deal;
  }
  return null;
}

class QRMenuItem extends QRTypeBase {
  StreamController<List<QRAction>>? stream;

  QRMenuItem(Map<String, dynamic> jsonArguments) : super(jsonArguments);

  Future<void> addToOrder(int id, BuildContext ctx, MenuItemDesc item) async {
    int res = await lastState.char!
        .newFeature(lastState.featureTypesByName["MenuItemToken"]!.pk, "extra", extra: item.toJSON());
    await lastState.char!.waitForResource(res);
    DealDesc? deal = findDeal(item.restaurantName);
    if (deal == null) {
      lastState
          .addError(Error.system("Не найден открытый счет. Возможно нужно снчала открыть счет через QR код стола"));
      Navigator.of(ctx).pop();
      return;
    }
    await deal.addOperation(resource: res);
    Navigator.of(ctx).pushReplacementNamed("/trade/rest", arguments: deal.pk);
  }

  Stream<List<QRAction>> getActions(int id) {
    List<QRAction> ret = [];
    var trans = lastState.transTree.getElement(["Order", "Item", id.toString()])?.leaf;
    if (trans != null) {
      try {
        MenuItemDesc item = MenuItemDesc.fromTransition(trans);
        ret.add(QRAction.description("${item.name} ${item.cost} cr"));
        ret.add(QRAction.description("в ресторане ${item.restaurantName}"));
        ret.add(QRAction("Добавить в заказ", (id, ctx) => addToOrder(id, ctx, item)));
      } on FormatException catch (e) {
        ret.add(QRAction.description("Ощибка декодирования ${e.message}"));
      }
    } else {
      ret.add(QRAction.description("Не найдено описание для пункта меню $id"));
    }
    stream!.add(ret);
    return stream!.stream;
  }

  @override
  void updateQResult(QRScannerResult res) {
    if (stream != null) stream!.close();
    stream = StreamController();
    res.actions = getActions(res.number!);
    res.name = "пункт меню";
  }
}
