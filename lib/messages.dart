import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'global.dart';
import 'model.dart';

class ChatsMenu extends StatelessWidget {
  const ChatsMenu({super.key});

  void pressedPlayer(BuildContext context) async {
    var other = await Navigator.of(context).pushNamed("/trade/new");
    int? otherId = other as int?;
    if (otherId == null) return;

    int chat_id = await lastState.newChat(otherId.toString(), [otherId], []);
    Navigator.of(navigatorKey.currentContext!).pushNamed("/chats/chat", arguments: chat_id);
  }

  void pressedMaster(BuildContext context) async {
    var res = await Navigator.of(context).pushNamed("/chats/masterNew");
    if (res == null) return;
    MasterChatMenuResult result = res as MasterChatMenuResult;
    int chatId = await lastState.newChat(
        result.chatName, result.getCharacters(), result.getMasters());
    Navigator.of(context).pushNamed("/chats/chat", arguments: chatId);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Чаты"),
        ),
        drawer: getCommonDrawer(context),
        body: StreamBuilder<StateStore>(
            stream: stateStreamController.stream,
            initialData: lastState,
            builder: (BuildContext ctx, AsyncSnapshot<StateStore?> shot) {
              List<Widget> widgets = [];

              for (var ent in shot.data!.chats.values) {
                widgets.add(ListTile(
                  title: Text(ent.getLabel()),
                  onTap: () => Navigator.of(context)
                      .pushNamed("/chats/chat", arguments: ent.pk),
                ));
              }
              return ListView(
                children: widgets,
              );
            }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (configuration.isMaster) {
              pressedMaster(context);
            } else {
              pressedPlayer(context);
            }
          },
          child: const Icon(Icons.add),
        ));
  }
}

class MasterChatMenuResult {
  late String chatName;
  late String masterName;
  late Set<int> characters;

  MasterChatMenuResult(this.chatName, this.masterName, this.characters);

  List<String> getMasters() {
    return [masterName];
  }

  List<int> getCharacters() {
    return characters.toList();
  }
}

class MasterChatNew extends StatefulWidget {
  const MasterChatNew({super.key});

  @override
  State<StatefulWidget> createState() {
    return MasterChatNewState();
  }
}

class MasterChatNewState extends State<MasterChatNew> {
  TextEditingController nameController = TextEditingController();
  TextEditingController chatController = TextEditingController();
  Set<int> checked = {};

  Widget streamedBuild(BuildContext context, AsyncSnapshot<StateStore> state) {
    if (state.data == null) {
      return const Text("No data available");
    }
    List<Widget> items = [];
    items.add(const ListTile(
      title: Text("Имя мастерского персонажа"),
      subtitle: Text("Оставьте пустым если не добавляем"),
    ));
    items.add(TextField(
      controller: nameController,
    ));
    items.add(const ListTile(
      title: Text("Название чата"),
      subtitle: Text("Оставьте пустым не важно"),
    ));
    items.add(TextField(
      controller: chatController,
    ));
    for (var char in state.data!.characters.values) {
      Widget box = const Icon(Icons.check_box_outline_blank);
      if (checked.contains(char.id)) {
        box = const Icon(Icons.check_box);
      }
      items.add(ListTile(
        title: Text(char.name),
        trailing: box,
        onTap: () {
          setState(() {
            if (!checked.contains(char.id)) {
              checked.add(char.id);
            } else {
              checked.remove(char.id);
            }
          });
        },
      ));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Создать чат"),
        actions: [loadIndicatorBuilder()],
      ),
      body: ListView(
        children: items,
      ),
      bottomNavigationBar: ElevatedButton(
        onPressed: () {
          Navigator.of(context).pop(MasterChatMenuResult(
              chatController.text, nameController.text, checked));
        },
        child: const Text("Создать"),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateStore>(
      builder: streamedBuild,
      stream: stateStreamController.stream,
      initialData: lastState,
    );
  }
}

class ChatMenu extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _inputController = TextEditingController();
  final DateFormat format = DateFormat.Hms();

  ChatMenu({super.key});

  Widget? item(Chat chat, BuildContext context, int index) {
    if (index == 0) {
      return Form(
          key: _formKey,
          child: Container(
              decoration:
                  BoxDecoration(border: Border.all(), color: Colors.blueGrey),
              child: Row(
                children: [
                  Expanded(child: TextFormField(controller: _inputController)),
                  IconButton(
                      onPressed: () {
                        chat.send(_inputController.text);
                        _inputController.text = "";
                      },
                      icon: const Icon(Icons.send))
                ],
              )));
    }

    Message? msg = chat.getMessage(index - 1);
    if (msg == null) {
      return null;
    }
    if (msg.isLoading) {
      return const ListTile(
        leading: CircularProgressIndicator(),
      );
      //return null;
    }
    ChatMember sender = chat.members[msg.sender] ?? ChatMember();
    late String subtitle;
    Color color = Colors.white;

    String time = format.format(msg.time.toLocal());
    if (!configuration.isMaster &&
        (sender.char?.id ?? -1) == lastState.char!.id) {
      color = Colors.green;
      subtitle = "я $time";
    } else {
      subtitle = "${sender.label} $time";
    }
    return ListTile(
      title: Text(subtitle),
      subtitle: Text(msg.text),
      textColor: color,
    );
  }

  @override
  Widget build(BuildContext context) {
    int pk = ModalRoute.of(context)!.settings.arguments as int? ?? -1;
    Chat chat = lastState.chats[pk]!;
    return StreamBuilder(
      stream: chat.streamController.stream,
      builder: (context, snapshot) {
        return Scaffold(
            appBar: AppBar(
              title: Text("Диалог с ${chat.getLabel()}"),
            ),
            body: CustomScrollView(
              slivers: [
                SliverList(
                    delegate: SliverChildBuilderDelegate(
                        (ctx, idx) => item(chat, ctx, idx)))
              ],
              reverse: true,
            ));
      },
    );
  }
}
