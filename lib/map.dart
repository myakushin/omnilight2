import 'dart:async';
import 'dart:developer';
import 'dart:ui' as ui;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:omnilight2/config.dart';

import 'global.dart';
import 'model.dart';

enum MapModeType { observe, pick }

class MapMode {
  MapModeType type = MapModeType.observe;
  List<MapObject> allowOnlyType = [];
  List<String> allowOnly = [];
  List<String> gray = [];
  List<String> exclude = [];
  String focusedOn = "";

  MapMode();

  MapMode.pick() {
    type = MapModeType.pick;
  }

  void setTypeFilterByConfig() {
    MapConfig conf = configuration.mapConfig!;
    allowOnlyType.clear();
    for (var name in conf.locationFeatures) {
      MapObject? obj = conf.objects[name];
      if (obj == null) {
        throw Exception("Location object $name not found in map objects");
      }
      allowOnlyType.add(obj);
    }
  }
}

typedef ImageAction = void Function();

class ImageObj {
  late ui.Image img;
  late Offset _offset;
  late GeoValues geo;
  Color color = Colors.white;
  Paint paint = Paint();
  String label = "";
  ImageAction? onClick;
  ImageObj? parent;

  ImageObj(this.img, this.geo) {
    _offset = Offset(geo.x.toDouble(), geo.y.toDouble());
  }

  Offset getOffset(double scale) => parent?.getChildOffset(scale) ?? _offset.scale(scale, scale);

  Offset getChildOffset(double scale) =>
      Offset(getOffset(scale).dx + img.width + 10, getOffset(scale).dy - (img.height / 2).floor());

  void setColor(Color c) {
    color = c;
    paint.colorFilter = ColorFilter.mode(color, BlendMode.modulate);
  }

  void setGray() {
    color = const Color.fromARGB(64, 255, 255, 255);
    paint.colorFilter = ColorFilter.mode(color, BlendMode.modulate);
  }

  bool checkHit(Offset position, double scale) {
    Offset imgoffset = Offset(-img.width / 2, -img.height / 2);
    Offset o = getOffset(scale) + imgoffset;
    log("Check hit pos=$position o=$o");
    if (o.dx > position.dx) return false;
    if (o.dx + img.width < position.dx) return false;
    if (o.dy > position.dy) return false;
    if (o.dy + img.height < position.dy) return false;
    return true;
  }

  void draw(Canvas canvas, double scale) {
    Offset imgoffset = Offset(-img.width / 2, -img.height / 2);
    canvas.drawImage(img, getOffset(scale) + imgoffset, paint);
    if (label.isNotEmpty) {
      final style = ui.TextStyle(fontFamily: "Furore", color: color, fontSize: 15);
      final pStyle = ui.ParagraphStyle();
      final bp = ui.ParagraphBuilder(pStyle);
      bp.pushStyle(style);
      bp.addText(label);
      const constraints = ui.ParagraphConstraints(
        width: 300,
      );
      final para = bp.build();
      para.layout(constraints);
      Offset toffset = getOffset(scale) + imgoffset + Offset(-para.maxIntrinsicWidth / 8, img.height.toDouble());
      canvas.drawParagraph(para, toffset);
    }
  }
}

class LinkObj {
  late Offset x1, x2;
  Paint paint = Paint();

  LinkObj(GeoValues from, GeoValues to) {
    x1 = Offset(from.x.toDouble(), from.y.toDouble());
    x2 = Offset(to.x.toDouble(), to.y.toDouble());
    paint.color = Colors.grey;
    paint.strokeWidth = 4;
  }

  void draw(Canvas canvas, scale) {
    canvas.drawLine(x1.scale(scale, scale), x2.scale(scale, scale), paint);
  }
}

class MapBGPainter extends CustomPainter {
  late MapState state;

  MapBGPainter(this.state);

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawImage(state.backgroundImg, const Offset(0, 0), Paint());
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}

class MapPainter extends CustomPainter {
  late MapState state;

  MapPainter(this.state);

  @override
  void paint(Canvas canvas, Size size) {
    try {
      var mapPosition = state.mapOffset;
      canvas.translate(mapPosition.dx, mapPosition.dy);
      //    print("Canvas translated $mapPosition");
      for (var img in state.links) {
        img.draw(canvas, state.scale);
      }
      for (var img in state.images) {
        img.draw(canvas, state.scale);
      }
    } on Exception catch (e) {
      addError(Error.system("Map render error ${e.toString()}"));
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    //print("Check repaint");
    return true;
  }
}

class MapMenu extends StatefulWidget {
  const MapMenu({super.key});

  @override
  State<StatefulWidget> createState() => MapState();
}

enum MapGestureMode { none, click, move, zoom }

Map<MapObjectActionType, String> mapActionMenu = {
  MapObjectActionType.orderQueue: "/map/orderMenu",
  MapObjectActionType.order: "/map/instantOrder",
  MapObjectActionType.details: "/map/details"
};
const double maxScale = 32;
const double minScale = 0.3;

class MapState extends State<MapMenu> {
  Offset dragStartPosition = const Offset(0, 0);
  Offset mapOffset = const Offset(0, 0);
  MapGestureMode gesture = MapGestureMode.none;
  Offset zoom1 = const Offset(0, 0), zoom2 = const Offset(0, 0);
  Offset zoomCenter = const Offset(0, 0);
  double scale = 1;
  double startScale = 1;
  double startDist = 0;
  Offset startOffset = const Offset(0, 0);
  StreamSubscription<StateStore>? stateSubscription;
  late TextEditingController searchEdit;
  late MapMode mapMode;
  bool isSearching = false;
  late ui.Image backgroundImg;
  List<ImageObj> images = [];
  List<LinkObj> links = [];
  Map<String, ImageObj> geoLabels = {};
  List<ImageObj> touchabels = [];
  bool calculated = false;
  List<ImageObj> found = [];

  void recalculate() {
    images.clear();
    links.clear();
    geoLabels.clear();
    touchabels.clear();
    try {
      var conf = configuration.mapConfig!;
      backgroundImg = configuration.assets[conf.backgroundName]!.image!;

      List<MapObject> objects = conf.objects.values.toList();
      objects.sort((a, b) => a.prio.compareTo(b.prio));
      for (MapObject obj in objects) {
        if (mapMode.allowOnlyType.isNotEmpty && !mapMode.allowOnlyType.contains(obj)) {
          continue;
        }
        if (!lastState.featureTypesByName.containsKey(obj.name)) continue;
        FeatureTypeDesc ftype = lastState.featureTypesByName[obj.name]!;
        if (ftype.static) {
          prepareStatic(obj, ftype);
        } else {
          if (configuration.isMaster) {
            for (CharacterDesc char in lastState.characters.values) {
              char.isLoading();
              for (FeatureDesc feat in char.featuresByTypeName[obj.name] ?? []) {
                prepareObject(feat.valueSimple, feat.geo, obj);
              }
            }
          } else {
            for (FeatureDesc feat in lastState.char!.featuresByTypeName[obj.name] ?? []) {
              prepareObject(feat.valueSimple, feat.geo, obj);
            }
          }
        }
      }
    } on Exception catch (e) {
      addError(Error.system("Map prepare error:$e"));
    }
    calculated = true;
  }

  Color getColor(MapObjectAttribute attr) {
    List<String> colors = attr.data.split(",");
    if (colors.length != 3) {
      throw FormatException("Wrong color for ${attr.name} ${attr.data}");
    }
    int r = int.tryParse(colors[0]) ?? 0;
    int g = int.tryParse(colors[1]) ?? 0;
    int b = int.tryParse(colors[2]) ?? 0;
    return Color.fromARGB(255, r, g, b);
  }

  ui.Image getImageFromAsset(MapObjectAttribute attrib) {
    String name = attrib.data;
    return configuration.assets[name]!.image!;
  }

  void prepareObject(DataSimple data, GeoValues geo, MapObject obj) {
    bool grayed = false;
    if ((data.getFirstObjectName("visible") ?? "True") == "False") {
      return;
    }

    var baseImg = ImageObj(getImageFromAsset(obj.getAttr("base")), geo);
    if (data.containsKey("name")) {
      baseImg.label = data.getFirstObjectName("name")!;
    }
    if (data.containsKey("location")) {
      String loc = data.getFirstObjectName("location")!;
      baseImg.parent = geoLabels[loc];
    }
    if (mapMode.allowOnly.isNotEmpty && !mapMode.allowOnly.contains(baseImg.label)) {
      if (!mapMode.gray.contains(baseImg.label)) {
        return;
      } else {
        grayed = true;
      }
    }
    images.add(baseImg);

    if (data.containsKey("attrib")) {
      for (var attrObj in data.getList("attrib")!) {
        var attr = obj.getAttr(attrObj.name);

        if (attr.type == MapObjectAttributeType.image) {
          var i = ImageObj(getImageFromAsset(attr), geo);
          i.parent = baseImg.parent;
          images.add(i);
        }

        if (attr.type == MapObjectAttributeType.color) {
          var color = getColor(attr);
          baseImg.setColor(color);
        }
      }
      if (!grayed) {
        if (mapMode.type == MapModeType.observe) {
          if (obj.actions.isNotEmpty) {
            MapObjectAction act = obj.actions.first;
            touchabels.add(baseImg);
            baseImg.onClick = () {
              var ctx = navigatorKey.currentContext;
              String? menu = mapActionMenu[act.type];
              if (ctx == null) return;
              if (menu == null) {
                addError(Error.system("Unknown menu action type ${act.type.name}"));
                return;
              }
              Navigator.of(ctx).pushNamed(menu, arguments: MapOrderQueueRequest(baseImg.label, obj, data));
            };
          }
        }
        if (mapMode.type == MapModeType.pick) {
          touchabels.add(baseImg);
        }
      } else {
        baseImg.setGray();
      }
    }

    if (data.containsKey("name")) {
      String name = data.getFirstObjectName("name")!;
      geoLabels[name] = baseImg;
    }

    if (data.containsKey("link")) {
      for (var otherName in data.getList("link")!) {
        GeoValues? otherGeo = geoLabels[otherName.name]?.geo;
        if (otherGeo != null) {
          links.add(LinkObj(geo, otherGeo));
        }
      }
    }
  }

  void prepareStatic(MapObject obj, FeatureTypeDesc ftype) {
    ftype.valuesSimple.forEach((resource, value) {
      GeoValues geo = ftype.geoform[resource]!;
      prepareObject(value, geo, obj);
    });
  }

  bool? click(ui.Offset position, double scale) {
    for (var t in touchabels) {
      if (t.checkHit(position, scale)) {
        if (t.onClick != null) {
          t.onClick!.call();
          return true;
        }
        if (mapMode.type == MapModeType.pick) {
          var ctx = navigatorKey.currentContext;
          if (ctx == null) return false;
          Navigator.of(ctx).pop(t.label);
          return true;
        }
      }
    }
    return false;
  }

  void onState(StateStore state) {
    setState(() {
      log("Reset state");
      recalculate();
    });
  }

  @override
  void dispose() {
    stateSubscription?.cancel();
    super.dispose();
  }

  void onPointerDown(PointerEvent event) {
    if (event.device == 0) zoom1 = event.localPosition;
    if (event.device == 1) zoom2 = event.localPosition;
    if (event.device == 0) {
      setState(() {
        gesture = MapGestureMode.click;
      });
    }
    if (event.device == 1) {
      setState(() {
        gesture = MapGestureMode.zoom;
        startDist = (zoom1 - zoom2).distance;
        startScale = scale;
        zoomCenter = zoom1;
        startOffset = (mapOffset - zoomCenter).scale(1 / scale, 1 / scale);
      });
    }
  }

  void onPointerMove(PointerEvent event) {
    Offset delta = event.localPosition - zoom1;
    if (gesture == MapGestureMode.click && event.device == 0 && delta.distance > 10) {
      setState(() {
        gesture = MapGestureMode.move;
      });
    }
    if (gesture == MapGestureMode.move) {
      setState(() {
        mapOffset = mapOffset + event.localDelta;
      });
    }
    if (gesture == MapGestureMode.zoom) {
      if (event.device == 0) zoom1 = event.localPosition;
      if (event.device == 1) zoom2 = event.localPosition;
      double d = (zoom1 - zoom2).distance;
      setState(() {
        scale = startScale * d / startDist;
        if (scale > maxScale) scale = maxScale;
        if (scale < minScale) scale = minScale;
        mapOffset = startOffset.scale(scale, scale) + zoomCenter;
      });
    }
  }

  void onPointerUo(PointerEvent event) {
    if (event.device == 0 && gesture == MapGestureMode.click) {
      click(event.localPosition - mapOffset, scale);
    }
  }

  void onPZStart(PointerPanZoomStartEvent event) {
    log("$event");
  }

  void onPZUpdate(PointerPanZoomUpdateEvent event) {
    log("$event");
  }

  void onPZEnd(PointerPanZoomEndEvent event) {
    log("$event");
  }

  MapPainter getPainter() => MapPainter(this);

  MapBGPainter getBGPainter() => MapBGPainter(this);

  void focusOn(ImageObj obj) {
    mapOffset = const Offset(200, 200) - obj.getOffset(scale);
  }

  @override
  Widget build(BuildContext context) {
    if (!calculated) {
      mapMode = ModalRoute.of(context)!.settings.arguments as MapMode? ?? MapMode();
      recalculate();
      if (mapMode.focusedOn.isNotEmpty) {
        ImageObj? label = geoLabels[mapMode.focusedOn];
        if (label != null) {
          focusOn(label);
        }
      }
    }
    Rect backRect = configuration.mapConfig!.backgroundRect;
    stateSubscription ??= stateStreamController.stream.listen(onState);

    Widget header = const Text("Unknown");

    if (isSearching) {
      List<String> k = geoLabels.keys.toList();
      k.sort();
      List<DropdownMenuEntry<ImageObj>> entries = [];
      for (var e in k) {
        ImageObj obj = geoLabels[e]!;
        entries.add(DropdownMenuEntry(
            value: obj,
            label: e,
            leadingIcon: Icon(
              Icons.circle,
              color: obj.color,
            )));
      }
      header = DropdownMenu<ImageObj>(
          enableFilter: true,
          enableSearch: true,
          dropdownMenuEntries: entries,
          controller: searchEdit,
          onSelected: (s) => setState(() => focusOn(s!)));
    } else {
      if (mapMode.type == MapModeType.observe) {
        if (kDebugMode) {
          header = Text(gesture.name);
        } else {
          header = const Text("Карта");
        }
      }
      if (mapMode.type == MapModeType.pick) {
        header = const Text("Выберете систему");
      }

      //     header = TextField(
      //       controller: searchEdit, onChanged: onSearchChanged, decoration: decor,);
    }
    return Scaffold(
        appBar: AppBar(
          title: header,
          actions: [
            IconButton(
                onPressed: () {
                  setState(() {
                    isSearching = !isSearching;
                    if (isSearching) {
                      searchEdit = TextEditingController();
                    }
                  });
                },
                icon: const Icon(Icons.search))
          ],
        ),
        drawer: getCommonDrawer(context),
        body: Listener(
            onPointerDown: onPointerDown,
            onPointerMove: onPointerMove,
            onPointerUp: onPointerUo,
            onPointerPanZoomStart: onPZStart,
            onPointerPanZoomUpdate: onPZUpdate,
            onPointerPanZoomEnd: onPZEnd,
            behavior: HitTestBehavior.translucent,
            child: CustomPaint(
                painter: getBGPainter(),
                foregroundPainter: getPainter(),
                size: Size(backRect.width, backRect.height))));
  }
}

class MapOrderQueueRequest {
  late String fleetName;
  late MapObject object;
  late DataSimple values;

  MapOrderQueueRequest(this.fleetName, this.object, this.values);
}

FeatureDesc? findOrderByFleetName(String fleetName) {
  String orderFeatureName = configuration.mapConfig!.orderFeatureName;
  FeatureTypeDesc? ftype = lastState.featureTypesByName[orderFeatureName];
  if (ftype == null) {
    return null;
  }
  return findOrder(ftype, fleetName);
}

FeatureDesc? findOrder(FeatureTypeDesc ftype, String fleetName) {
  if (configuration.isMaster) {
    for (CharacterDesc char in lastState.characters.values) {
      var ret = findOrderInList(fleetName, char.featuresByTypeID[ftype.pk] ?? []);
      if (ret != null) return ret;
    }
  } else {
    return findOrderInList(fleetName, lastState.char!.featuresByTypeID[ftype.pk] ?? []);
  }
  return null;
}

FeatureDesc? findOrderInList(String fleetName, List<FeatureDesc> featList) {
  for (FeatureDesc feature in featList) {
    if (feature.valueSimple.getFirstObjectName("fleet") == fleetName &&
        !["canceled", "done"].contains(feature.valueSimple.getFirstObjectName("status") ?? "")) {
      return feature;
    }
  }
  return null;
}

FeatureDesc? findFeatureByFleetName(MapObject obj, String fleetName) {
  for (FeatureDesc f in lastState.char!.featuresByTypeName[obj.name] ?? []) {
    if (f.valueSimple.getFirstObjectName("name") == fleetName) {
      return f;
    }
  }
  return null;
}

class ValidateOrder {
  FeatureDesc feat;
  FeatureDesc? fleet;

  ValidateOrder(this.feat) {
    loadSystems();
    for (FeatureDesc f in lastState.char!.featuresByTypeName["Караван"] ?? []) {
      if (f.valueSimple.getFirstObjectName("name") == feat.valueSimple.getFirstObjectName("fleet")) {
        fleet = f;
      }
    }
    if (fleet != null) {
      payload = fleet?.valueSimple.getList("payload")?.map((e) => e.name).toList() ?? [];
    }
    validate();
  }

  List<String> errors = [];
  List<String> warnings = [];
  List<String> payload = [];
  Map<String, DataSimple> systems = {};

  void loadSystems() {
    String sysTypeName = "Система";
    var sysTypes = lastState.featureTypesByName[sysTypeName]!;
    for (DataSimple s in sysTypes.valuesSimple.values) {
      String name = s.getFirstObjectName("name") ?? "";
      systems[name] = s;
    }
  }

  void checkSystem(String sysname) {
    if (!systems.containsKey(sysname)) {
      errors.add("Системы $sysname не существует");
    }
  }

  void validate() {
    bool isStarted = false;
    String sysName = "";
    for (DataSimpleObject obj in feat.valueSimple.getList("orders") ?? []) {
      String order = obj.name;
      switch (order) {
        case "start":
          if (isStarted == true) {
            errors.add("2 приказа Начать из");
          }
          isStarted = true;
          String newSys = obj.args[0];
          checkSystem(newSys);
          sysName = newSys;
          break;
        case "move":
          if (isStarted == false) {
            errors.add("нет приказа Начать из");
          }
          DataSimple? prevSys = systems[sysName];
          String newSys = obj.args[0];
          checkSystem(newSys);
          if (prevSys != null) {
            //А если её не сущетвут то ошибка будет раньше
            List<DataSimpleObject> linksObj = prevSys.getList("link") ?? [];
            Set<String> links = linksObj.map((e) => e.name).toSet();
            if (!links.contains(newSys)) {
              errors.add("Нет прямого перехода между $sysName и $newSys");
            }
          }
          sysName = newSys;
          break;
        case "load":
          if (isStarted == false) {
            errors.add("нет приказа Начать из");
          }
          var cont = obj.args[0];
          if (int.tryParse(cont) == null) {
            errors.add("Неправильный идетифектаор контейнера $cont должно быть число");
          }
          payload.add(cont);
          break;
        case "unload":
          if (isStarted == false) {
            errors.add("нет приказа Начать из");
          }
          var cont = obj.args[0];
          if (int.tryParse(cont) == null) {
            errors.add("Неправильный идетифектаор контейнера $cont должно быть число");
          }
          if (!payload.contains(cont)) {
            errors.add("Невозможно выгрузить контейнер $cont так как он не загружен");
          } else {
            payload.remove(cont);
          }
          break;
        default:
          errors.add("Неизвестный приказ $order");
      }
    }
    if (payload.isNotEmpty) {
      warnings.add("Контейнеры ${payload.join(",")} не разгружены");
    }
  }
}

class MapOrderQueueMenu extends StatelessWidget {
  const MapOrderQueueMenu({super.key});

  Widget buildOrderScreen(BuildContext context, AsyncSnapshot<StateStore> snapshot) {
    MapOrderQueueRequest? request = ModalRoute.of(context)!.settings.arguments as MapOrderQueueRequest?;
    String orderFeatureName = configuration.mapConfig!.orderFeatureName;
    List<Widget> tiles = [];
    Widget? floating;
    Widget? bottom;
    if (orderFeatureName.isEmpty) {
      return const Text("Order feature not set");
    }
    FeatureTypeDesc? ftype = lastState.featureTypesByName[orderFeatureName];
    if (ftype == null) {
      return Text("Can`t find feature type $orderFeatureName");
    }
    if (request == null) {
      return const Text("Empty request");
    }
    FeatureDesc? feat = findOrder(ftype, request.fleetName);
    if (feat == null) {
      tiles.add(const ListTile(
        title: Text("Нет активных приказов для данного флота"),
      ));
      tiles.add(ElevatedButton(
          onPressed: () {
            DataSimple data = DataSimple();
            data.setFirstObjectName("fleet", request.fleetName);
            data.setFirstObjectName("status", "new");
            lastState.char!.newFeature(ftype.pk, data.toString());
          },
          child: const Text("Создать")));
    } else {
      String status = feat.valueSimple.getFirstObjectName("status") ?? "new";
      for (DataSimpleObject obj in feat.valueSimple.getList("orders") ?? []) {
        MapObjectOrder? orderType = request.object.actions.first.getOrderByName(obj.name);
        if (orderType == null) {
          tiles.add(ListTile(
            title: Text("Unknown order ${obj.name}"),
          ));
          continue;
        }
        Widget? trailing;
        String label = orderType.args[0].name;
        String sublabel = obj.args[0];

        if (status == "new") {
          //TODO: Удалять только последний move
          trailing = IconButton(
              onPressed: () {
                feat.valueSimple.removeObjectFromList("orders", obj);
                String lastPosition = "";
                List<DataSimpleObject> ordersObj = feat.valueSimple.getList("orders") ?? [];
                try {
                  lastPosition = ordersObj.lastWhere((element) => ["start", "move"].contains(element.name)).args[0];
                } on StateError {
                  lastPosition = "";
                }
                feat.valueSimple.setFirstObjectName("lastPosition", lastPosition);
                feat.setData();
              },
              icon: const Icon(Icons.delete));
        }

        tiles.add(ListTile(
          title: Text(label),
          subtitle: Text(sublabel),
          trailing: trailing,
        ));
      }
      ValidateOrder validator = ValidateOrder(feat);
      for (var e in validator.errors) {
        tiles.add(ListTile(
            title: Text(
          "Ошибка:$e",
          style: const TextStyle(color: Colors.red),
        )));
      }
      for (var e in validator.warnings) {
        tiles.add(ListTile(
            title: Text(
          "Предупреждение:$e",
          style: const TextStyle(color: Colors.yellow),
        )));
      }
      if (status == "new") {
        floating = FloatingActionButton(
            onPressed: () => Navigator.of(context).pushNamed("/map/addOrder", arguments: request),
            child: const Icon(Icons.add));
        VoidCallback? butt;
        if (tiles.isNotEmpty && validator.errors.isEmpty) {
          butt = () {
            feat.valueSimple.setFirstObjectName("status", "send");
            feat.setData();

            FeatureDesc? fleet = findFeatureByFleetName(request.object, request.fleetName);
            if (fleet != null) {
              fleet.valueSimple.addObject("attrib", DataSimpleObject("awaiting"));
              fleet.setData();
            }
          };
        }
        bottom = ElevatedButton(
          onPressed: butt,
          child: const Text("Отправить"),
        );
      }
      if (status == "send") {
        ButtonStyle style = ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.red));
        bottom = ElevatedButton(
            onPressed: () async {
              feat.valueSimple.setFirstObjectName("status", "canceled");
              await feat.setData();
              FeatureDesc? fleet = findFeatureByFleetName(request.object, request.fleetName);
              if (fleet != null) {
                DataSimpleObject? toRemove;
                for (DataSimpleObject o in fleet.valueSimple.getList("attrib") ?? []) {
                  if (o.name == "awaiting") {
                    toRemove = o;
                  }
                }
                if (toRemove != null) {
                  fleet.valueSimple.removeObjectFromList("attrib", toRemove);
                  await fleet.setData();
                }
              }
            },
            style: style,
            child: const Text("Отменить"));
        //TODO: подтверждение
      }
      if (status == "flight") {
        int start = int.tryParse(feat.valueSimple.getFirstObjectName("start_time") ?? "") ?? 0;
        DateTime st = DateTime.fromMillisecondsSinceEpoch(start * 1000);
        bottom = ElevatedButton(
            onPressed: () {}, child: Text("Караван начнет движение в ${st.hour}:${st.minute}:${st.second}"));
      }
    }

    return Scaffold(
        appBar: AppBar(
          title: Text("Приказы для ${request.fleetName}"),
          actions: [loadIndicatorBuilder()],
        ),
        body: ListView(
          children: tiles,
        ),
        floatingActionButton: floating,
        bottomNavigationBar: BottomAppBar(child: bottom));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<StateStore>(
        initialData: lastState, stream: stateStreamController.stream, builder: buildOrderScreen);
  }
}

typedef OrderFunc = Future<void> Function(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request);

Future<void> orderStart(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async {
  var mode = MapMode.pick();
  mode.setTypeFilterByConfig();

  String? sysLabelStr = await Navigator.of(context).pushNamed("/map", arguments: mode) as String?;
  if (sysLabelStr == null) {
    return;
  }
  FeatureDesc? orderFeature = findOrderByFleetName(request.fleetName);
  if (orderFeature == null) {
    throw Exception("Order for fleet ${request.fleetName} not exist ");
  }

  _orderStartSet(sysLabelStr, orderFeature);
}

void _orderStartSet(String sysLabelStr, FeatureDesc orderFeature) {
  DataSimpleObject order = DataSimpleObject.empty();
  order.name = "start";
  order.args.add(sysLabelStr);
  orderFeature.valueSimple.addObject("orders", order);
  orderFeature.valueSimple.setFirstObjectName("lastPosition", sysLabelStr);

  orderFeature.setData();
}

DataSimple? getLocationDataByName(String name) {
  List<String> locationTypeNames = configuration.mapConfig!.locationFeatures;
  for (var typeName in locationTypeNames) {
    FeatureTypeDesc? fType = lastState.featureTypesByName[typeName];
    if (fType == null) {
      throw Exception("Feature type from locationTypes $typeName not found");
    }
    if (fType.static) {
      for (DataSimple dat in fType.valuesSimple.values) {
        if (dat.getFirstObjectName("name") == name) {
          return dat;
        }
      }
    } else {
      for (FeatureDesc feat in lastState.char!.featuresByTypeID[fType.pk] ?? []) {
        if (feat.valueSimple.getFirstObjectName("name") == name) {
          return feat.valueSimple;
        }
      }
    }
  }
  return null;
}

Future<void> orderMove(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async {
  FeatureDesc? orderFeature = findOrderByFleetName(request.fleetName);
  if (orderFeature == null) {
    throw Exception("Can`t find order for fleet ${request.fleetName}");
  }

  String? currLocation = orderFeature.valueSimple.getFirstObjectName("lastPosition");
  if (currLocation == null) {
    throw Exception("lastPosition not set for fleet ${request.fleetName}. No start order?");
  }
  DataSimple? sysData = getLocationDataByName(currLocation);
  if (sysData == null) {
    throw Exception("can`t find system $currLocation");
  }

  var mapMode = MapMode.pick();
  mapMode.setTypeFilterByConfig();
  mapMode.allowOnly = sysData.getList("link")?.map((e) => e.name).toList() ?? [];
  mapMode.gray.add(currLocation);
  mapMode.focusedOn = currLocation;
  String? nextSysLabel = await Navigator.of(context).pushNamed("/map", arguments: mapMode) as String?;
  if (nextSysLabel == null) {
    return;
  }

  var obj = DataSimpleObject("move");
  obj.args.add(nextSysLabel);
  orderFeature.valueSimple.addObject("orders", obj);
  orderFeature.valueSimple.setFirstObjectName("lastPosition", nextSysLabel);
  await orderFeature.setData();
}

Future<void> orderLoadUnload(
    BuildContext context, MapObjectOrder order, MapOrderQueueRequest request, String action) async {
  List<String>? contList;
  FeatureDesc? feat = findOrderByFleetName(request.fleetName);
  if (feat == null) {
    throw Exception("Orders for fleet ${request.fleetName} not found");
  }
  if (action == "unload") {
    contList = ValidateOrder(feat).payload;
  }
  CodeRequestAnswer? code = await codeRequest(context, "код контейнера", contList: contList);
  if (code == null) {
    return;
  }

  var obj = DataSimpleObject(action);
  obj.args.add(code.code);
  if (code.changeOwner) {
    int newOnwer = code.newOwner ?? 0;
    obj.args.add(newOnwer.toString());
  }
  feat.valueSimple.addObject("orders", obj);
  await feat.setData();
}

Future<void> orderLoad(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async =>
    orderLoadUnload(context, order, request, "load");

Future<void> orderUnload(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async =>
    orderLoadUnload(context, order, request, "unload");

Future<void> setLocationOrderAttrib(
    BuildContext context, MapObjectOrder order, MapOrderQueueRequest request, String newOrder, String attrib) async {
  FeatureDesc? feat = findFeatureByFleetName(request.object, request.fleetName);
  if (feat == null) {
    throw Exception("Orders for fleet ${request.fleetName} not found");
  }
  var mode = MapMode.pick();
  mode.setTypeFilterByConfig();

  String? sysLabelStr = await Navigator.of(context).pushNamed("/map", arguments: mode) as String?;
  if (sysLabelStr == null) {
    return;
  }
  feat.valueSimple.setFirstObjectName("location", sysLabelStr);
  feat.valueSimple.setFirstObjectName("order", newOrder);
  feat.valueSimple.setFirstObjectName("attrib", attrib);
  feat.valueSimple.setFirstObjectName("start_order", (DateTime.now().millisecondsSinceEpoch / 1000).round().toString());
  await feat.setData();
}

Future<void> orderExplorer(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async {
  FeatureDesc? feat = findFeatureByFleetName(request.object, request.fleetName);
  if (feat == null) {
    throw Exception("Orders for fleet ${request.fleetName} not found");
  }
  var code = await codeRequest(context, "код системы");
  if (code == null) {
    return;
  }
  feat.valueSimple.data.remove("location");
  var ord = DataSimpleObject("explore");
  ord.args.add(code.code);
  feat.valueSimple.setFirstObject("order", ord);
  feat.valueSimple.setFirstObjectName("attrib", "explore");
  feat.valueSimple.setFirstObjectName("start_order", (DateTime.now().millisecondsSinceEpoch / 1000).round().toString());
  await feat.setData();
}

Future<void> orderJump(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async =>
    setLocationOrderAttrib(context, order, request, "", "");

Future<void> orderSpy(BuildContext context, MapObjectOrder order, MapOrderQueueRequest request) async =>
    setLocationOrderAttrib(context, order, request, "spy", "spy");

Map<MapObjectOrderType, OrderFunc> orderFuncTable = {
  MapObjectOrderType.start: orderStart,
  MapObjectOrderType.move: orderMove,
  MapObjectOrderType.load: orderLoad,
  MapObjectOrderType.unload: orderUnload,
  MapObjectOrderType.jump: orderJump,
  MapObjectOrderType.spy: orderSpy,
  MapObjectOrderType.explore: orderExplorer
};

class AddMapOrderMenu extends StatelessWidget {
  const AddMapOrderMenu({super.key});

  final startOrder = MapObjectOrderType.start;

  @override
  Widget build(BuildContext context) {
    MapOrderQueueRequest request = ModalRoute.of(context)!.settings.arguments as MapOrderQueueRequest;
    List<Widget> tiles = [];
    FeatureDesc? orderFeature = findOrderByFleetName(request.fleetName);
    var orders = orderFeature?.valueSimple.getList("orders") ?? [];
    bool isFirst = orders.isEmpty;
    for (MapObjectAction action in request.object.actions) {
      if (action.type == MapObjectActionType.orderQueue) {
        for (var order in action.orders) {
          if (isFirst != (startOrder == order.type)) {
            continue; //Если это первый приказ  в списке, то мы показываем только start
            //Если не первый то показываем все кроме старта
          }
          if (order.type == startOrder) {
            FeatureDesc? fleet = findFeatureByFleetName(request.object, request.fleetName);
            List<DataSimpleObject> payload = fleet?.valueSimple.getList("payload") ?? [];
            if (payload.isNotEmpty) {
              //Наш караван не пустой, мы можем начать только с текущей системы
              String loc = fleet!.valueSimple.getFirstObjectName("location")!;
              _orderStartSet(loc, orderFeature!);
              return Scaffold(
                appBar: AppBar(title: Text("Караван стартует из $loc")),
                body: Text("Караван стартует из $loc потому что в нем есть неразгруженные ящики."
                    "Ящики $payload"),
              );
            }
          }
          tiles.add(ListTile(
              title: Text(order.args[0].name),
              onTap: () async {
                await orderFuncTable[order.type]?.call(context, order, request);
                Navigator.of(navigatorKey.currentContext!).pop();
              }));
        }
      }
    }
    return Scaffold(
      appBar: AppBar(title: Text("Добавить приказ для ${request.fleetName}")),
      body: ListView(
        children: tiles,
      ),
    );
  }
}

class CodeRequestAnswer {
  late String code;
  int? newOwner;
  bool changeOwner = false;

  CodeRequestAnswer(this.code);
}

Future<CodeRequestAnswer?> codeRequest(BuildContext context, String label, {List<String>? contList}) async {
  var editController = TextEditingController();
  List<DropdownMenuEntry<int>> items = [];
  items.add(const DropdownMenuEntry(value: -1, label: "Не менять"));
  items.add(const DropdownMenuEntry(value: 0, label: "Сделать публичным"));
  for (var c in lastState.characters.values) {
    items.add(DropdownMenuEntry(value: c.id, label: c.name));
  }
  int? selected;
  String? contSelected;
  return await showDialog(
      context: context,
      builder: (BuildContext ctx) {
        List<Widget> contetns = [];
        if (contList == null) {
          contetns.add(TextField(
            controller: editController,
            decoration: InputDecoration(label: Text(label)),
          ));
        } else {
          contetns.add(StatefulBuilder(
              builder: (BuildContext ctx, StateSetter setState) => DropdownButton<String>(
                  value: contSelected,
                  items: contList.map((e) => DropdownMenuItem(value: e, child: Text(e))).toList(),
                  onChanged: (s) => setState(() {
                        contSelected = s;
                      }))));
        }
        /*   contetns.add(DropdownMenu<int>(
          enableFilter: true,
          label: const Text("Сменить владельца"),
          dropdownMenuEntries: items,
          onSelected: (s) {
            selected = s;
          },
        ));*/
        return AlertDialog(
          title: Text("Введите $label"),
          content: Column(children: contetns),
          actions: [
            TextButton(
                onPressed: () {
                  var r = CodeRequestAnswer((contList == null) ? editController.text : contSelected ?? "");
                  if (selected != null && selected != -1) {
                    r.changeOwner = true;
                    r.newOwner = selected;
                  }
                  Navigator.of(context).pop(r);
                },
                child: const Text('ok'))
          ],
        );
      });
}

class FleetMenu extends StatelessWidget {
  const FleetMenu({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Флоты"),
          actions: [loadIndicatorBuilder()],
        ),
        drawer: getCommonDrawer(context),
        body: StreamBuilder<StateStore>(
          initialData: lastState,
          stream: stateStreamController.stream,
          builder: ((context, snapshot) {
            List<Widget> tiles = [];
            for (var obj in configuration.mapConfig!.objects.values) {
              if (obj.actions.isEmpty) continue;
              FeatureTypeDesc fType = lastState.featureTypesByName[obj.name]!;
              if (!fType.static) {
                for (FeatureDesc feat in lastState.char!.featuresByTypeID[fType.pk] ?? []) {
                  String name = feat.valueSimple.getFirstObjectName("name") ?? "";
                  String title = "${obj.name} $name";
                  String location = "Находится в ${feat.valueSimple.getFirstObjectName("location")}";
//TODO: таймер
                  List<Widget> icons = [];
                  List<String> attrNames = ["base"];
                  attrNames.addAll(feat.valueSimple.getList("attrib")?.map((e) => e.name) ?? []);
                  for (String attrName in attrNames) {
                    if (attrName.isEmpty) continue;
                    var attr = obj.getAttr(attrName);

                    if (attr.type == MapObjectAttributeType.image) {
                      Asset? ass = configuration.assets[attr.data];
                      if (ass != null) {
                        icons.add(Image.file(ass.file));
                      }
                    }
                  }
                  tiles.add(ListTile(
                    title: Text(title),
                    subtitle: Text(location),
                    leading: Stack(
                      children: icons,
                    ),
                    onTap: () {
                      MapObjectAction act = obj.actions.first;
                      String path = mapActionMenu[act.type] ?? "/";
                      Navigator.of(context)
                          .pushNamed(path, arguments: MapOrderQueueRequest(name, obj, feat.valueSimple));
                    },
                  ));
                }
              }
            }
            return ListView(
              children: tiles,
            );
          }),
        ));
  }
}

Set<String> productions = {
  "Вода и Лед",
  "Квантиум-40",
  "Литий-6",
  "Минералы",
  "Медикаменты",
  "Продовольствие",
  "Удобрения",
  "Стройматериалы",
  "Химикаты",
  "Электроника",
  "Машинерия",
  "Товары роскоши",
  "Оружие наземное",
  "Оружие космическое",
  "Корабельные запчасти",
};

class MapDetails extends StatelessWidget {
  const MapDetails({super.key});

  @override
  Widget build(BuildContext context) {
    MapOrderQueueRequest request = ModalRoute.of(context)!.settings.arguments as MapOrderQueueRequest;
    return Scaffold(
        appBar: AppBar(
          title: Text("Оисание системы ${request.fleetName}"),
        ),
        body: StreamBuilder<StateStore>(
            initialData: lastState,
            stream: stateStreamController.stream,
            builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
              List<Widget> tiles = [];
              String owner = request.values.getFirstObjectName("owned") ?? "Неизвестен";
              tiles.add(ListTile(title: Text("Владелец: $owner")));
              bool custom = false;
              String size = "Неизвестен";
              for (DataSimpleObject c in request.values.getList("attrib") ?? []) {
                if (c.name.endsWith("colony") || c.name.endsWith("homeworld")) {
                  if (c.name[0] == "s") size = "маленькая";
                  if (c.name[0] == "m") size = "средняя";
                  if (c.name[0] == "l") size = "большая";
                }
                if (c.name.endsWith("homeworld")) {
                  custom = true;
                  break;
                }
              }
              tiles.add(ListTile(
                title: Text("Размер системы: $size"),
              ));
              tiles.add(ListTile(
                title: Text("Таможня ${custom ? 'пристствует' : 'отсутствует'}"),
              ));
              tiles.add(ListTile(
                title: Text("Стоимость прохода ${request.values.getFirstObjectName("move_cost") ?? 0} кредитов"),
              ));
              for (FeatureContainer c in state.data!.containers.values) {
                if (c.extraSimple?.getFirstObjectName("location") != request.fleetName) {
                  continue;
                }
                tiles.add(ListTile(
                  leading: const Icon(Icons.circle),
                  title: Text(c.name),
                  onTap: () => Navigator.of(context).pushNamed("/containers/detail", arguments: c),
                ));
              }
              tiles.add(const ListTile(
                title: Text("В системе находятся:"),
              ));
              List<DataSimpleObject> production = request.values.getList("production") ?? [];
              if (production.isNotEmpty) {
                for (DataSimpleObject o in production) {
                  tiles.add(ListTile(
                    title: Text("Система прозводит(${o.name}):"),
                  ));
                  for (String l in o.args) {
                    tiles.add(ListTile(leading: const Icon(Icons.circle), title: Text(l)));
                  }
                }
              }
              return ListView(
                children: tiles,
              );
            }));
  }
}

class InstantOrderMenu extends StatelessWidget {
  const InstantOrderMenu({super.key});

  String? getCooldown(FeatureDesc feat) {
    DateFormat format = DateFormat.Hms();
    String? cooldown = feat.valueSimple.getFirstObjectName("cooldown");
    if (cooldown == null) return null;
    int i_cooldown = int.tryParse(cooldown) ?? 0;
    if (i_cooldown == 0) return null;
    DateTime dt = DateTime.fromMillisecondsSinceEpoch(i_cooldown * 1000);
    if (dt.difference(DateTime.now()).isNegative) {
      log("Cooldown expire dt=$dt now=${DateTime.now()}");
      feat.valueSimple.setFirstObjectName("attrib", "");
      feat.valueSimple.setFirstObjectName("cooldown", "0");
      feat.setData();
    }
    return format.format(dt);
  }

  @override
  Widget build(BuildContext context) {
    MapOrderQueueRequest? request = ModalRoute.of(context)?.settings.arguments as MapOrderQueueRequest?;
    if (request == null) {
      return const Text("Запущен InstanOrderMenu без указания флота");
    }
    FeatureDesc? fleet = findFeatureByFleetName(request.object, request.fleetName);
    if (fleet == null) {
      return Text("Флот ${request.fleetName} не найден");
    }
    String name = fleet.valueSimple.getFirstObjectName("name") ?? "неизвестно";
    List<Widget> tiles = [];

    String? cooldown = getCooldown(fleet);
    if (cooldown != null) {
      tiles.add(ListTile(
        title: Text("Находися в кулдауне до $cooldown"),
      ));
    } else {
      for (var order in request.object.actions[0].orders) {
        tiles.add(ListTile(
          title: Text(order.args[0].name),
          onTap: () async {
            var func = orderFuncTable[order.type];
            if (func == null) {
              addError(Error.system("Функция для ${order.type.name} не найдена"));
              return;
            }
            await func.call(context, order, request);
          },
        ));
      }
    }
    return Scaffold(
      appBar: AppBar(
        title: Text("Выберете приказ для $name"),
      ),
      body: ListView(
        children: tiles,
      ),
    );
  }
}
