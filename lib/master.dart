import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:omnilight2/config.dart';
import 'package:omnilight2/model.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'global.dart';

class PlayersMenu extends StatelessWidget {
  const PlayersMenu({Key? key}) : super(key: key);

  Widget getListTile(PlayerDesc d, BuildContext ctx) {
    return ListTile(
      title: Text(d.nickname),
      onTap: () => Navigator.of(ctx).pushNamed("/master/player", arguments: d.id),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Игроки"),
        actions: [loadIndicatorBuilder()],
      ),
      drawer: getCommonDrawer(context),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          return ListView(children: state.data!.players.values.map((e) => getListTile(e, context)).toList());
        },
      ),
    );
  }
}

class PlayerMenu extends StatelessWidget {
  const PlayerMenu({Key? key}) : super(key: key);

  List<Widget> showPlayer(PlayerDesc d, BuildContext ctx) {
    List<Widget> ret = [];
    ret.add(ListTile(
      title: const Text("Имя"),
      subtitle: Text(d.realName),
    ));
    ret.add(ListTile(
      title: const Text("Ник"),
      subtitle: Text(d.nickname),
    ));
    ret.add(ListTile(
      title: const Text("ID"),
      subtitle: Text(d.id.toString()),
    ));
    ret.add(ListTile(
      title: const Text("Медицинска информация"),
      subtitle: Text(d.medicalInfo),
    ));
    ret.add(ListTile(
      title: const Text("Контакты"),
      subtitle: Text(d.contacts.join(" ")),
    ));
    ret.add(ListTile(
      title: const Text("Уплочено"),
      subtitle: Text(d.paid.toString()),
    ));
    ret.add(ListTile(
        title: const Text("Персонаж"),
        subtitle: Text(d.character.toString()),
        trailing: ElevatedButton(
          onPressed: () => Navigator.of(ctx).pushNamed("/master/character", arguments: d.character),
          child: const Text("Открыть"),
        )));

    return ret;
  }

  @override
  Widget build(BuildContext context) {
    int id = ModalRoute.of(context)?.settings.arguments as int? ?? -1;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Игрок"),
        actions: [loadIndicatorBuilder()],
      ),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          if (id < 0) {
            return Text("Неправильный ID персонажа $id");
          }
          PlayerDesc? p = state.data!.players[id];
          if (p == null) return Text("Игрок $id не найден");
          p.isLoading();
          return ListView(children: showPlayer(p, context));
        },
      ),
    );
  }
}

class CharactersMenu extends StatelessWidget {
  const CharactersMenu({Key? key}) : super(key: key);

  Widget getListTile(CharacterDesc d, BuildContext ctx) {
    return ListTile(
      title: Text(d.name),
      onTap: () => Navigator.of(ctx).pushNamed("/master/character", arguments: d.id),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Персонажи"),
        actions: [loadIndicatorBuilder()],
      ),
      drawer: getCommonDrawer(context),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          return ListView(children: state.data!.characters.values.map((e) => getListTile(e, context)).toList());
        },
      ),
    );
  }
}

class MasterCharacterMenu extends StatelessWidget {
  const MasterCharacterMenu({Key? key}) : super(key: key);

  Widget getLocations(CharacterDesc d) {
    List<Widget> buttons = [];
    for (var lid in d.locationIDs) {
      String label = "Загрузка...";
      Location? l = lastState.locations[lid];
      if (l != null) {
        label = l.name;
      }
      buttons.add(ElevatedButton(
          onPressed: () {
            Navigator.of(navigatorKey.currentContext!).pushNamed("/master/location", arguments: lid);
          },
          child: Text(label)));
    }
    return Row(children: buttons);
  }

  List<Widget> showCharacter(CharacterDesc d) {
    List<Widget> ret = [];
    ret.add(ListTile(
      title: const Text("Имя"),
      subtitle: Text(d.name),
    ));
    ret.add(ListTile(
      title: const Text("ID"),
      subtitle: Text(d.id.toString()),
    ));
    ret.add(ListTile(
      title: const Text("Локации"),
      subtitle: getLocations(d),
    ));
    ret.add(ListTile(
      title: const Text("Статус"),
      subtitle: Text(d.currentStatus),
    ));
    for (FeatureDesc ft in d.features) {
      ret.add(ListTile(
        title: Text(ft.name),
        subtitle: Text(ft.valueStr),
      ));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    int id = ModalRoute.of(context)?.settings.arguments as int? ?? -1;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Персонаж"),
        actions: [loadIndicatorBuilder()],
      ),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          if (id < 0) {
            return Text("неправильный ID персонажа $id");
          }
          CharacterDesc? c = state.data!.characters[id];
          if (c == null) {
            return Text("Перосонаж с id $id не найден");
          }
          c.isLoading();
          return ListView(children: showCharacter(c));
        },
      ),
    );
  }
}

class TransitionsList extends StatelessWidget {
  const TransitionsList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("транзишены"),
      ),
      drawer: getCommonDrawer(context),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> shot) {
          if (shot.data == null) {
            return const Text("Нет данных");
          }

          List<Widget> widgets = [];

          for (TransitionDesc d in shot.data!.transitions.values) {
            widgets.add(ListTile(
              title: Text(d.key),
              subtitle: Text(d.title),
              onTap: () => Navigator.of(context).pushNamed("/master/transition", arguments: d.key),
            ));
          }
          return ListView(
            children: widgets,
          );
        },
      ),
    );
  }
}

class TransitionDetail extends StatelessWidget {
  const TransitionDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String key = ModalRoute.of(context)!.settings.arguments as String;

    return Scaffold(
      appBar: AppBar(
        title: const Text("детали транзишена"),
      ),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> shot) {
          if (shot.data == null) {
            return const Text("Нет данных");
          }
          TransitionDesc? trans = shot.data!.transitions[key];
          if (trans == null) {
            return Text("Транзишен $key не найден");
          }
          List<Widget> widgets = [];
          trans.isLoading();
          widgets.add(ListTile(
            title: const Text("Ключ"),
            subtitle: Text(trans.key),
          ));
          widgets.add(ListTile(
            title: const Text("Заголовок"),
            subtitle: Text(trans.title),
          ));
          widgets.add(ListTile(
            title: const Text("Описание"),
            subtitle: Text(trans.description),
          ));
          widgets.add(const ListTile(title: Text("Компоненты")));
          for (var c in trans.components) {
            widgets.add(ListTile(
              title: const Text("Действие"),
              subtitle: Text(c.type),
            ));
            widgets.add(ListTile(
              title: const Text("Свойство"),
              subtitle: Text(c.feature_type_name),
            ));
            widgets.add(ListTile(
              title: const Text("Значение"),
              subtitle: Text(c.feature_value),
            ));
            widgets.add(ListTile(
              title: const Text("Описание(обычно ошибка)"),
              subtitle: Text(c.title),
            ));
            widgets.add(const ListTile(
              title: Text("=========="),
            ));
          }
          return ListView(
            children: widgets,
          );
        },
      ),
    );
  }
}

class MasterQRCodeDetail extends StatelessWidget {
  const MasterQRCodeDetail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var res = ModalRoute.of(context)!.settings.arguments as QRScannerResult;
    return Scaffold(
      appBar: AppBar(
        title: const Text("QR code"),
      ),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> shot) {
          if (shot.data == null) {
            return const Text("Нет данных");
          }

          List<Widget> widgets = [];
          String codefull = "${res.code}${res.number!.toString().padLeft(6, '0')}";
          widgets.add(QrImage(
            data: codefull,
            version: QrVersions.auto,
            size: 200,
            backgroundColor: const Color.fromARGB(255, 255, 255, 255),
          ));
          widgets.add(ListTile(
            title: Text(codefull),
          ));
          QRTypeBase? action = configuration.qrActions[res.code];
          if (action == null) {
            widgets.add(const ListTile(title: Text("Неизвестный тип кода")));
          } else {
            widgets.addAll(configuration.qrActions[res.code]!.masterInformation(res));
          }
          return ListView(
            children: widgets,
          );
        },
      ),
    );
  }
}

class LocationMenu extends StatelessWidget {
  const LocationMenu({Key? key}) : super(key: key);

  Widget getListTile(Location d, BuildContext ctx) {
    return ListTile(
      title: Text(d.name),
      subtitle: Text("${d.members.length} перосонажей"),
      onTap: () => Navigator.of(ctx).pushNamed("/master/location", arguments: d.pk),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Локации"),
        actions: [loadIndicatorBuilder()],
      ),
      drawer: getCommonDrawer(context),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          return ListView(children: state.data!.locations.values.map((e) => getListTile(e, context)).toList());
        },
      ),
    );
  }
}

class MasterLocationMenu extends StatelessWidget {
  const MasterLocationMenu({Key? key}) : super(key: key);

  String getNameById(int cid) {
    CharacterDesc? char = lastState.getCharacterById(cid);
    if (char != null) {
      return char.name;
    }
    return "Загрузка...";
  }

  List<Widget> showLocation(Location d) {
    List<Widget> ret = [];
    d.isLoading();
    ret.add(ListTile(
      title: const Text("Имя"),
      subtitle: Text(d.name),
    ));
    ret.add(ListTile(
      title: const Text("ID"),
      subtitle: Text(d.pk.toString()),
    ));

    ret.add(ListTile(
      title: const Text("Лидер"),
      subtitle: Text(getNameById(d.leaderPk)),
      onTap: () {
        Navigator.of(navigatorKey.currentContext!).pushNamed("/master/character", arguments: d.leaderPk);
      },
    ));
    ret.add(const ListTile(
      title: Text("Участники:"),
    ));
    for (var cid in d.members) {
      ret.add(ListTile(
          title: Text(getNameById(cid)),
          onTap: () {
            Navigator.of(navigatorKey.currentContext!).pushNamed("/master/character", arguments: cid);
          }));
    }
    return ret;
  }

  @override
  Widget build(BuildContext context) {
    int id = ModalRoute.of(context)?.settings.arguments as int? ?? -1;
    return Scaffold(
      appBar: AppBar(
        title: const Text("Локация"),
        actions: [loadIndicatorBuilder()],
      ),
      body: StreamBuilder<StateStore>(
        stream: stateStreamController.stream,
        initialData: lastState,
        builder: (BuildContext ctx, AsyncSnapshot<StateStore> state) {
          if (state.data == null) {
            return const Text("Нет данных");
          }
          if (id < 0) {
            return Text("неправильный ID локации $id");
          }
          Location? c = state.data!.locations[id];
          if (c == null) {
            return Text("Локация с id $id не найдена");
          }
          c.isLoading();
          return ListView(children: showLocation(c));
        },
      ),
    );
  }
}

abstract class MasterSendRcptBase {
  String getTitle();

  String getSubtitle();

  Set<int> getPKs();
}

class MasterSendCharacter extends MasterSendRcptBase {
  late CharacterDesc char;

  MasterSendCharacter(String charName) {
    char = lastState.charactersByName[charName]!;
  }

  @override
  Set<int> getPKs() => {char.id};

  @override
  String getSubtitle() => "";

  @override
  String getTitle() => char.name;
}

class MasterSendLocation extends MasterSendRcptBase {
  late Location loc;

  MasterSendLocation(String name) {
    for (var l in lastState.locations.values) {
      if (l.name == name) {
        loc = l;
        return;
      }
    }
    throw Exception("Can`t find location $name");
  }

  @override
  Set<int> getPKs() => Set<int>.from(loc.members);

  @override
  String getSubtitle() => "${loc.members.length} пероснажей";

  @override
  String getTitle() => loc.name;
}

class MasterSendAll extends MasterSendRcptBase {
  Set<int> pks = {};
  late CharacterType t;

  MasterSendAll(this.t) {
    for (var c in lastState.characters.values) {
      if (t == CharacterType.all) {
        pks.add(c.id);
        continue;
      }
      if (c.type == t) {
        pks.add(c.id);
      }
    }
  }

  @override
  Set<int> getPKs() => pks;

  @override
  String getSubtitle() => "${pks.length} персонажей";

  @override
  String getTitle() => "Все персонажи типа ${t.toString()}";
}

class MasterMassSend extends StatefulWidget {
  const MasterMassSend({super.key});

  @override
  State<StatefulWidget> createState() => MasterMassSendState();
}

class MasterMassSendState extends State<MasterMassSend> {
  final fromEditControl = TextEditingController();
  final textEditControl = TextEditingController();
  final recipients = <MasterSendRcptBase>[];

  String sendState = "";

  List<DropdownMenuEntry<String>> getKnownSender() {
    Set<String> already = {};
    List<DropdownMenuEntry<String>> knownSenders = [];
    for (var c in lastState.chats.values) {
      for (var m in c.members.values) {
        if (m.type == ChatMemberType.Master && !already.contains(m.name)) {
          knownSenders.add(DropdownMenuEntry(value: m.name, label: m.name));
          already.add(m.name);
        }
      }
    }
    return knownSenders;
  }

  Widget allDialogBuilder(BuildContext ctx) {
    CharacterType now = CharacterType.all;
    List<DropdownMenuItem<CharacterType>> types = [];
    types.add(const DropdownMenuItem(
      value: CharacterType.all,
      child: Text("Вообще всем"),
    ));
    types.add(const DropdownMenuItem(value: CharacterType.player, child: Text("Настоящим игрокам")));
    types.add(const DropdownMenuItem(value: CharacterType.npc, child: Text("Игротехам")));
    types.add(const DropdownMenuItem(value: CharacterType.technical, child: Text("Техническим персонажам")));
    return AlertDialog(
      title: const Text("Насколько всем игрокам отправит?"),
      content: StatefulBuilder(
          builder: (BuildContext ctx, StateSetter setState) => DropdownButton<CharacterType>(
              items: types,
              value: now,
              onChanged: (CharacterType? n) => setState(() {
                    now = n!;
                  }))),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(ctx).pop(now);
            },
            child: const Text("ok")),
        TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: const Text("Отмена"))
      ],
    );
  }

  Chat? findChat(int cid, String masterName) {
    log("Find chat btw $cid and $masterName");
    for (var c in lastState.chats.values) {
      bool masterOk = false, charOk = false;
      for (var m in c.members.values) {
        if (m.type == ChatMemberType.Character && m.id == cid) {
          charOk = true;
        }
        if (m.type == ChatMemberType.Master && m.name == masterName) {
          masterOk = true;
        }
      }
      if (charOk && masterOk) {
        return c;
      }
    }
    return null;
  }

  Future<void> send(Set<int> sendto) async {
    String masterName = fromEditControl.text;
    int inow = 0;
    int iall = sendto.length;
    for (var cid in sendto) {
      setState(() {
        sendState = "Отправка $inow/$iall";
      });
      Chat? chat = findChat(cid, masterName);
      if (chat == null) {
        var chatID = await lastState.newChat(masterName, [cid], [masterName]);
        await lastState.reloadChats();
        chat = lastState.chats[chatID]!;
      }

      await chat.send(textEditControl.text);
      inow++;
    }
    setState(() {
      sendState = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = [];
    items.add(const ListTile(
      title: Text("От кого:"),
    ));

    items.add(DropdownMenu<String>(
      dropdownMenuEntries: getKnownSender(),
      controller: fromEditControl,
    ));
    items.add(ListTile(
      title: const Text("Кому:"),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ElevatedButton(
              onPressed: () async {
                var dresp = await showDialog(context: context, builder: allDialogBuilder);
                if (dresp != null) {
                  CharacterType t = dresp as CharacterType;
                  setState(() {
                    recipients.add(MasterSendAll(t));
                  });
                }
              },
              child: const Text("+Все")),
          ElevatedButton(
              onPressed: () async {
                var dresp =
                    await Navigator.of(context).pushNamed("/master/selectto", arguments: lastState.characters.values);
                if (dresp != null) {
                  Set<dynamic> resp = dresp as Set<dynamic>;
                  setState(() {
                    for (String name in resp) {
                      recipients.add(MasterSendCharacter(name));
                    }
                  });
                }
              },
              child: const Text("+Персонаж")),
          ElevatedButton(
              onPressed: () async {
                var dresp =
                    await Navigator.of(context).pushNamed("/master/selectto", arguments: lastState.locations.values);
                if (dresp != null) {
                  Set<dynamic> resp = dresp as Set<dynamic>;
                  setState(() {
                    for (String name in resp) {
                      recipients.add(MasterSendLocation(name));
                    }
                  });
                }
              },
              child: const Text("+Локации"))
        ],
      ),
    ));
    Set<int> sendto = {};
    for (var r in recipients) {
      items.add(ListTile(
        title: Text(r.getTitle()),
        subtitle: Text(r.getSubtitle()),
        trailing: IconButton(
          onPressed: () {
            setState(() {
              recipients.remove(r);
            });
          },
          icon: const Icon(Icons.delete_forever),
        ),
      ));
      sendto.addAll(r.getPKs());
    }
    items.add(ListTile(title: Text("Сообщение будет отправлено ${sendto.length} игрокам")));
    items.add(TextField(
      controller: textEditControl,
      maxLines: 3,
    ));
    if (sendState.isEmpty) {
      items.add(ElevatedButton(onPressed: () => send(sendto), child: const Text("Отправить")));
    } else {
      items.add(ElevatedButton(onPressed: null, child: Text(sendState)));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Массовая рассылка сообщений"),
      ),
      drawer: getCommonDrawer(context),
      body: ListView(
        children: items,
      ),
    );
  }
}

class SelectToMenu extends StatefulWidget {
  const SelectToMenu({super.key});

  @override
  State<StatefulWidget> createState() => SelectToState();
}

class SelectToState extends State<SelectToMenu> {
  final selected = <String>{};

  @override
  Widget build(BuildContext context) {
    Iterable<dynamic> items = ModalRoute.of(context)?.settings.arguments as Iterable? ?? [];
    List<Widget> elems = [];
    for (var i in items) {
      String name = i.name;
      late Widget check;
      if (!selected.contains(name)) {
        check = const Icon(Icons.check_box_outline_blank);
      } else {
        check = const Icon(Icons.check_box);
      }
      elems.add(ListTile(
        title: Text(name),
        trailing: check,
        onTap: () => setState(() {
          if (selected.contains(name)) {
            selected.remove(name);
          } else {
            selected.add(name);
          }
        }),
      ));
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text("Выберите получателя"),
        actions: [
          ElevatedButton(
              onPressed: () {
                Navigator.of(context).pop(selected);
              },
              child: const Text("Готово"))
        ],
      ),
      body: ListView(
        children: elems,
      ),
    );
  }
}
